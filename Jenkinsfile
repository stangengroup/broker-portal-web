pipeline {
  agent any

  triggers {
    pollSCM('* * * * *')
  }

  environment {
      JAVA_HOME = '/usr/lib/jvm/java-11-openjdk-amd64/'
      ANSIBLE_INVENTORY= '/etc/ansible/ec2.py'
      EC2_INI_PATH = '/etc/ansible/ec2.ini'
      PATH="/home/ubuntu/node16/bin:$PATH"
      NVM_DIR="/var/lib/jenkins/.nvm"
  }

  stages {
    stage('Check commit message') {
      when { changelog '.*\\[maven-release-plugin\\].*' }
      steps {
       script {
          pom = readMavenPom file: 'pom.xml'
          currentBuild.displayName = pom.version
          currentBuild.result = 'NOT_BUILT'
       }
       error('Skipping release build')
     }
    }

    stage('Build Angular') {
      steps {
        dir('src/main/angular') {
          sh '''
             [ -s "$NVM_DIR/nvm.sh" ] && \\. "$NVM_DIR/nvm.sh"  # This loads nvm
             [ -s "$NVM_DIR/bash_completion" ] && \\. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
             nvm use 16
             npm install
             npm run build:prod
             '''
        }
      }
    }

    stage('Build and Test') {
      steps {
        sh '''
          mvn clean verify
        '''
      }
    }
    stage('Deliver for Test') {
      when {
        branch 'test'
      }
      steps {
        sh '''
          mvn clean install -Dmaven.test.skip=true
          sudo cp ./target/*.jar /mnt/dev/dev/services/deploy
        '''
        dir('/var/lib/jenkins/ansible') {
          sh '''
            ansible-playbook --private-key /var/lib/jenkins/.ssh/stangen-dev.pem --extra-vars\
 "host=tag_Diff_Role_Dev_Kp_Services\
 target_group=arn:aws:elasticloadbalancing:eu-west-1:219682524890:targetgroup/test-svc-broker-portal-service/4abf8ffac833263b\
 service_user=www-data\
 service_name=broker-portal-orch\
 service_port=7001\
 source_folder=/mnt/data/dev/services/deploy\
 file=broker-portal-orch\
 environment_file=/usr/local/stangengroup/broker-portal-orch.conf\
 start_memory=-Xms64m\
 max_memory=-Xmx128m\
 version=0.0.1-SNAPSHOT"\
 deploy.yml
          '''
        }
      }
    }
    stage('Deliver for Master') {
      when {
        branch 'master'
      }
      steps {
        sh '''
          mvn clean install -Dmaven.test.skip=true
          sudo cp ./target/*.jar /mnt/data/prod/services/deploy
        '''
        dir('/var/lib/jenkins/ansible') {
          sh '''
            ansible-playbook --private-key /var/lib/jenkins/.ssh/SGProd.pem --extra-vars\
 "host=tag_Diff_Role_Prod_Kp_Services_1\
 target_group=arn:aws:elasticloadbalancing:eu-west-1:219682524890:targetgroup/test-svc-broker-portal-service/4abf8ffac833263b\
 service_user=www-data\
 service_name=broker-portal-orch\
 service_port=7001\
 source_folder=/mnt/data/prod/services/deploy\
 file=broker-portal-orch\
 environment_file=/usr/local/stangengroup/broker-portal-orch.conf\
 start_memory=-Xms64m\
 max_memory=-Xmx128m\
 version=0.0.1-SNAPSHOT"\
 deploy.yml
          '''
        }
      }
    }
  }
}