import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { environment } from 'src/environments/environment';
import { SharedModule } from './shared/shared.module';
import { SessionState } from './state/store/session.state';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth/auth.service';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AuthApiClass } from './api/auth.api.class';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MsalBroadcastService, MsalGuard, MsalInterceptor, MsalModule, MsalRedirectComponent, MsalService } from '@azure/msal-angular';
import { BrowserCacheLocation, InteractionType, LogLevel, PublicClientApplication } from '@azure/msal-browser';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ConfigApi } from './api/config.api.class';
import { ConfigService } from './services/config/config.service';
import { MsalContainerGuard } from './guards/msal-container/msal-container.guard';
import { ConfirmDialogComponent } from './generic-components/dialog/confirm-dialog/confirm-dialog.component';
import { DialogService } from './services/shared/dialog/dialog.service';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { UtilityService } from "./services/utility/utility.service";
import { StaticService } from "./services/static/static.service";
import { InterceptorService } from "./services/interceptor/interceptor.service";
import { DataService } from "./services/shared/data/data.service";
import { LoginModule } from "./login/login.module";
// import { msalConfig } from './login/auth-config';

const isIE = window.navigator.userAgent.indexOf("MSIE ") > -1 || window.navigator.userAgent.indexOf("Trident/") > -1;
if (environment.production) {
  window.console.log = function () { };   // disable any console.log debugging statements in production mode
  // window.console.error = function () { };

}

// /**
//  * Here we pass the configuration parameters to create an MSAL instance.
//  * For more info, visit: https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/configuration.md
//  */
//  export function MSALInstanceFactory() {
//   return new PublicClientApplication(
// }

// /**
//  * Set your default interaction type for MSALGuard here. If you have any
//  * additional scopes you want the user to consent upon login, add them here as well.
//  */
// export function MSALGuardConfigFactory(): MsalGuardConfiguration {
//   return {
//     interactionType: InteractionType.Redirect,
//   };
// }

// export function MSALInterceptorConfigFactory(): MsalInterceptorConfiguration {
//   const protectedResourceMap = new Map<string, Array<string>>();
//   protectedResourceMap.set('Enter_the_Graph_Endpoint_Here/v1.0/me', ['user.read']);

//   return {
//     interactionType: InteractionType.Redirect,
//     protectedResourceMap
//   };
// }

@NgModule({
  declarations: [
    AppComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    HttpClientModule,
    CommonModule,
    BrowserModule,
    NgxsModule.forRoot([
      SessionState
    ], {
      developmentMode: !environment.production
    }),
    SharedModule,
    MsalModule.forRoot(new PublicClientApplication({
      auth: {
        clientId: '3692179a-1772-44cd-8a81-0ead37933c8b', // This is your client ID
        authority: 'https://login.microsoftonline.com/e854d556-86e5-42b2-92dd-b1a73984ada7', // This is your tenant ID
        redirectUri: 'https://brokeradmin.kingpricelife.co.za', // This is your redirect URI
        postLogoutRedirectUri: 'https://brokeradmin.kingpricelife.co.za/logout'
        // clientId: '840256e9-4165-4ee8-84f9-0967da0f90ef', // This is your client ID
        // authority: 'https://login.microsoftonline.com/84bb96f9-e490-46b8-ba95-9e8de39b2718', // This is your tenant ID
        // redirectUri: 'http://localhost:4200', // This is your redirect URI
        // postLogoutRedirectUri: 'http://localhost:4200/logout'
      },
      cache: {
        cacheLocation: BrowserCacheLocation.LocalStorage, // Configures cache location. "sessionStorage" is more secure, but "localStorage" gives you SSO between tabs.
        storeAuthStateInCookie: isIE, // Set this to "true" if you are having issues on IE11 or Edge
      },
      system: {
        loggerOptions: {
          loggerCallback(logLevel: LogLevel, message: string) {
            console.log(message);
          },
          logLevel: LogLevel.Info,
          piiLoggingEnabled: false
        }
      }
    }), {
      interactionType: InteractionType.Popup,
      authRequest: {
        scopes: ['user.read']
      }
    }, {
      interactionType: InteractionType.Popup, // MSAL Interceptor Configuration
      protectedResourceMap: new Map([
        ['https://graph.microsoft.com/v1.0/me', ['user.read']]
      ])
    }),
    LoginModule
  ],
  providers: [
    MsalContainerGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    {
      provide: JWT_OPTIONS,
      useValue: JWT_OPTIONS,
    },
    JwtHelperService,
    AuthService,
    AuthApiClass,
    MsalService,
    MsalBroadcastService,
    ConfigApi,
    ConfigService,
    DataService,
    {
      provide: MatDialogRef,
      useValue: {}
    },
    DialogService,
    UtilityService,
    StaticService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
