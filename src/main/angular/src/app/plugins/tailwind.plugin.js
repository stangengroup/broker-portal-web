module.exports = function () {
  return function ({e, addUtilities}) {
    let percents = [
      {
        key: '1/12',
        value: '8.33333333334%'
      },
      {
        key: '2/12',
        value: '16.66666666667%'
      },
      {
        key: '3/12',
        value: '25%'
      },
      {
        key: '4/12',
        value: '33.33333333334%'
      },
      {
        key: '5/12',
        value: '41.66666666667%'
      },
      {
        key: '6/12',
        value: '50%'
      },
      {
        key: '7/12',
        value: '58.33333333334%'
      },
      {
        key: '8/12',
        value: '66.66666666667%'
      },
      {
        key: '9/12',
        value: '75%'
      },
      {
        key: '10/12',
        value: '83.33333333334%'
      },
      {
        key: '11/12',
        value: '91.66666666667%'
      },
    //  10
      {
        key: '1/10',
        value: '10%'
      },
      {
        key: '2/10',
        value: '20%'
      },
      {
        key: '3/10',
        value: '30%'
      },
      {
        key: '4/10',
        value: '40%'
      },
      {
        key: '5/10',
        value: '50%'
      },
      {
        key: '6/10',
        value: '60%'
      },
      {
        key: '7/10',
        value: '70%'
      },
      {
        key: '8/10',
        value: '80%'
      },
      {
        key: '9/10',
        value: '90%'
      }
    ];

    const heightUtilities = percents.map(item => {
      return {
        [`.h-${e(item.key)}`]: {
          height: item.value
        },
        [`.w-${e(item.key)}`]: {
          width: item.value
        }
      }
    });

    addUtilities(heightUtilities, {
      variants: ['responsive'],
    });
  }
}
