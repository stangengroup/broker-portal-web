import {Injectable, OnDestroy} from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { MsalBroadcastService, MsalGuard, MsalService } from '@azure/msal-angular';
import { Observable, of } from 'rxjs';
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MsalContainerGuard implements OnDestroy {

  private subscription!: Subscription;

  constructor(
    private msalGuard: MsalGuard,
    private router: Router) {
  }

  canLoad(): Promise<boolean> {

    return new Promise((resolve, reject) => {
      this.subscription = this.msalGuard.canLoad()
      .subscribe({
        next: (val) => {
          console.log(val);
          resolve(val);
          return of(val);
        },
        error: (err) => {
          console.error(err);
          resolve(false);
          // this.router.navigate(['']);
          return of(false);
        }
      });
    });

  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

}
