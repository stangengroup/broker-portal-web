import { TestBed } from '@angular/core/testing';

import { MsalContainerGuard } from './msal-container.guard';

describe('MsalContainerGuard', () => {
  let guard: MsalContainerGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MsalContainerGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
