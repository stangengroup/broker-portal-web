import {Component, Inject, OnDestroy, OnInit, Output} from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { filter, map, Subject, Subscriber, Subscription, takeUntil } from 'rxjs';
import { LoginApiResponse } from '../api/login.api.class';
import { Login } from '../interfaces/login/login';
import { Session } from '../interfaces/session/session';
import { LoginService } from '../services/login/login.service';
import { SnackbarService } from '../services/snackbar/snackbar.service';
import { SetSession } from '../state/actions/session.action';
import { MsalBroadcastService, MsalGuardConfiguration, MsalService, MSAL_GUARD_CONFIG } from '@azure/msal-angular';
import { EventMessage, EventType, AuthenticationResult, InteractionStatus, RedirectRequest, PopupRequest, InteractionType } from '@azure/msal-browser';

import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  @Output() login = new EventEmitter<boolean>();

  // // formControl: FormControl = new FormControl();
  // // loginForm: FormGroup;
  // // formSubmitted: boolean = false;
  // // login!: Login;
  // // loading: boolean = false;
  // // session!: Session;

  // // subscribable!: Subscription;

  // // constructor(
  // //   private fb: FormBuilder,
  // //   private loginService: LoginService,
  // //   private router: Router,
  // //   private store: Store,
  // //   private snackBarService: SnackbarService
  // // ) {
  // //   this.loginForm = this.fb.group({
  // //     username: ['', [Validators.required, Validators.email]],
  // //     password: ['', [Validators.required]]
  // //   });
  // // }

  // // ngOnInit(): void {
  // // }

  // // submitForm(): void {
  // //   console.log('submit form...');
  // //   this.loading = true;
  // //   this.formSubmitted = true;

  // //   this.login = {
  // //     username: this.username?.value,
  // //     password: this.password?.value
  // //   };

  // //   this.subscribable = this.loginService
  // //     .login(this.login)
  // //     .subscribe({
  // //       next: (res: LoginApiResponse) => {
  // //         this.loading = false;
  // //         console.log('Observable response', res);
  // //         if (res.data) {
  // //           this.snackBarService.showSnackBar('Successful login', 'Welcome');
  // //           this.session = {
  // //             token: res.data
  // //           };
  // //           this.store.dispatch(new SetSession(this.session));
  // //           this.router.navigate(['/home']);
  // //         } else {
  // //           this.snackBarService.showSnackBar(res.message);
  // //         }
  // //       },
  // //       error: (err) => {
  // //         this.loading = false;
  // //         this.snackBarService.showSnackBar(err);
  // //         console.log('Observable error response', err);
  // //       }
  // //     });
  // // }

  // // get username() {
  // //   return this.loginForm.controls['username'];
  // // }

  // // get password() {
  // //   return this.loginForm.controls['password'];
  // // }

  // // ngOnDestroy(): void {
  // //   this.subscribable?.unsubscribe();
  // // }

  // loginDisplay = false;
  // displayedColumns: string[] = ['claim', 'value'];
  // dataSource: any =[];
  // isIframe = false;
  // private readonly _destroying$ = new Subject<void>();

  // constructor(
  //   @Inject(MSAL_GUARD_CONFIG) private msalGuardConfig: MsalGuardConfiguration,
  //   private authService: MsalService,
  //   private msalBroadcastService: MsalBroadcastService,
  // ) { }

  // ngOnInit(): void {

  //   this.isIframe = window !== window.parent && !window.opener;

  //   /**
  //    * You can subscribe to MSAL events as shown below. For more info,
  //    * visit: https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-angular/docs/v2-docs/events.md
  //    */
  //   this.msalBroadcastService.inProgress$
  //   .pipe(
  //     filter((status: InteractionStatus) => status === InteractionStatus.None),
  //     takeUntil(this._destroying$)
  //   )
  //   .subscribe(() => {
  //     this.setLoginDisplay();
  //   });

  //   this.msalBroadcastService.msalSubject$
  //     .pipe(
  //       filter((msg: EventMessage) => msg.eventType === EventType.LOGIN_SUCCESS),
  //       takeUntil(this._destroying$)
  //     )
  //     .subscribe((result: EventMessage) => {
  //       console.log(result);
  //       const payload = result.payload as AuthenticationResult;
  //       this.authService.instance.setActiveAccount(payload.account);
  //     });

  //     this.msalBroadcastService.inProgress$
  //     .pipe(
  //       filter((status: InteractionStatus) => status === InteractionStatus.None)
  //     )
  //     .subscribe(() => {
  //       this.setLoginDisplay();
  //       this.checkAndSetActiveAccount();
  //       this.getClaims(this.authService.instance.getActiveAccount()?.idTokenClaims)
  //     });

  //     this.login();
  // }

  // checkAndSetActiveAccount() {
  //   /**
  //    * If no active account set but there are accounts signed in, sets first account to active account
  //    * To use active account set here, subscribe to inProgress$ first in your component
  //    * Note: Basic usage demonstrated. Your app may require more complicated account selection logic
  //    */
  //   let activeAccount = this.authService.instance.getActiveAccount();

  //   if (!activeAccount && this.authService.instance.getAllAccounts().length > 0) {
  //     let accounts = this.authService.instance.getAllAccounts();
  //     this.authService.instance.setActiveAccount(accounts[0]);
  //   }
  // }

  // setLoginDisplay() {
  //   this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  // }

  // login(userFlowRequest?: RedirectRequest | PopupRequest) {
  //   if (this.msalGuardConfig.interactionType === InteractionType.Popup) {
  //     if (this.msalGuardConfig.authRequest) {
  //       this.authService.loginPopup({...this.msalGuardConfig.authRequest, ...userFlowRequest} as PopupRequest)
  //         .subscribe((response: AuthenticationResult) => {
  //           this.authService.instance.setActiveAccount(response.account);
  //         });
  //     } else {
  //       this.authService.loginPopup(userFlowRequest)
  //         .subscribe((response: AuthenticationResult) => {
  //           this.authService.instance.setActiveAccount(response.account);
  //         });
  //     }
  //   } else {
  //     if (this.msalGuardConfig.authRequest){
  //       this.authService.loginRedirect({...this.msalGuardConfig.authRequest, ...userFlowRequest} as RedirectRequest);
  //     } else {
  //       this.authService.loginRedirect(userFlowRequest);
  //     }
  //   }
  // }

  // logout() {
  //   this.authService.logout();
  // }

  // // editProfile() {
  // //   let editProfileFlowRequest = {
  // //     scopes: ["openid"],
  // //     authority: b2cPolicies.authorities.editProfile.authority,
  // //   };

  // //   this.login(editProfileFlowRequest);
  // // }


  // getClaims(claims: any) {
  //   this.dataSource = [
  //     {id: 1, claim: "Display Name", value: claims ? claims['name'] : null},
  //     {id: 2, claim: "Object ID", value: claims ? claims['oid']: null},
  //     {id: 3, claim: "Job Title", value: claims ? claims['jobTitle']: null},
  //     {id: 4, claim: "City", value: claims ? claims['city']: null},
  //   ];
  // }

  // ngOnDestroy(): void {
  //   this._destroying$.next(undefined);
  //   this._destroying$.complete();
  // }

  loginDisplay = false;
  private subscription!: Subscription;
  private subscription2!: Subscription;

  constructor() {
    console.log('Login constructor');
  }

  ngOnInit(): void {
    console.log('Login init');
    // this.subscription = this.msalBroadcastService.msalSubject$
    //   .pipe(
    //     filter((msg: EventMessage) => msg.eventType === EventType.LOGIN_SUCCESS),
    //   )
    //   .subscribe((result: EventMessage) => {
    //     console.log(result);
    //   });
    //
    // this.subscription2 = this.msalBroadcastService.inProgress$
    //   .pipe(
    //     filter((status: InteractionStatus) => status === InteractionStatus.None)
    //   )
    //   .subscribe(() => {
    //     this.setLoginDisplay();
    //   });
    //
    //   this.setLoginDisplay();
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
  }

  popupLogin(): void {
    this.login.emit(true);
  }

  // setLoginDisplay() {
  //   this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  // }

}
