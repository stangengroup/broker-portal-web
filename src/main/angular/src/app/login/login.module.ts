import { NgModule } from '@angular/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from '../services/login/login.service';
import { LoginApi } from '../api/login.api.class';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AuthService } from '../services/auth/auth.service';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        LoginRoutingModule,
        HttpClientModule,
        MatTableModule,
        MatToolbarModule,
        CommonModule,
        BrowserModule
    ],
    exports: [
        LoginComponent
    ],
    providers: [
        LoginService,
        LoginApi,
        AuthService
    ]
})
export class LoginModule { }
