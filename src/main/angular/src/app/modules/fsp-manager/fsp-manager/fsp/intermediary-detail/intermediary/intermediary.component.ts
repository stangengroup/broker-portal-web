import {Component, OnDestroy, OnInit, Type} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs';
import { FspDetails } from 'src/app/interfaces/fsp/FspDetails';
import { Address } from 'src/app/interfaces/generic/Address';
import { BankingDetails } from 'src/app/interfaces/generic/BankingDetails';
import { Commission } from 'src/app/interfaces/generic/Commission';
import { Fees } from 'src/app/interfaces/generic/Fees';
import { IntermediaryDetail } from 'src/app/interfaces/intermediary/IntermediaryDetail';
import {IntermediarySpecificDetailComponent} from "./intermediary-specific-detail/intermediary-specific-detail.component";
import {IntermediaryPolicyListComponent} from "./intermediary-policy-list/intermediary-policy-list.component";

export interface TabDetailIntermediary {
  name: string;
  code: string;
  route: string;
  disabled: boolean;
  component: Type<any>,
}

@Component({
  selector: 'app-intermediary',
  templateUrl: './intermediary.component.html',
  styleUrls: ['./intermediary.component.scss']
})
export class IntermediaryComponent implements OnInit, OnDestroy {

  tabDetail: TabDetailIntermediary[] = [
    {
      name: 'Intermediary Details',
      code: 'INTERMEDIARY_DETAIL',
      route: `/intermediary-view/undefined/detail`,
      disabled: false,
      component: IntermediarySpecificDetailComponent
    },
    {
      name: 'Policy List',
      code: 'INTERMEDIARY_POLICY_LIST',
      route: `/intermediary-view/undefined/policy-list`,
      disabled: false,
      component: IntermediaryPolicyListComponent
    }
  ];

  private paramSubscription?: Subscription | null;
  private paramSubscription2?: Subscription;
  public fspId!: string;
  public intermediaryId!: string;
  public intermediaryName: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.paramSubscription = this.route
      .parent
      ?.parent
      ?.parent
      ?.parent
      ?.params
      .subscribe((params: any) => {
        console.log(`fsp id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.fspId = params['id'];
        }
      });

    this.paramSubscription2 = this.route
      .params
      .subscribe((params: any) => {
        console.log(`intermediary id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.intermediaryId = params['id'];
          this.tabDetail = [
            {
              name: 'Intermediary Details',
              code: 'INTERMEDIARY_DETAIL',
              route: `/intermediary-view/${this.intermediaryId}/detail`,
              disabled: false,
              component: IntermediarySpecificDetailComponent
            },
            {
              name: 'Policy List',
              code: 'INTERMEDIARY_POLICY_LIST',
              route: `/intermediary-view/${this.intermediaryId}/policy-list`,
              disabled: false,
              component: IntermediaryPolicyListComponent
            }
          ];
          // this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view/${this.intermediaryId}/detail`]);
        } else {
          this.tabDetail = [
            {
              name: 'Intermediary Details',
              code: 'INTERMEDIARY_DETAIL',
              route: `/intermediary-view/${this.intermediaryId}/detail`,
              disabled: false,
              component: IntermediarySpecificDetailComponent
            }
          ];
          // this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view/${this.intermediaryId}/detail`]);
        }
      });
  }

  ngOnDestroy() {
    this.paramSubscription?.unsubscribe();
    this.paramSubscription2?.unsubscribe();
  }

  ngOnInit() {
  }

  intermediaryRoute(): string {
    return `/home/fsp-manager/fsp/${this.fspId}/intermediary`;
  }

}
