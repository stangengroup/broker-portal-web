import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import {IntermediaryDescriptor} from "../../../../../../interfaces/intermediary/IntermediaryDescriptor";
import {KiService} from "../../../../../../services/key-individual/ki.service";
import {SnackbarService} from "../../../../../../services/snackbar/snackbar.service";
import {IntermediaryService} from "../../../../../../services/intermediary/intermediary.service";
import {KeyIndividualDescriptor} from "../../../../../../interfaces/key-individual/KeyIndividualDescriptor";
import {HttpErrorResponse} from "@angular/common/http";
import {StaticService} from "../../../../../../services/static/static.service";
import {Subscription} from "rxjs";
import {ListCount} from "../../../../../../interfaces/generic/ListCount";
import {ConfigService} from "../../../../../../services/config/config.service";
import {DataService} from "../../../../../../services/shared/data/data.service";
import {FspDetails} from "../../../../../../interfaces/fsp/FspDetails";

@Component({
  selector: 'app-intermediary-list',
  templateUrl: './intermediary-list.component.html',
  styleUrls: ['./intermediary-list.component.scss']
})
export class IntermediaryListComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['name', 'brokerCode', 'emailAddress', 'contactNumber', 'created', 'actions'];
  dataSource!: MatTableDataSource<IntermediaryDescriptor>;
  intermediaryList!: IntermediaryDescriptor[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  fspId!: string;
  public limit: number = 5;
  public page: number = 0;
  public loading: boolean = true;
  public totalElements!: number;

  private subscription!: Subscription | undefined;
  private subscription2!: Subscription;
  private subscription3!: Subscription;
  private subscription4!: Subscription;
  private subscription5!: Subscription;

  public fspDetail: FspDetails;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private intermediaryService: IntermediaryService,
    private snackBarService: SnackbarService,
    private staticService: StaticService,
    private configService: ConfigService,
    private dataService: DataService
  ) {
    this.getSavedLimit();
    // this.staticService.loading.next(true);

    this.subscription = this.route
    .parent
    ?.parent
    ?.parent
    ?.parent
    ?.params
    .subscribe((params) => {
      console.log(`fsp id | ${params['id']}`);
      this.fspId = params['id'];
    });

    console.log(this.dataService.activeFsp?.getValue());
    this.fspDetail = this.dataService.activeFsp?.getValue();
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
    this.subscription5?.unsubscribe();
  }

  getSavedLimit(): void {
    const limit = this.configService.getUserPreferenceFromStorage('pagination-limit');
    if (limit == null) {
      return;
    }
    if (parseInt(limit, 10) > 0) {
      this.limit = parseInt(limit, 10);
    }
  }

  ngOnInit(): void {
    this.updateTable(false);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navigateToIntermediary(intermediaryData: IntermediaryDescriptor): void {
    this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view/${intermediaryData.id}`]);
  }

  addNewIntermediary(): void {
    this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view`]);
  }

  public async asyncUpdateTable(updatePage?: boolean): Promise<any> {
    this.loading = true;
    // this.staticService.loading.next(true);
    this.subscription2 = this.intermediaryService
      .listIntermediary(parseInt(this.fspId, 10), this.limit, this.page)
      .subscribe({
        next: (intermediaryList: IntermediaryDescriptor[]) => {
          this.intermediaryList = intermediaryList;
          console.log(this.intermediaryList);
          this.dataSource = new MatTableDataSource(this.intermediaryList);

          // -- Count --
          this.subscription3 = this.intermediaryService
            .listCount(parseInt(this.fspId, 10))
            .subscribe({
              next: (res: ListCount) => {
                // this.staticService.loading.next(false);
                this.loading = false;
                this.totalElements = res.count;
                if (!updatePage) {
                  this.paginator.pageIndex = 0;
                }
                return;
              },
              error: (err: HttpErrorResponse) => {
                // this.staticService.loading.next(false);
                this.loading = false;
                return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
              }
            });

          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });
  }

  private updateTable(updatePage: boolean): void {
    this.loading = true;
    // this.staticService.loading.next(true);
    this.subscription4 = this.intermediaryService
      .listIntermediary(parseInt(this.fspId, 10), this.limit, this.page)
      .subscribe({
        next: (intermediaryList: IntermediaryDescriptor[]) => {
          this.intermediaryList = intermediaryList;
          console.log(this.intermediaryList);
          this.dataSource = new MatTableDataSource(this.intermediaryList);
          // Paging
          this.dataSource.sort = this.sort;
          if (!updatePage) {
            this.paginator.pageIndex = 0;
          }
          this.loading = false;

          // -- Count --
          this.subscription5 = this.intermediaryService
            .listCount(parseInt(this.fspId, 10))
            .subscribe({
              next: (res: ListCount) => {
                // this.staticService.loading.next(false);
                return this.totalElements = res.count;
              },
              error: (err: HttpErrorResponse) => {
                // this.staticService.loading.next(false);
                return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
              }
            });

          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar('Something went wrong', 'Please try again');
        }
      });
  }

  async setPageSizeOptions(setPageSizeOptionsInput: PageEvent): Promise<PageEvent> {
    this.page = setPageSizeOptionsInput.pageIndex;
    this.limit = setPageSizeOptionsInput.pageSize;
    this.paginator.pageIndex = this.page;
    this.paginator.pageSize = this.limit;
    this.configService.setUserPreferenceFromStorage('pagination-limit', this.limit);
    await this.asyncUpdateTable(true);
    return setPageSizeOptionsInput;
  }

}
