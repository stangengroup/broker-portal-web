import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntermediarySpecificDetailRoutingModule } from './intermediary-specific-detail-routing.module';
import { IntermediarySpecificDetailComponent } from './intermediary-specific-detail.component';
import {SharedModule} from "../../../../../../../shared/shared.module";
import {MatDividerModule} from "@angular/material/divider";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {IntermediaryDocumentsComponent} from "../../intermediary-documents/intermediary-documents.component";
import {IntermediaryService} from "../../../../../../../services/intermediary/intermediary.service";
import {IntermediaryApi} from "../../../../../../../api/intermediary.api.class";
import {DocumentService} from "../../../../../../../services/document/document.service";
import {DocumentApi} from "../../../../../../../api/document.api.class";


@NgModule({
  declarations: [
    IntermediarySpecificDetailComponent,
    IntermediaryDocumentsComponent
  ],
  imports: [
    CommonModule,
    IntermediarySpecificDetailRoutingModule,
    SharedModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatIconModule
  ],
  providers: [
    IntermediaryService,
    IntermediaryApi,
    DocumentService,
    DocumentApi
  ]
})
export class IntermediarySpecificDetailModule { }
