import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntermediaryListComponent } from './intermediary-list.component';

const routes: Routes = [{ path: '', component: IntermediaryListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntermediaryListRoutingModule { }
