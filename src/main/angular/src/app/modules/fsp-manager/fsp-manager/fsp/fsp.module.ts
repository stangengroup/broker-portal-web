import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FspRoutingModule } from './fsp-routing.module';
import { FspComponent } from './fsp.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatTabsModule } from '@angular/material/tabs';
import {FspDetailModule} from "./fsp-detail/fsp-detail.module";
import {KiDetailModule} from "./ki-detail/ki-detail.module";
import {IntermediaryDetailModule} from "./intermediary-detail/intermediary-detail.module";

@NgModule({
  declarations: [
    FspComponent
  ],
  imports: [
    CommonModule,
    FspRoutingModule,
    SharedModule,
    MatTabsModule,
    FspDetailModule,
    KiDetailModule,
    IntermediaryDetailModule
  ]
})
export class FspModule { }
