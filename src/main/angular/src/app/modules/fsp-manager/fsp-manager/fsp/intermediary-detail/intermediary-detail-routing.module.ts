import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntermediaryDetailComponent } from './intermediary-detail.component';

const routes: Routes = [
  {
    path: '',
    component: IntermediaryDetailComponent,
    children: [
      {
        path: 'intermediary-list',
        loadChildren: () => import('./intermediary-list/intermediary-list.module').then(m => m.IntermediaryListModule)
      },
      {
        path: 'intermediary-view',
        loadChildren: () => import('./intermediary/intermediary.module').then(m => m.IntermediaryModule)
      },
      {
        path: 'intermediary-view/:id',
        loadChildren: () => import('./intermediary/intermediary.module').then(m => m.IntermediaryModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntermediaryDetailRoutingModule { }
