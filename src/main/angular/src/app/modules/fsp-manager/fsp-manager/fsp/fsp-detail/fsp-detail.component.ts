import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom, Subscription } from 'rxjs';
import { FspDetails } from 'src/app/interfaces/fsp/FspDetails';
import { Address } from 'src/app/interfaces/generic/Address';
import { BankingDetails } from 'src/app/interfaces/generic/BankingDetails';
import { Commission } from 'src/app/interfaces/generic/Commission';
import { Fees } from 'src/app/interfaces/generic/Fees';
import { FspService } from 'src/app/services/fsp/fsp.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import {UtilityService} from "../../../../../services/utility/utility.service";
import {StaticService} from "../../../../../services/static/static.service";
import {ConfigDetail} from "../../../../../interfaces/config/ConfigDetail";
import {PostResponse} from "../../../../../interfaces/generic/PostResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {DialogService} from "../../../../../services/shared/dialog/dialog.service";
import {DialogData} from "../../../../../interfaces/generic/DialogData";
import {environment} from "../../../../../../environments/environment";
import {MatSelectChange} from "@angular/material/select";
import {DataService} from "../../../../../services/shared/data/data.service";

@Component({
  selector: 'app-fsp-detail',
  templateUrl: './fsp-detail.component.html',
  styleUrls: ['./fsp-detail.component.scss']
})
export class FspDetailComponent implements OnInit, OnDestroy {

  fspStatuses: string[] = ['Pending', 'Approved', 'Disabled'];
  banks: string[] = ['FNB', 'Standard Bank', 'Nedbank', 'Absa', 'Investec', 'African Bank', 'Old Mutual', 'Discovery'];
  accountTypes: string[] = ['Cheque', 'Savings'];
  commissionTypes: string[] = ['Now', 'As and When'];
  paramSubscription?: Subscription;

  fspForm!: FormGroup;
  loading: boolean = true;
  fspId!: string;
  fspActive!: boolean;
  fspInactive!: boolean;

  fspDetails!: FspDetails;
  address!: Address;
  bankingDetails!: BankingDetails;
  fees!: Fees;
  commission!: Commission;

  subscription!: Subscription;
  subscription2!: Subscription;
  subscription3!: Subscription;
  subscription4!: Subscription;
  subscription5!: Subscription;
  subscription6!: Subscription;
  commissionError!: string | null;

  public configData!: ConfigDetail;

  public upfrontCommissionHintMsg: string = 'Upfront + as and when commissions should equate to 100';
  public asAndWhenHintMsg: string = 'Upfront + as and when commissions should equate to 100';

  public fspDoc: any;
  public scrollToDocuments!: boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private fspService: FspService,
    private snackBarService: SnackbarService,
    public utilityService: UtilityService,
    private staticService: StaticService,
    private dialogService: DialogService,
    private dataService: DataService,
    private ref: ChangeDetectorRef
  ) {
    // this.staticService.loading.next(true);
    this.createForm();

    this.paramSubscription = this.route
    .parent
    ?.params
    .subscribe((params) => {
      console.log(`fsp id | ${params['id']}`);
      if (parseInt(params['id'])) {
        this.fspId = params['id'];
        this.setExistingFspData(null);
      } else {
        this.loading = false;
        // this.staticService.loading.next(false);
      }
    });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
    this.subscription5?.unsubscribe();
    this.subscription6?.unsubscribe();
    this.paramSubscription?.unsubscribe();
  }

  setExistingFspData(fspDetail: FspDetails | null): void {
    if (fspDetail) {
      // Set active fsp
      this.fspDetails = fspDetail;
      this.dataService.setActiveFsp(fspDetail);

      console.log(this.configData.fspStatusList[this.configData.fspStatusList.findIndex(value => value.id === fspDetail.fspStatus.id)]);
      this.fspStatus.setValue(this.configData.fspStatusList[this.configData.fspStatusList.findIndex(value => value.id === fspDetail.fspStatus.id)]);
      this.fspActive = this.fspStatus.value?.name?.toUpperCase() === 'APPROVED';
      this.tradingAs.setValue(fspDetail.tradingAs);
      this.fspDomainName.setValue(fspDetail.domainName);
      this.companyRegNumber.setValue(fspDetail.companyRegNumber);
      this.vatNumber.setValue(fspDetail.vatNumber);
      this.taxNumber.setValue(fspDetail.taxNumber);
      this.fspRegNumber.setValue(fspDetail.fspRegNumber);
      this.emailAddress.setValue(fspDetail.emailAddress);
      this.alternativeEmailAddress.setValue(fspDetail.altEmailAddress);
      this.workNumber.setValue(fspDetail.workNumber);
      this.mobileNumber.setValue(fspDetail.mobileNumber);
      this.alternativeContactNumber.setValue(fspDetail.altContactNumber);
      this.streetNumber.setValue(fspDetail.address.streetNumber);
      this.street1.setValue(fspDetail.address.streetName);
      this.street2.setValue(fspDetail.address.streetName2);
      this.suburb.setValue(fspDetail.address.suburb);
      this.town.setValue(fspDetail.address.town);
      this.province.setValue(fspDetail.address.province);
      this.postalCode.setValue(fspDetail.address.postalCode);
      this.bankName.setValue(fspDetail.bankingDetails.bankName);
      this.branchCode.setValue(fspDetail.bankingDetails.branchCode);
      this.accountHolderName.setValue(fspDetail.bankingDetails.accountHolderName);
      this.accountType.setValue(this.accountTypes[this.accountTypes.findIndex(value => value === fspDetail.bankingDetails.accountType)]);
      this.accountNumber.setValue(fspDetail.bankingDetails.accountNumber);
      this.feesPercentage.setValue(fspDetail.fees.percentage);
      this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.id === fspDetail.commission.commissionStructureType.id)]);
      this.upfrontCommission.setValue(fspDetail.commission.upfront);
      this.asAndWhenCommission.setValue(fspDetail.commission.asAndWhen);

      // Check inactive
      if (this.fspStatus.value?.name?.toUpperCase() === 'INACTIVE') {
        this.setFormInactive();
      }

      this.loading = false;
      // // this.staticService.loading.next(false);
    } else {
      this.subscription = this.fspService
        .getFsp(parseInt(this.fspId, 10))
        .subscribe({
          next: (fspDetail: FspDetails) => {
            // Set active fsp
            this.fspDetails = fspDetail;
            this.dataService.setActiveFsp(fspDetail);

            console.log(this.configData.fspStatusList[this.configData.fspStatusList.findIndex(value => value.id === fspDetail.fspStatus.id)]);
            this.fspStatus.setValue(this.configData.fspStatusList[this.configData.fspStatusList.findIndex(value => value.id === fspDetail.fspStatus.id)]);
            this.fspActive = this.fspStatus.value?.name?.toUpperCase() === 'APPROVED';
            this.tradingAs.setValue(fspDetail.tradingAs);
            this.fspDomainName.setValue(fspDetail.domainName);
            this.companyRegNumber.setValue(fspDetail.companyRegNumber);
            this.vatNumber.setValue(fspDetail.vatNumber);
            this.taxNumber.setValue(fspDetail.taxNumber);
            this.fspRegNumber.setValue(fspDetail.fspRegNumber);
            this.emailAddress.setValue(fspDetail.emailAddress);
            this.alternativeEmailAddress.setValue(fspDetail.altEmailAddress);
            this.workNumber.setValue(fspDetail.workNumber);
            this.mobileNumber.setValue(fspDetail.mobileNumber);
            this.alternativeContactNumber.setValue(fspDetail.altContactNumber);
            this.streetNumber.setValue(fspDetail.address.streetNumber);
            this.street1.setValue(fspDetail.address.streetName);
            this.street2.setValue(fspDetail.address.streetName2);
            this.suburb.setValue(fspDetail.address.suburb);
            this.town.setValue(fspDetail.address.town);
            this.province.setValue(fspDetail.address.province);
            this.postalCode.setValue(fspDetail.address.postalCode);
            this.bankName.setValue(fspDetail.bankingDetails.bankName);
            this.branchCode.setValue(fspDetail.bankingDetails.branchCode);
            this.accountHolderName.setValue(fspDetail.bankingDetails.accountHolderName);
            this.accountType.setValue(this.accountTypes[this.accountTypes.findIndex(value => value === fspDetail.bankingDetails.accountType)]);
            this.accountNumber.setValue(fspDetail.bankingDetails.accountNumber);
            this.feesPercentage.setValue(fspDetail.fees.percentage);
            this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.id === fspDetail.commission.commissionStructureType.id)]);
            this.upfrontCommission.setValue(fspDetail.commission.upfront);
            this.asAndWhenCommission.setValue(fspDetail.commission.asAndWhen);

            // Check inactive
            if (this.fspStatus.value?.name?.toUpperCase() === 'INACTIVE') {
              this.setFormInactive();
            }

            this.loading = false;
            // // this.staticService.loading.next(false);
          },
          error: (err: HttpErrorResponse) => {
        return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        this.loading = false;
        // this.staticService.loading.next(false);
      }
    });
    }

  }

  get fspStatus(): AbstractControl {
    return this.fspForm.controls['fspStatus'];
  }

  get tradingAs(): AbstractControl {
    return this.fspForm.controls['tradingAs'];
  }

  get fspDomainName(): AbstractControl {
    return this.fspForm.controls['fspDomainName'];
  }

  get companyRegNumber(): AbstractControl {
    return this.fspForm.controls['companyRegNumber'];
  }

  get vatNumber(): AbstractControl {
    return this.fspForm.controls['vatNumber'];
  }

  get taxNumber(): AbstractControl {
    return this.fspForm.controls['taxNumber'];
  }

  get fspRegNumber(): AbstractControl {
    return this.fspForm.controls['fspRegNumber'];
  }

  get emailAddress(): AbstractControl {
    return this.fspForm.controls['emailAddress'];
  }

  get alternativeEmailAddress(): AbstractControl {
    return this.fspForm.controls['alternativeEmailAddress'];
  }

  get workNumber(): AbstractControl {
    return this.fspForm.controls['workNumber'];
  }

  get mobileNumber(): AbstractControl {
    return this.fspForm.controls['mobileNumber'];
  }

  get alternativeContactNumber(): AbstractControl {
    return this.fspForm.controls['alternativeContactNumber'];
  }

  get streetNumber(): AbstractControl {
    return this.fspForm.controls['streetNumber'];
  }

  get street1(): AbstractControl {
    return this.fspForm.controls['street1'];
  }

  get street2(): AbstractControl {
    return this.fspForm.controls['street2'];
  }

  get suburb(): AbstractControl {
    return this.fspForm.controls['suburb'];
  }

  get town(): AbstractControl {
    return this.fspForm.controls['town'];
  }

  get province(): AbstractControl {
    return this.fspForm.controls['province'];
  }

  get postalCode(): AbstractControl {
    return this.fspForm.controls['postalCode'];
  }

  get bankName(): AbstractControl {
    return this.fspForm.controls['bankName'];
  }

  get branchCode(): AbstractControl {
    return this.fspForm.controls['branchCode'];
  }

  get accountHolderName(): AbstractControl {
    return this.fspForm.controls['accountHolderName'];
  }

  get accountType(): AbstractControl {
    return this.fspForm.controls['accountType'];
  }

  get accountNumber(): AbstractControl {
    return this.fspForm.controls['accountNumber'];
  }

  get feesPercentage(): AbstractControl {
    return this.fspForm.controls['feesPercentage'];
  }

  get commissionType(): AbstractControl {
    return this.fspForm.controls['commissionType'];
  }

  get upfrontCommission(): AbstractControl {
    return this.fspForm.controls['upfrontCommission'];
  }

  get asAndWhenCommission(): AbstractControl {
    return this.fspForm.controls['asAndWhenCommission'];
  }

  createForm(): void {
    this.fspForm = this.fb.group({
      fspStatus: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]],
      tradingAs: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(100)]],
      fspDomainName: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^(?!.* .*)(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.)+[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]$/)]],
      companyRegNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "/]*$/), Validators.minLength(14), Validators.maxLength(14)]],
      vatNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      taxNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      fspRegNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      emailAddress: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.email, Validators.maxLength(200)]],
      alternativeEmailAddress: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.email, Validators.maxLength(200)]],
      workNumber: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(10), Validators.min(100000000), Validators.max(999999999999)]],
      mobileNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(10), Validators.min(100000000), Validators.max(999999999999)]],
      alternativeContactNumber: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(10), Validators.min(100000000), Validators.max(999999999999)]],
      streetNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.min(0), Validators.max(999999), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(6)]],
      street1: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      street2: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      suburb: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      town: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      province: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(100)]],
      postalCode: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.max(9999999999), Validators.minLength(4), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      bankName: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      branchCode: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(4), Validators.maxLength(6), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      accountHolderName: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      accountType: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(20)]],
      accountNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(9), Validators.maxLength(17), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      feesPercentage: ['', [Validators.required, Validators.min(0), Validators.max(100), Validators.maxLength(3), Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      commissionType: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(240)]],
      upfrontCommission: ['', [Validators.required, Validators.min(0), Validators.max(100), Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(3)]],
      asAndWhenCommission: ['', [Validators.required, Validators.min(0), Validators.max(100), Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(3)]]
    });

    this.fspForm.valueChanges
      .subscribe((val) => {

        // Commissions

        if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || (parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) > 100) {
          this.upfrontCommission.setErrors([Validators.max]);
          this.asAndWhenCommission.setErrors([Validators.max]);
          this.commissionError = 'Commissions are over 100%';
        } else {
          if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || !((parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) < 100)) {
            this.upfrontCommission.setErrors(null);
            this.asAndWhenCommission.setErrors(null);
            this.commissionError = null;
          }
        }

        if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || (parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) < 100) {
          this.upfrontCommission.setErrors([Validators.min]);
          this.asAndWhenCommission.setErrors([Validators.min]);
          this.commissionError = 'Commissions are less than 100%';
        } else {
          if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || !((parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) > 100)) {
            this.upfrontCommission.setErrors(null);
            this.asAndWhenCommission.setErrors(null);
            this.commissionError = null;
          }
        }

        // if (((parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10))) < 100) {
        //   this.upfrontCommission.setErrors([Validators.min]);
        //   this.asAndWhenCommission.setErrors([Validators.min]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.upfrontCommission.setErrors(null);
        //   this.asAndWhenCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.upfrontCommission?.value, 10) > 100) {
        //   this.upfrontCommission.setErrors([Validators.max]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.upfrontCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.asAndWhenCommission?.value, 10) > 100) {
        //   this.asAndWhenCommission.setErrors([Validators.max]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.asAndWhenCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.upfrontCommission?.value, 10) < 0) {
        //   this.upfrontCommission.setErrors([Validators.min]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.upfrontCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.asAndWhenCommission?.value, 10) < 0) {
        //   this.asAndWhenCommission.setErrors([Validators.min]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.asAndWhenCommission.setErrors(null);
        // }

      });

  }

  saveFsp(): void {

    this.loading = true;
    // this.staticService.loading.next(true);

    if (!this.fspId) {
      this.fspStatus.setValue(this.configData.fspStatusList[this.configData.fspStatusList.findIndex(value => value.name.toUpperCase() === 'PENDING')]);
    }

    this.address = {
      postalCode: this.postalCode?.value?.toString(),
      province: this.province?.value,
      streetName: this.street1?.value,
      streetName2: this.street2?.value,
      streetNumber: this.streetNumber?.value,
      suburb: this.suburb?.value,
      town: this.town?.value
    };

    this.bankingDetails = {
      accountHolderName: this.accountHolderName?.value,
      accountNumber: this.accountNumber?.value,
      accountType: this.accountType?.value,
      bankName: this.bankName?.value,
      branchCode: this.branchCode?.value
    };

    this.commission = {
      asAndWhen: this.asAndWhenCommission?.value,
      commissionStructureType: this.commissionType?.value,
      upfront: this.upfrontCommission?.value
    };

    this.fees = {
      feeStructureType: this.configData.feeStructureTypeList[0],
      percentage: this.feesPercentage?.value
    };

    this.fspDetails = {
      address: this.address,
      commission: this.commission,
      bankingDetails: this.bankingDetails,
      fees: this.fees,
      altContactNumber: this.alternativeContactNumber?.value,
      companyRegNumber: this.companyRegNumber?.value,
      domainName: this.fspDomainName?.value,
      emailAddress: this.emailAddress?.value,
      fspRegNumber: this.fspRegNumber?.value,
      id: parseInt(this.fspId, 10),
      taxNumber: this.taxNumber?.value,
      tradingAs: this.tradingAs?.value,
      vatNumber: this.vatNumber?.value,
      workNumber: this.workNumber?.value,
      mobileNumber: this.mobileNumber?.value,
      altEmailAddress: this.alternativeEmailAddress?.value,
      fspStatus: this.fspStatus?.value
    };

    console.log(this.fspDetails);

    if (!this.fspId) {
      this.subscription2 = this.fspService
        .saveFsp(this.fspDetails)
        .subscribe({
          next: (resp: FspDetails) => {
            console.log(resp);
            // this.staticService.loading.next(false);
            this.snackBarService.showSnackBar('FSP Successfully saved, you can now create and edit KIs and Intermediaries', 'Please upload the relevant documents');
            this.dataService.setScrollToDocuments(true);
            this.router.navigate([`/home/fsp-manager/fsp/${resp.id}/fsp-detail`]);
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err: HttpErrorResponse) => {
            console.error(err);
            // this.staticService.loading.next(false);
            // if (err.status === 409) {
            //   this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            // }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            // this.staticService.loading.next(false);
            this.subscription?.unsubscribe();
          }
        });
    } else {
      this.subscription3 = this.fspService
        .updateFsp(this.fspDetails, parseInt(this.fspId, 10))
        .subscribe({
          next: (resp: FspDetails) => {
            console.log(resp);
            this.setExistingFspData(resp);
            // this.staticService.loading.next(false);
            this.snackBarService.showSnackBar('FSP Successfully edited', 'Success');
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err) => {
            // this.staticService.loading.next(false);
            console.error(err);
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            // this.staticService.loading.next(false);
            this.subscription?.unsubscribe();
          }
        });
    }

  }

  async ngOnInit() {

    this.configData = await firstValueFrom(this.staticService
      .configDetail);
      

    // In dev environment, fill with mock data
    if (!environment.production) {
      this.fspStatus.setValue("Pending")
      this.tradingAs.setValue("Company Inc");
      this.fspDomainName.setValue("Company.co.za");
      this.companyRegNumber.setValue("005323332");
      this.vatNumber.setValue("005323332");
      this.taxNumber.setValue("005323332");
      this.fspRegNumber.setValue("005323332");
      this.emailAddress.setValue("test@email.com");
      this.alternativeEmailAddress.setValue("test@email.com");
      this.workNumber.setValue("0000000000");
      this.mobileNumber.setValue("0000000000");
      this.alternativeContactNumber.setValue("0000000000");
      this.streetNumber.setValue("1");
      this.street1.setValue("Main St");
      this.street2.setValue("0039");
      this.suburb.setValue("Suburb");
      this.town.setValue("Town");
      this.province.setValue("Province");
      this.postalCode.setValue("0039");
      this.bankName.setValue(this.banks[0]);
      this.branchCode.setValue("000000");
      this.accountHolderName.setValue("Account Holder");
      this.accountType.setValue(this.accountTypes[0]);
      this.accountNumber.setValue("0000000000");
      this.feesPercentage.setValue(25);
      this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.name.toUpperCase() === 'HYBRID')]);
      this.upfrontCommission.setValue(50);
      this.asAndWhenCommission.setValue(50);

      console.log(this.fspForm);
    }

  }

  confirmFsp() {

    // this.staticService.loading.next(true);

    const dialogData: DialogData = {
      buttonText: {
        confirm: 'Save',
        cancel: 'Cancel'
      },
      description: 'Saving this FSP will add them to the system.',
      failureMesssage: '',
      successMessage: '',
      title: `Are you sure you want to save?`
    };

    this.subscription4 = this.dialogService
      .openDialog(dialogData)
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (result) {
            this.saveFsp();
          } else {
            // this.staticService.loading.next(false);
          }
        },
        error: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Error occurred during confirmation dialog');
        }
      })
  }

  confirmFspStatusChange($event: any) {

    // this.staticService.loading.next(true);

    const dialogData: DialogData = {
      buttonText: {
        confirm: 'Save',
        cancel: 'Cancel'
      },
      description: $event.value?.name.toUpperCase() === 'APPROVED' ? `Please make sure all required information and documents has been submitted.` : $event.value?.name.toUpperCase() === 'INACTIVE' ? `Changing the FSP status to disabled will result in disabling the KI's and Brokers listed under the FSP` : ``,
      failureMesssage: '',
      successMessage: '',
      title: `Are you sure you want to ${$event.value?.name.toUpperCase() === 'APPROVED' ? 'Approve' : $event.value?.name.toUpperCase() === 'INACTIVE' ? 'Disable' : ''}?`
    };

    this.subscription5 = this.dialogService
      .openDialog(dialogData)
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (result) {
            // If status === disabled... disable form and tabs.
            this.subscription6 = this.fspService
              .updateStatusFsp($event.value?.id, parseInt(this.fspId, 10))
              .subscribe({
                next: () => {
                  // this.staticService.loading.next(false);
                  if ($event.value?.name?.toUpperCase() === 'INACTIVE') {
                    this.fspDetails.fspStatus = $event.value;
                    this.dataService.setActiveFsp(this.fspDetails);
                    this.setFormInactive();
                  } else {
                    this.fspDetails.fspStatus = $event.value;
                    this.dataService.setActiveFsp(this.fspDetails);
                    this.setFormActive();
                  }
                  this.snackBarService.showSnackBar('FSP Status successfully changed', $event.value?.name);
                },
                error: (err: HttpErrorResponse) => {
                  // this.staticService.loading.next(false);
                  return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
                }
              });
          } else {
            // this.staticService.loading.next(false);
          }
        },
        error: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Error occurred during FSP Status change attempt');
        }
      })
  }

  selectCommissionType($event: MatSelectChange) {
    // Disabling depending on commission structure type
    console.log($event?.value?.name);
    if ($event.value?.name?.toUpperCase() === 'AS AND WHEN') {
      this.asAndWhenCommission.disable();
      this.asAndWhenCommission.updateValueAndValidity();
      this.upfrontCommission.disable();
      this.upfrontCommission.updateValueAndValidity();
      this.upfrontCommission.setValue(0);
      this.asAndWhenCommission.setValue(100);
      this.upfrontCommissionHintMsg = 'This input is disabled as As and When commission is selected';
      this.asAndWhenHintMsg = 'This input is disabled as the value is defaulted to 100%';
    }

    if ($event.value?.name?.toUpperCase() === 'UPFRONT') {
      this.upfrontCommission.disable();
      this.upfrontCommission.updateValueAndValidity();
      this.asAndWhenCommission.disable();
      this.asAndWhenCommission.updateValueAndValidity();
      this.upfrontCommission.setValue(100);
      this.asAndWhenCommission.setValue(0);
      this.upfrontCommissionHintMsg = 'This input is disabled as the value is defaulted to 100%';
      this.asAndWhenHintMsg = 'This input is disabled as upfront commission is selected';
    }

    if ($event.value?.name?.toUpperCase() === 'HYBRID') {
      this.asAndWhenCommission.enable();
      this.upfrontCommission.enable();
      this.asAndWhenCommission.updateValueAndValidity();
      this.upfrontCommission.updateValueAndValidity();
      this.upfrontCommission.setValue(50);
      this.asAndWhenCommission.setValue(50);
      this.upfrontCommissionHintMsg = 'Upfront + as and when commissions should equate to 100';
      this.asAndWhenHintMsg = 'Upfront + as and when commissions should equate to 100';
    }
  }

  setFormInactive(): void {
    this.fspForm.disable();
    this.fspStatus.enable();
    this.fspStatus.updateValueAndValidity();
    this.fspInactive = true;
  }

  setFormActive(): void {
    this.fspForm.enable();
    this.fspStatus.enable();
    this.fspStatus.updateValueAndValidity();
    this.fspInactive = false;
  }

  checkNumbers($event: KeyboardEvent) {
    const numbersArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    console.log($event);
    if (numbersArr.indexOf($event.key) === -1) {
      return $event.preventDefault();
    }
    return;
  }

  checkNumbersPaste($event: ClipboardEvent) {
    const regexNumbers = new RegExp(/^[0-9]{1,45}$/);
    console.log($event);
    if (!regexNumbers.test(($event.clipboardData?.getData('text') as string))) {
      this.snackBarService.showSnackBar('You are attempting to paste a non digit-only value into a digit-only field');
      return $event.preventDefault();
    }
  }

}
