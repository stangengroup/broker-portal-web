import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntermediarySpecificDetailComponent } from './intermediary-specific-detail.component';

const routes: Routes = [{ path: '', component: IntermediarySpecificDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntermediarySpecificDetailRoutingModule { }
