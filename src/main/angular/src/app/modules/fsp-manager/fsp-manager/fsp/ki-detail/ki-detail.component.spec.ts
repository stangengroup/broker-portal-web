import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KiDetailComponent } from './ki-detail.component';

describe('KiDetailComponent', () => {
  let component: KiDetailComponent;
  let fixture: ComponentFixture<KiDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KiDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
