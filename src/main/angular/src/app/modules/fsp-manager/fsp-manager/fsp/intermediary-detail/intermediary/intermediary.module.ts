import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntermediaryRoutingModule } from './intermediary-routing.module';
import { IntermediaryComponent } from './intermediary.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDividerModule } from '@angular/material/divider';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { IntermediaryDocumentsComponent } from '../intermediary-documents/intermediary-documents.component';
import {MatTabNav, MatTabsModule} from "@angular/material/tabs";
import {IntermediaryService} from "../../../../../../services/intermediary/intermediary.service";
import {IntermediaryApi} from "../../../../../../api/intermediary.api.class";
import {DocumentService} from "../../../../../../services/document/document.service";
import {DocumentApi} from "../../../../../../api/document.api.class";
import {PolicyService} from "../../../../../../services/policy/policy.service";
import {PolicyApiClass} from "../../../../../../api/policy.api.class";
import {MatTooltipModule} from "@angular/material/tooltip";


@NgModule({
  declarations: [
    IntermediaryComponent
  ],
  imports: [
    CommonModule,
    IntermediaryRoutingModule,
    SharedModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatIconModule,
    MatTabsModule,
    MatTooltipModule
  ],
  providers: [
    MatTabNav,
    IntermediaryService,
    IntermediaryApi,
    DocumentService,
    DocumentApi,
    PolicyService,
    PolicyApiClass
  ]
})
export class IntermediaryModule { }
