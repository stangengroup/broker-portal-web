import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DocumentDetails} from "../../../../../../interfaces/document/DocumentDetails";
import {ConfigDetail} from "../../../../../../interfaces/config/ConfigDetail";
import {DocumentService} from "../../../../../../services/document/document.service";
import {StaticService} from "../../../../../../services/static/static.service";
import {SnackbarService} from "../../../../../../services/snackbar/snackbar.service";
import {HttpErrorResponse} from "@angular/common/http";
import {firstValueFrom, Subscription} from "rxjs";
import {DataService} from "../../../../../../services/shared/data/data.service";

@Component({
  selector: 'app-ki-documents',
  templateUrl: './ki-documents.component.html',
  styleUrls: ['./ki-documents.component.scss']
})
export class KiDocumentsComponent implements OnInit, OnDestroy {

  fileName!: string;
  documents!: DocumentDetails[];
  public configData!: ConfigDetail;

  @Input("kiId") kiId!: string;
  @Input("kiActive") kiActive!: boolean;
  @Input("kiInactive") kiInactive!: boolean;
  scrollToDocuments!: boolean;
  loading: boolean = true;

  private subscription!: Subscription;
  private subscription2!: Subscription;
  private subscription3!: Subscription;

  constructor(
    private documentService: DocumentService,
    private staticService: StaticService,
    private snackBarService: SnackbarService,
    private dataService: DataService
  ) {
    // this.staticService.loading.next(true);

    if (this.dataService?.scrollToDocuments?.getValue()) {
      this.scrollToDocuments = true;
      this.dataService.scrollToDocuments.next(false);
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
  }

  async ngOnInit() {

    this.configData = await firstValueFrom(this.staticService
      .configDetail);

    this.listDocuments();
  }

  viewDocument(document: DocumentDetails): void {
    window.open(document.url, "_blank");
  }

  listDocuments(): void {
    this.loading = true;
    // this.staticService.loading.next(true);
    this.subscription = this.documentService
      .listByKeyIndividual(parseInt(this.kiId, 10))
      .subscribe({
        next: (val: DocumentDetails[]) => {
          this.documents = val;
          if (this.scrollToDocuments) {
            setTimeout(() => {
              const docEl = (document.getElementById('ki-documents') as HTMLElement);
              console.log(docEl);
              this.scroll(docEl);
            }, 250)
          }
          if (val.filter(value => value.url != null).length === val.length) {
            // If all documents are uploaded
            if (!this.kiActive) {
              this.snackBarService.showSnackBar('All documents have been uploaded', 'You should consider changing this FSP status as approved');
            }
          }
          this.loading = false;
          // this.staticService.loading.next(false);
        },
        error: (err: HttpErrorResponse) => {
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          this.loading = false;
          // this.staticService.loading.next(false);
        }
      });
  }


  replaceDocument(image: any, document: DocumentDetails): void {
    // this.staticService.loading.next(true);
    const imageData = new FormData();
    imageData.set('image', image);
    this.subscription2 = this.documentService
      .saveByKeyIndividual(parseInt(this.kiId, 10), document.documentType.id, imageData)
      .subscribe({
        next: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Document successfully uploaded');
          this.listDocuments();
        },
        error: (err: HttpErrorResponse) => {
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });
  }

  upload(image: any, document: DocumentDetails): void {
    // this.staticService.loading.next(true);
    const imageData = new FormData();
    imageData.set('image', image);
    console.log(image);
    console.log(imageData.get("image"));
    // this.merchantService
    //   .postMerchantDocument(imageData, this.merchantId)
    //   .subscribe((res: MerchantDocument[]) => {
    //     this.merchantDocuments = res;
    //     console.log('merchant documents res from post', this.merchantDocuments);
    //     this.snackBarService.showSnackBar('Successfully uploaded document');
    //   }, (err: HttpErrorResponse) => {
    //     this.snackBarService.showSnackBar('Failed to upload document');
    //   });

    this.subscription3 = this.documentService
      .saveByKeyIndividual(parseInt(this.kiId, 10), document.documentType.id, imageData)
      .subscribe({
        next: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Document successfully uploaded');
          this.listDocuments();
        },
        error: (err: HttpErrorResponse) => {
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });

  }

  onFileSelected(event: any, documentDetail: DocumentDetails): void {
    const file: File = event.target.files[0];
    if (file) {
      this.fileName = file.name;
      this.upload(file, documentDetail);
    }
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({behavior: 'smooth'});
  }

  setInactive(): void {

  }

}
