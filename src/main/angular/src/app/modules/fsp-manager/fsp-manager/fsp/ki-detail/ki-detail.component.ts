import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-ki-detail',
  templateUrl: './ki-detail.component.html',
  styleUrls: ['./ki-detail.component.scss']
})
export class KiDetailComponent implements OnInit, AfterViewInit, OnDestroy {

  fspId!: string;
  private subscription!: Subscription | undefined;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
      this.subscription = this.route
      .parent
      ?.parent
      ?.parent
      ?.params
      .subscribe((params) => {
        console.log(`fsp id | ${params['id']}`);
        this.fspId = params['id'];
      });
  }

  ngAfterViewInit() {
    console.log('ROUTE TO KI LIST!');
    if (parseInt(this.fspId)) {
      this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/key-individual/ki-list`]);
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

}
