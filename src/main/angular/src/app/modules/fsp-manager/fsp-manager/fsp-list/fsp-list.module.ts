import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FspListRoutingModule } from './fsp-list-routing.module';
import { FspListComponent } from './fsp-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import {FspService} from "../../../../services/fsp/fsp.service";
import {FspApi} from "../../../../api/fsp.api.class";
import {ConfigService} from "../../../../services/config/config.service";


@NgModule({
  declarations: [
    FspListComponent
  ],
  imports: [
    CommonModule,
    FspListRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule
  ],
  providers: [
    ConfigService
  ]
})
export class FspListModule { }
