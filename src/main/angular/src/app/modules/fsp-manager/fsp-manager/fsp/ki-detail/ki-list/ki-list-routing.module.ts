import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KiListComponent } from './ki-list.component';

const routes: Routes = [{ path: '', component: KiListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KiListRoutingModule { }
