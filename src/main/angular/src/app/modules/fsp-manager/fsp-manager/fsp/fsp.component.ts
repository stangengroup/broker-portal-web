import {AfterViewInit, Component, OnDestroy, OnInit, Type, ViewChild} from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {ActivatedRoute, NavigationStart, Router} from '@angular/router';
import { Subscription } from 'rxjs';
import {FspService} from "../../../../services/fsp/fsp.service";
import {PolicyDescriptor} from "../../../../interfaces/policy/PolicyDescriptor";
import {MatTableDataSource} from "@angular/material/table";
import {HttpErrorResponse} from "@angular/common/http";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {FspDescriptor} from "../../../../interfaces/fsp/FspDescriptor";
import {SnackbarService} from "../../../../services/snackbar/snackbar.service";
import {DataService} from "../../../../services/shared/data/data.service";
import {FspDetails} from "../../../../interfaces/fsp/FspDetails";

export interface TabDetail {
  name: string;
  code: string;
  route: string;
  disabled: boolean;
  component?: Type<any>,
}

@Component({
  selector: 'app-fsp',
  templateUrl: './fsp.component.html',
  styleUrls: ['./fsp.component.scss']
})
export class FspComponent implements OnInit, AfterViewInit, OnDestroy {

  tabDetail: TabDetail[] = [
    {
      name: 'FSP Details',
      code: 'FSP_DETAILS',
      route: '/fsp-detail',
      disabled: false
    },
    {
      name: 'Key Individuals',
      code: 'KEY_INDIVIDUALS',
      route: '/key-individual',
      disabled: true
    },
    {
      name: 'Intermediaries',
      code: 'INTERMEDIARIES',
      route: '/intermediary',
      disabled: true
    }
  ];

  paramSubscription!: Subscription;
  fspName: string = 'ABC Brokers INC';
  fspRegNumber: string = 'KP0001000001';
  fspId!: string;
  public loading: boolean = true;
  private fspDetails!: FspDetails;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService

  ) {
    this.paramSubscription = this.route
      .params
      .subscribe((params) => {
        console.log(`fsp id | ${params['id']}`);
        this.fspId = params['id'];
        console.log(this.route, this.route.snapshot.paramMap.get('scrollToDocuments'));
        // if (!this.route.snapshot.paramMap.get('scrollToDocuments')) {
        // this.router.navigate([`/home/fsp-manager/fsp/${this.fspId}/fsp-detail`]);
        // }
        if (parseInt(this.fspId, 10)) {
          this.tabDetail.filter((val) => val.code === 'KEY_INDIVIDUALS')[0].disabled = false;
          this.tabDetail.filter((val) => val.code === 'INTERMEDIARIES')[0].disabled = false;
        }

      });

  }

  ngAfterViewInit() {
    console.log('ROUTE TO FSP DETAIL!');
    this.router.navigate([`/home/fsp-manager/fsp/${this.fspId}/fsp-detail`]);
  }

  ngOnInit(): void {
    let count = 1;
    const interval = setInterval(() => {
      if (count > 30) {
        clearInterval(interval);
      }

      if (this.dataService?.activeFsp?.getValue()) {
        this.fspDetails = this.dataService?.activeFsp?.getValue();
        this.fspName = this.fspDetails.domainName;
        this.fspRegNumber = this.fspDetails.fspRegNumber;
      }
    }, 1000);


  }

  ngOnDestroy(): void {
    this.paramSubscription?.unsubscribe();
  }

  back(): void {
    this.router.navigate([`/home/fsp-manager/list`])
  }

  // selectedTabChanged(index: number): void {
  //   console.log(this.router);
  //   console.log(this.route);
  //   console.log(`mat tab changed to | ${index}`);
  //   // Navigate to new tab route
  //   this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/${this.tabDetail[index].route}`]);
  // }

  fspRoute(): string {
    return `/home/fsp-manager/fsp/${this.fspId}`;
  }

  fspDetailActive(): boolean {
    return window.location.href.indexOf('fsp-detail') > -1;
  }

  kiDetailActive(): boolean {
    return window.location.href.indexOf('key-individual') > -1;

  }

  intermediaryDetailActive(): boolean {
    return window.location.href.indexOf('intermediary') > -1;

  }
}
