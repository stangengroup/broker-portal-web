import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntermediaryPolicyListComponent } from './intermediary-policy-list.component';

const routes: Routes = [{ path: '', component: IntermediaryPolicyListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntermediaryPolicyListRoutingModule { }
