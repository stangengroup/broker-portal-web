import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {firstValueFrom, Subscription} from "rxjs";
import { ConfigService } from 'src/app/services/config/config.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { StaticService } from 'src/app/services/static/static.service';

@Component({
  selector: 'app-fsp-manager',
  templateUrl: './fsp-manager.component.html',
  styleUrls: ['./fsp-manager.component.scss']
})
export class FspManagerComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  public loading: boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService,
    private staticService: StaticService,
    private configService: ConfigService,
  ) {
    this.subscription = this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          console.log(event);
          if (event.urlAfterRedirects === '/home/fsp-manager') {
            this.router.navigate(['/home/fsp-manager/list']);
          }
        }
      })
  }

  async ngOnInit() {
    console.log('init fsp manager');
    this.router.navigate(['/home/fsp-manager/list']);
    this.loading = true;
    const data = await firstValueFrom(this.configService.getAllConfigData());
    this.loading = false;
    this.snackbarService.showSnackBar('Config data retrieved');
    this.staticService.setBehaviourSubject(data);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

}
