import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import {FspDescriptor} from "../../../../interfaces/fsp/FspDescriptor";
import {HttpErrorResponse} from "@angular/common/http";
import {FspService} from "../../../../services/fsp/fsp.service";
import {SnackbarService} from "../../../../services/snackbar/snackbar.service";
import {Subscription} from "rxjs";
import {ListCount} from "../../../../interfaces/generic/ListCount";
import {StaticService} from "../../../../services/static/static.service";
import {ConfigService} from "../../../../services/config/config.service";

@Component({
  selector: 'app-fsp-list',
  templateUrl: './fsp-list.component.html',
  styleUrls: ['./fsp-list.component.scss']
})
export class FspListComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['name', 'id', 'fspRegNumber', 'created', 'fspStatus', 'actions'];
  dataSource!: MatTableDataSource<FspDescriptor>;
  fspList!: FspDescriptor[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public limit: number = 5;
  public page: number = 0;
  public loading: boolean = true;
  public totalElements!: number;

  private subscription!: Subscription;
  private subscription2!: Subscription;
  private subscription3!: Subscription | undefined;
  private subscription4!: Subscription | undefined;

  constructor(
    private router: Router,
    private fspService: FspService,
    private snackBarService: SnackbarService,
    private staticService: StaticService,
    private configService: ConfigService
  ) {
    this.getSavedLimit();
  }

  ngOnInit(): void {
    this.updateTable(false);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
  }

  getSavedLimit(): void {
    const limit = this.configService.getUserPreferenceFromStorage('pagination-limit');
    if (limit == null) {
      return;
    }
    if (parseInt(limit, 10) > 0) {
      this.limit = parseInt(limit, 10);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navigateToFsp(fspData: FspDescriptor): void {
    console.log('Navigate to fsp', fspData);
    this.router.navigate([`/home/fsp-manager/fsp/${fspData.id}`]);
  }

  addNewFsp(): void {
    console.log('Add new fsp');
    this.router.navigate([`/home/fsp-manager/fsp`]);
  }

  public async asyncUpdateTable(updatePage?: boolean): Promise<any> {
    // this.staticService.loading.next(true);
    this.subscription = this.fspService
      .listFsp(this.limit, this.page)
      .subscribe({
        next: (fspList: FspDescriptor[]) => {
          this.fspList = fspList;
          this.dataSource = new MatTableDataSource(this.fspList);

          // -- Count --
          this.subscription3 = this.fspService
            .listCount()
            .subscribe({
              next: (res: ListCount) => {
                // this.staticService.loading.next(false);
                this.totalElements = res.count;
                // Paging
                // this.dataSource.sort = this.sort;
                if (!updatePage) {
                  this.paginator.pageIndex = 0;
                }
                // this.dataSource.paginator = this.paginator;
                // this.dataSource.sort = this.sort;
                return;
              },
              error: (err: HttpErrorResponse) => {
                // this.staticService.loading.next(false);
                return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
              }
            });

          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });
  }

  private updateTable(updatePage: boolean): void {
    // this.staticService.loading.next(true);
    this.subscription2 = this.fspService
      .listFsp(this.limit, this.page)
      .subscribe({
        next: (fspList: FspDescriptor[]) => {
          this.fspList = fspList;
          this.dataSource = new MatTableDataSource(this.fspList);
          // Paging
          this.dataSource.sort = this.sort;
          if (!updatePage) {
            this.paginator.pageIndex = 0;
          }
          // -- Count --
          this.subscription4 = this.fspService
            .listCount()
            .subscribe({
              next: (res: ListCount) => {
                // this.staticService.loading.next(false);
                return this.totalElements = res.count;
              },
              error: (err: HttpErrorResponse) => {
                // this.staticService.loading.next(false);
                return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
              }
            });

          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar('Something went wrong', 'Please try again');
        }
      });
  }

  async setPageSizeOptions(setPageSizeOptionsInput: PageEvent): Promise<PageEvent> {
    this.page = setPageSizeOptionsInput.pageIndex;
    this.limit = setPageSizeOptionsInput.pageSize;
    this.paginator.pageIndex = this.page;
    this.paginator.pageSize = this.limit;
    this.configService.setUserPreferenceFromStorage('pagination-limit', this.limit);
    await this.asyncUpdateTable(true);
    return setPageSizeOptionsInput;
  }

}
