import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FspDocumentsComponent } from './fsp-documents.component';

describe('FspDocumentsComponent', () => {
  let component: FspDocumentsComponent;
  let fixture: ComponentFixture<FspDocumentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FspDocumentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FspDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
