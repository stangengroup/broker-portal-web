import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FspManagerRoutingModule } from './fsp-manager-routing.module';
import { FspManagerComponent } from './fsp-manager.component';
import {FspService} from "../../../services/fsp/fsp.service";
import {FspApi} from "../../../api/fsp.api.class";

@NgModule({
  declarations: [
    FspManagerComponent
  ],
  imports: [
    CommonModule,
    FspManagerRoutingModule
  ],
  providers: [
    FspService,
    FspApi
  ]
})
export class FspManagerModule { }
