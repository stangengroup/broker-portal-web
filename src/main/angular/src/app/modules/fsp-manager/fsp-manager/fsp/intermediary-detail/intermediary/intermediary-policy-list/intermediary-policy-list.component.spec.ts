import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediaryPolicyListComponent } from './intermediary-policy-list.component';

describe('IntermediaryPolicyListComponent', () => {
  let component: IntermediaryPolicyListComponent;
  let fixture: ComponentFixture<IntermediaryPolicyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntermediaryPolicyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediaryPolicyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
