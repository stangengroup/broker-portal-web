import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FspComponent } from './fsp.component';

const routes: Routes = [
  {
    path: '',
    component: FspComponent,
    children: [
      {
        path: 'fsp-detail',
        loadChildren: () => import('./fsp-detail/fsp-detail.module').then(m => m.FspDetailModule)
      },
      {
        path: 'key-individual',
        loadChildren: () => import('./ki-detail/ki-detail.module').then(m => m.KiDetailModule)
      },
      {
        path: 'intermediary',
        loadChildren: () => import('./intermediary-detail/intermediary-detail.module').then(m => m.IntermediaryDetailModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FspRoutingModule { }
