import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FspManagerComponent } from './fsp-manager.component';

const routes: Routes = [
  {
    path: '',
    component: FspManagerComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./fsp-list/fsp-list.module').then(m => m.FspListModule)
      },
      {
        path: 'fsp',
        loadChildren: () => import('./fsp/fsp.module').then(m => m.FspModule)
      },
      {
        path: 'fsp/:id',
        loadChildren: () => import('./fsp/fsp.module').then(m => m.FspModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FspManagerRoutingModule { }
