import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FspDetailComponent } from './fsp-detail.component';

describe('FspDetailComponent', () => {
  let component: FspDetailComponent;
  let fixture: ComponentFixture<FspDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FspDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FspDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
