import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-intermediary-detail',
  templateUrl: './intermediary-detail.component.html',
  styleUrls: ['./intermediary-detail.component.scss']
})
export class IntermediaryDetailComponent implements OnInit, AfterViewInit, OnDestroy {

  fspId!: string;
  private subscription!: Subscription | undefined;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
      this.subscription = this.route
      .parent
      ?.parent
      ?.parent
      ?.params
      .subscribe((params) => {
        console.log(`fsp id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.fspId = params['id'];
        }
        console.log(!this.route.snapshot.paramMap.get('copy'), this.route.snapshot.firstChild?.firstChild?.paramMap.get('copy'));
        if (!this.route.snapshot.paramMap.get('copy')) {
          // this.router.navigate([`home/fsp-manager/fsp/${params['id']}/intermediary/intermediary-list`]);
        }


      });
  }

  ngAfterViewInit() {
    console.log(this.route.snapshot.firstChild?.firstChild?.paramMap.get('copy'));
    if (this.fspId && !this.route.snapshot.firstChild?.firstChild?.paramMap.get('copy')) {
      this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-list`]);
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {

  }

}
