import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FspListComponent } from './fsp-list.component';

const routes: Routes = [
  {
    path: '', component: FspListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FspListRoutingModule { }
