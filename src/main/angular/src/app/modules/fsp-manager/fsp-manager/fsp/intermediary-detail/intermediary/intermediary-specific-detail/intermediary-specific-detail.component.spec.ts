import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediarySpecificDetailComponent } from './intermediary-specific-detail.component';

describe('IntermediarySpecificDetailComponent', () => {
  let component: IntermediarySpecificDetailComponent;
  let fixture: ComponentFixture<IntermediarySpecificDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntermediarySpecificDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediarySpecificDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
