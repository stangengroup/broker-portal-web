import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FspDetailRoutingModule } from './fsp-detail-routing.module';
import { FspDetailComponent } from './fsp-detail.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { FspDocumentsComponent } from './fsp-documents/fsp-documents.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import {DocumentService} from "../../../../../services/document/document.service";
import {DocumentApi} from "../../../../../api/document.api.class";
import {DataService} from "../../../../../services/shared/data/data.service";
import {IMaskModule} from "angular-imask";

@NgModule({
  declarations: [
    FspDetailComponent,
    FspDocumentsComponent
  ],
  imports: [
    CommonModule,
    FspDetailRoutingModule,
    SharedModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatIconModule,
    IMaskModule
  ],
  exports: [
    FspDetailComponent
  ],
  providers: [
    DocumentService,
    DocumentApi,
    DataService
  ]
})
export class FspDetailModule { }
