import {Component, OnDestroy, OnInit} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { firstValueFrom, Subscription } from 'rxjs';
import { Address } from 'src/app/interfaces/generic/Address';
import { BankingDetails } from 'src/app/interfaces/generic/BankingDetails';
import { Commission } from 'src/app/interfaces/generic/Commission';
import { Fees } from 'src/app/interfaces/generic/Fees';
import { KeyIndividualDetail } from 'src/app/interfaces/key-individual/KeyIndividualDetail';
import {StaticService} from "../../../../../../services/static/static.service";
import {ConfigDetail} from "../../../../../../interfaces/config/ConfigDetail";
import {UtilityService} from "../../../../../../services/utility/utility.service";
import {HttpErrorResponse} from "@angular/common/http";
import {KiService} from "../../../../../../services/key-individual/ki.service";
import {SnackbarService} from "../../../../../../services/snackbar/snackbar.service";
import {PostResponse} from "../../../../../../interfaces/generic/PostResponse";
import {environment} from "../../../../../../../environments/environment";
import {DialogData} from "../../../../../../interfaces/generic/DialogData";
import {DialogService} from "../../../../../../services/shared/dialog/dialog.service";
import {DataService} from "../../../../../../services/shared/data/data.service";

@Component({
  selector: 'app-ki',
  templateUrl: './ki.component.html',
  styleUrls: ['./ki.component.scss']
})
export class KiComponent implements OnInit, OnDestroy {

  kiStatuses: string[] = ['Active', 'Inactive'];
  paramSubscription: Subscription | undefined;
  titles: string[] = ['MR', 'MRS']

  kiForm!: FormGroup;
  loading: boolean = false;
  kiId!: string;
  fspId!: string;
  kiActive!: boolean;
  kiInactive!: boolean;

  keyIndividualDetail!: KeyIndividualDetail;
  address!: Address;
  public configData!: ConfigDetail;
  public subscription!: Subscription;
  private subscription2!: Subscription;
  private subscription3!: Subscription;
  private subscription4!: Subscription;
  private subscription5!: Subscription;

  public scrollToDocuments!: boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private staticService: StaticService,
    public utilityService: UtilityService,
    private kiService: KiService,
    private snackBarService: SnackbarService,
    private dialogService: DialogService,
    private dataService: DataService
  ) {
    // this.staticService.loading.next(true);
    this.createForm();

    this.paramSubscription = this.route
    .parent
    ?.parent
    ?.parent
    ?.parent
    ?.params
    .subscribe((params) => {
      console.log(`fsp id | ${params['id']}`);
      if (parseInt(params['id'])) {
        this.fspId = params['id'];
      }
    });

    this.subscription = this.route
    .params
    .subscribe((params) => {
      console.log(`ki id | ${params['id']}`);
      if (parseInt(params['id'])) {
        this.kiId = params['id'];
        this.setExistingKiData(null);
      } else {
        // this.staticService.loading.next(false);
      }
    });

  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
    this.subscription5?.unsubscribe();
    this.paramSubscription?.unsubscribe();
  }

  private setExistingKiData(keyIndividualDetail1: KeyIndividualDetail | null) {
    // this.staticService.loading.next(true);

    if (keyIndividualDetail1) {
      this.kiStatus.setValue(keyIndividualDetail1.active ? this.kiStatuses[this.kiStatuses.findIndex(value => value.toUpperCase() === 'ACTIVE')] : this.kiStatuses[this.kiStatuses.findIndex(value => value.toUpperCase() === 'INACTIVE')]);
      this.kiActive = keyIndividualDetail1.active;
      this.title.setValue(keyIndividualDetail1.title);
      this.fullName.setValue(keyIndividualDetail1.fullName);
      this.surname.setValue(keyIndividualDetail1.surname);
      this.idNumber.setValue(keyIndividualDetail1.idNumber);
      this.emailAddress.setValue(keyIndividualDetail1.emailAddress);
      this.alternativeEmailAddress.setValue(keyIndividualDetail1.alternativeEmailAddress);
      this.workNumber.setValue(keyIndividualDetail1.workNumber);
      this.mobileNumber.setValue(keyIndividualDetail1.mobileNumber);
      this.alternativeContactNumber.setValue(keyIndividualDetail1.alternativeContactNumber);
      this.streetNumber.setValue(keyIndividualDetail1.address.streetNumber);
      this.street1.setValue(keyIndividualDetail1.address.streetName);
      this.street2.setValue(keyIndividualDetail1.address.streetName2);
      this.suburb.setValue(keyIndividualDetail1.address.suburb);
      this.town.setValue(keyIndividualDetail1.address.town);
      this.province.setValue(keyIndividualDetail1.address.province);
      this.postalCode.setValue(keyIndividualDetail1.address.postalCode);

      // Check inactive
      if (!this.kiActive) {
        this.setFormInactive();
      } else {
        this.setFormActive();
      }

      this.loading = false;
      // this.staticService.loading.next(false);
    } else {
      this.subscription2 = this.kiService
        .getKeyIndividual(parseInt(this.kiId, 10))
        .subscribe({
          next: (keyIndividualDetail1: KeyIndividualDetail) => {
            this.kiStatus.setValue(keyIndividualDetail1.active ? this.kiStatuses[this.kiStatuses.findIndex(value => value.toUpperCase() === 'ACTIVE')] : this.kiStatuses[this.kiStatuses.findIndex(value => value.toUpperCase() === 'INACTIVE')]);
            this.kiActive = keyIndividualDetail1.active;
            this.title.setValue(keyIndividualDetail1.title);
            this.fullName.setValue(keyIndividualDetail1.fullName);
            this.surname.setValue(keyIndividualDetail1.surname);
            this.idNumber.setValue(keyIndividualDetail1.idNumber);
            this.emailAddress.setValue(keyIndividualDetail1.emailAddress);
            this.alternativeEmailAddress.setValue(keyIndividualDetail1.alternativeEmailAddress);
            this.workNumber.setValue(keyIndividualDetail1.workNumber);
            this.mobileNumber.setValue(keyIndividualDetail1.mobileNumber);
            this.alternativeContactNumber.setValue(keyIndividualDetail1.alternativeContactNumber);
            this.streetNumber.setValue(keyIndividualDetail1.address.streetNumber);
            this.street1.setValue(keyIndividualDetail1.address.streetName);
            this.street2.setValue(keyIndividualDetail1.address.streetName2);
            this.suburb.setValue(keyIndividualDetail1.address.suburb);
            this.town.setValue(keyIndividualDetail1.address.town);
            this.province.setValue(keyIndividualDetail1.address.province);
            this.postalCode.setValue(keyIndividualDetail1.address.postalCode);

            // Check inactive
            if (!this.kiActive) {
              this.setFormInactive();
            } else {
              this.setFormActive();
            }

            this.loading = false;
            // this.staticService.loading.next(false);
          },
          error: (err: HttpErrorResponse) => {
        return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        this.loading = false;
        // this.staticService.loading.next(false);
      }
    });
    }
  }

  get kiStatus(): AbstractControl {
    return this.kiForm.controls['kiStatus'];
  }

  get title(): AbstractControl {
    return this.kiForm.controls['title'];
  }

  get fullName(): AbstractControl {
    return this.kiForm.controls['fullName'];
  }

  get surname(): AbstractControl {
    return this.kiForm.controls['surname'];
  }

  get idNumber(): AbstractControl {
    return this.kiForm.controls['idNumber'];
  }

  get emailAddress(): AbstractControl {
    return this.kiForm.controls['emailAddress'];
  }

  get alternativeEmailAddress(): AbstractControl {
    return this.kiForm.controls['alternativeEmailAddress'];
  }

  get workNumber(): AbstractControl {
    return this.kiForm.controls['workNumber'];
  }

  get mobileNumber(): AbstractControl {
    return this.kiForm.controls['mobileNumber'];
  }

  get alternativeContactNumber(): AbstractControl {
    return this.kiForm.controls['alternativeContactNumber'];
  }

  get streetNumber(): AbstractControl {
    return this.kiForm.controls['streetNumber'];
  }

  get street1(): AbstractControl {
    return this.kiForm.controls['street1'];
  }

  get street2(): AbstractControl {
    return this.kiForm.controls['street2'];
  }

  get suburb(): AbstractControl {
    return this.kiForm.controls['suburb'];
  }

  get town(): AbstractControl {
    return this.kiForm.controls['town'];
  }

  get province(): AbstractControl {
    return this.kiForm.controls['province'];
  }

  get postalCode(): AbstractControl {
    return this.kiForm.controls['postalCode'];
  }

  createForm(): void {
    this.kiForm = this.fb.group({
      kiStatus: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]],
      title: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      fullName: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      surname: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      idNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(13), Validators.maxLength(13), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      emailAddress: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.email, Validators.maxLength(240)]],
      alternativeEmailAddress: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.email, Validators.maxLength(240)]],
      workNumber: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(10), Validators.maxLength(12), Validators.min(100000000), Validators.max(999999999999)]],
      mobileNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(10), Validators.maxLength(12), Validators.min(100000000), Validators.max(999999999999)]],
      alternativeContactNumber: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(10), Validators.maxLength(12), Validators.min(100000000), Validators.max(999999999999)]],
      streetNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.min(0), Validators.max(999999), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(6)]],
      street1: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      street2: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      suburb: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      town: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      province: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(100)]],
      postalCode: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.max(9999999999), Validators.minLength(4), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
    });

    this.kiForm.valueChanges
      .subscribe((val) => {
        console.log(val);
      });

  }

  saveKeyIndividual(): void {
    // this.staticService.loading.next(true);
    if (!this.kiId) {
      this.kiStatus.setValue('Active');
    }

    this.address = {
      postalCode: this.postalCode?.value?.toString(),
      province: this.province?.value,
      streetName: this.street1?.value,
      streetName2: this.street2?.value,
      streetNumber: this.streetNumber?.value,
      suburb: this.suburb?.value,
      town: this.town?.value
    };

    this.keyIndividualDetail = {
      address: this.address,
      alternativeContactNumber: this.alternativeContactNumber?.value,
      emailAddress: this.emailAddress?.value,
      id: parseInt(this.fspId, 10),
      workNumber: this.workNumber?.value,
      alternativeEmailAddress: this.alternativeEmailAddress?.value,
      fspId: parseInt(this.fspId, 10),
      fullName: this.fullName?.value,
      idNumber: this.idNumber?.value,
      mobileNumber: this.mobileNumber?.value,
      surname: this.surname?.value,
      title: this.title?.value,
      active: this.kiStatus?.value.toUpperCase() === 'ACTIVE'
    };

    console.log(this.keyIndividualDetail);

    if (!this.kiId) {
      this.subscription3 = this.kiService
        .saveKeyIndividual(this.keyIndividualDetail)
        .subscribe({
          next: (resp: KeyIndividualDetail) => {
            console.log(resp);
            this.snackBarService.showSnackBar('Key Individual Successfully saved', 'Success');
            this.dataService.setScrollToDocuments(true);
            this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/key-individual/ki/${resp.keyIndividualId}`]);

            // this.staticService.loading.next(false);
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err) => {
            console.error(err);
            // this.staticService.loading.next(false);
            if (err.status === 409) {
              this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            this.subscription?.unsubscribe();
            // this.staticService.loading.next(false);
          }
        });
    } else {
      this.subscription4 = this.kiService
        .updateKeyIndividual(this.keyIndividualDetail, parseInt(this.kiId, 10))
        .subscribe({
          next: (resp: KeyIndividualDetail) => {
            console.log(resp);
            this.snackBarService.showSnackBar('Key Individual Successfully edited', 'Success');
            // Check inactive
            if (this.kiStatus.value?.toUpperCase() === 'INACTIVE') {
              this.setFormInactive();
            } else {
              this.setFormActive();
            }
            this.setExistingKiData(resp);
            // this.staticService.loading.next(false);
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err) => {
            console.error(err);
            // this.staticService.loading.next(false);
            if (err.status === 409) {
              this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            this.subscription?.unsubscribe();
            // this.staticService.loading.next(false);
          }
        });
    }

  }

  copyToIntermediary(): void {

    if (!this.kiId) {
      this.kiStatus.setValue('Active');
    }

    this.address = {
      postalCode: this.postalCode?.value,
      province: this.province?.value,
      streetName: this.street1?.value,
      streetName2: this.street2?.value,
      streetNumber: this.streetNumber?.value,
      suburb: this.suburb?.value,
      town: this.town?.value
    };

    this.keyIndividualDetail = {
      address: this.address,
      alternativeContactNumber: this.alternativeContactNumber?.value,
      emailAddress: this.emailAddress?.value,
      id: parseInt(this.fspId, 10),
      workNumber: this.workNumber?.value,
      alternativeEmailAddress: this.alternativeEmailAddress?.value,
      fspId: parseInt(this.fspId, 10),
      fullName: this.fullName?.value,
      idNumber: this.idNumber?.value,
      mobileNumber: this.mobileNumber?.value,
      surname: this.surname?.value,
      title: this.title?.value,
      active: this.kiStatus?.value.toUpperCase() === 'ACTIVE'
    };

    console.log('Copying to broker...', this.keyIndividualDetail);
    this.dataService.setCopyIntermediaryData(this.keyIndividualDetail);
    console.log('Copy this KI', this.kiId, 'to fsp', this.fspId, 'to create a new intermediary');
    this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view/undefined/detail`, { copy: true }]);
  }

  confirmKeyIndividual() {

    // this.staticService.loading.next(true);

    const dialogData: DialogData = {
      buttonText: {
        confirm: 'Save',
        cancel: 'Cancel'
      },
      description: 'Saving this KI will add them to the system.',
      failureMesssage: '',
      successMessage: '',
      title: `Are you sure you want to save?`
    };

    this.subscription5 = this.dialogService
      .openDialog(dialogData)
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (result) {
            this.saveKeyIndividual();
          } else {
            // this.staticService.loading.next(false);
          }
        },
        error: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Error occurred during confirmation dialog');
        }
      })
  }

  async ngOnInit() {
    this.configData = await firstValueFrom(this.staticService
      .configDetail);

    // In dev environment, fill with mock data
    if (!environment.production) {
      this.kiStatus.setValue(this.kiStatuses[this.kiStatuses.findIndex(value => value.toUpperCase() === 'ACTIVE')]);
      this.title.setValue("MR");
      this.fullName.setValue("Name");
      this.surname.setValue("Surname")
      this.idNumber.setValue("0000000000000");
      this.emailAddress.setValue("test@email.com");
      this.alternativeEmailAddress.setValue("test@email.com");
      this.workNumber.setValue("0000000000");
      this.mobileNumber.setValue("0000000000");
      this.alternativeContactNumber.setValue("0000000000");
      this.streetNumber.setValue("1");
      this.street1.setValue("Main St");
      this.street2.setValue("0039");
      this.suburb.setValue("Suburb");
      this.town.setValue("Town");
      this.province.setValue("Province");
      this.postalCode.setValue("0039");
    }

  }

  setFormInactive(): void {
    this.kiForm.disable();
    this.kiStatus.enable();
    this.kiStatus.updateValueAndValidity();
    this.kiInactive = true;
  }

  setFormActive(): void {
    this.kiForm.enable();
    this.kiStatus.enable();
    this.kiStatus.updateValueAndValidity();
    this.kiInactive = false;
  }

  checkNumbers($event: KeyboardEvent) {
    const numbersArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    console.log($event);
    if (numbersArr.indexOf($event.key) === -1) {
      return $event.preventDefault();
    }
  }

  checkNumbersPaste($event: ClipboardEvent) {
    const regexNumbers = new RegExp(/^[0-9]{1,45}$/);
    console.log($event);
    if (!regexNumbers.test(($event.clipboardData?.getData('text') as string))) {
      this.snackBarService.showSnackBar('You are attempting to paste a non digit-only value into a digit-only field');
      return $event.preventDefault();
    }
  }

}
