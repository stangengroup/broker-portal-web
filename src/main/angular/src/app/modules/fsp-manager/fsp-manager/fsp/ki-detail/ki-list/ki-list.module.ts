import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KiListRoutingModule } from './ki-list-routing.module';
import { KiListComponent } from './ki-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import {KiService} from "../../../../../../services/key-individual/ki.service";
import {KiApi} from "../../../../../../api/ki.api.class";
import {ConfigService} from "../../../../../../services/config/config.service";
import {DataService} from "../../../../../../services/shared/data/data.service";


@NgModule({
  declarations: [
    KiListComponent
  ],
  imports: [
    CommonModule,
    KiListRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule
  ],
  providers: [
    KiService,
    KiApi,
    ConfigService
  ]
})
export class KiListModule { }
