import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KiDetailRoutingModule } from './ki-detail-routing.module';
import { KiDetailComponent } from './ki-detail.component';


@NgModule({
  declarations: [
    KiDetailComponent
  ],
  exports: [
    KiDetailComponent
  ],
  imports: [
    CommonModule,
    KiDetailRoutingModule
  ]
})
export class KiDetailModule { }
