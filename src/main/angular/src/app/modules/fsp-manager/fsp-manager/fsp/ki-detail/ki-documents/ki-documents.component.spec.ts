import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KiDocumentsComponent } from './ki-documents.component';

describe('KiDocumentsComponent', () => {
  let component: KiDocumentsComponent;
  let fixture: ComponentFixture<KiDocumentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KiDocumentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KiDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
