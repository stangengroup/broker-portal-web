import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KiRoutingModule } from './ki-routing.module';
import { KiComponent } from './ki.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDividerModule } from '@angular/material/divider';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { KiDocumentsComponent } from '../ki-documents/ki-documents.component';
import {KiService} from "../../../../../../services/key-individual/ki.service";
import {KiApi} from "../../../../../../api/ki.api.class";
import {DocumentService} from "../../../../../../services/document/document.service";
import {DocumentApi} from "../../../../../../api/document.api.class";


@NgModule({
  declarations: [
    KiComponent,
    KiDocumentsComponent
  ],
  imports: [
    CommonModule,
    KiRoutingModule,
    SharedModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatIconModule
  ],
  providers: [
    KiService,
    KiApi,
    DocumentService,
    DocumentApi
  ]
})
export class KiModule { }
