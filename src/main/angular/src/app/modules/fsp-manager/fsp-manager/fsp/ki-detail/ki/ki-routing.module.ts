import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KiComponent } from './ki.component';

const routes: Routes = [{ path: '', component: KiComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KiRoutingModule { }
