import {Component, OnDestroy, OnInit} from '@angular/core';
import {firstValueFrom, Subscription} from "rxjs";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IntermediaryDetail} from "../../../../../../../interfaces/intermediary/IntermediaryDetail";
import {Address} from "../../../../../../../interfaces/generic/Address";
import {Fees} from "../../../../../../../interfaces/generic/Fees";
import {Commission} from "../../../../../../../interfaces/generic/Commission";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfigDetail} from "../../../../../../../interfaces/config/ConfigDetail";
import {StaticService} from "../../../../../../../services/static/static.service";
import {UtilityService} from "../../../../../../../services/utility/utility.service";
import {KeyIndividualDetail} from "../../../../../../../interfaces/key-individual/KeyIndividualDetail";
import {HttpErrorResponse} from "@angular/common/http";
import {IntermediaryService} from "../../../../../../../services/intermediary/intermediary.service";
import {SnackbarService} from "../../../../../../../services/snackbar/snackbar.service";
import {PostResponse} from "../../../../../../../interfaces/generic/PostResponse";
import {environment} from "../../../../../../../../environments/environment";
import {DialogData} from "../../../../../../../interfaces/generic/DialogData";
import {DialogService} from "../../../../../../../services/shared/dialog/dialog.service";
import {DataService} from "../../../../../../../services/shared/data/data.service";
import {MatOptionSelectionChange} from "@angular/material/core";
import {MatSelectChange} from "@angular/material/select";

@Component({
  selector: 'app-intermediary-specific-detail',
  templateUrl: './intermediary-specific-detail.component.html',
  styleUrls: ['./intermediary-specific-detail.component.scss']
})
export class IntermediarySpecificDetailComponent implements OnInit, OnDestroy {

  statuses: string[] = ['Active', 'Inactive'];
  paramSubscription?: Subscription;
  paramSubscription2?: Subscription;
  titles: string[] = ['MR', 'MRS']

  intermediaryForm!: FormGroup;
  loading: boolean = false;
  intermediaryId!: string;
  fspId!: string;
  intermediaryActive!: boolean;
  intermediaryInactive!: boolean;

  intermediaryDetails!: IntermediaryDetail;
  keyIndividualDetail!: KeyIndividualDetail;
  address!: Address;
  fees!: Fees;
  commission!: Commission;

  public configData!: ConfigDetail;
  commissionError!: string | null;
  public subscription!: Subscription;
  public subscription2!: Subscription;
  public subscription3!: Subscription;
  public subscription4!: Subscription;

  public upfrontCommissionHintMsg: string = 'Upfront + as and when commissions should equate to 100';
  public asAndWhenHintMsg: string = 'Upfront + as and when commissions should equate to 100';

  public scrollToDocuments!: boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private staticService: StaticService,
    public utilityService: UtilityService,
    private intermediaryService: IntermediaryService,
    private snackBarService: SnackbarService,
    private router: Router,
    private dialogService: DialogService,
    private dataService: DataService
  ) {

    // this.staticService.loading.next(true);

    this.createForm();

    this.paramSubscription = this.route
      .parent
      ?.parent
      ?.parent
      ?.parent
      ?.params
      .subscribe((params: any) => {
        console.log(`fsp id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.fspId = params['id'];
        }
      });

    this.paramSubscription2 = this.route
      .params
      .subscribe((params: any) => {
        console.log(`intermediary id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.intermediaryId = params['id'];
          this.setExistingIntermediaryData(null);
        } else {
          // this.staticService.loading.next(false);
        }
      });

    if (this.route.snapshot.paramMap.get('copy')) {
      // this.staticService.loading.next(true);

      this.keyIndividualDetail = this.dataService.keyIndividualToBroker.getValue();

      console.log(this.keyIndividualDetail);
      this.intermediaryStatus.setValue(this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'ACTIVE')]);
      this.title.setValue(this.keyIndividualDetail.title);
      this.fullName.setValue(this.keyIndividualDetail.fullName);
      this.surname.setValue(this.keyIndividualDetail.surname);
      this.idNumber.setValue(this.keyIndividualDetail.idNumber);
      this.emailAddress.setValue(this.keyIndividualDetail.emailAddress);
      this.alternativeEmailAddress.setValue(this.keyIndividualDetail.alternativeEmailAddress);
      this.password.setValue(this.keyIndividualDetail.password);
      this.workNumber.setValue(this.keyIndividualDetail.workNumber);
      this.mobileNumber.setValue(this.keyIndividualDetail.mobileNumber);
      this.alternativeContactNumber.setValue(this.keyIndividualDetail.alternativeContactNumber);
      this.streetNumber.setValue(this.keyIndividualDetail.address.streetNumber);
      this.street1.setValue(this.keyIndividualDetail.address.streetName);
      this.street2.setValue(this.keyIndividualDetail.address.streetName2);
      this.suburb.setValue(this.keyIndividualDetail.address.suburb);
      this.town.setValue(this.keyIndividualDetail.address.town);
      this.province.setValue(this.keyIndividualDetail.address.province);
      this.postalCode.setValue(this.keyIndividualDetail.address.postalCode);
      // this.feesPercentage.setValue(intermediaryDetail.perc);
      // this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.name === 'Basic')]);
      // this.upfrontCommission.setValue(intermediaryDetail.commission.upfront);
      // this.asAndWhenCommission.setValue(intermediaryDetail.commission.asAndWhen);
      // this.staticService.loading.next(false);

    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
    this.paramSubscription?.unsubscribe();
    this.paramSubscription2?.unsubscribe();
  }

  private setExistingIntermediaryData(intermediaryDetail: IntermediaryDetail | null) {
    // this.staticService.loading.next(true);

    if (intermediaryDetail) {
      this.intermediaryStatus.setValue(intermediaryDetail.active ? this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'ACTIVE')] : this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'INACTIVE')]);
      this.intermediaryActive = intermediaryDetail.active;
      this.title.setValue(intermediaryDetail.title);
      this.fullName.setValue(intermediaryDetail.fullName);
      this.surname.setValue(intermediaryDetail.surname);
      this.idNumber.setValue(intermediaryDetail.idNumber);
      this.emailAddress.setValue(intermediaryDetail.emailAddress);
      this.password.setValue(intermediaryDetail.password);
      this.alternativeEmailAddress.setValue(intermediaryDetail.alternativeEmailAddress);
      this.workNumber.setValue(intermediaryDetail.workNumber);
      this.mobileNumber.setValue(intermediaryDetail.mobileNumber);
      this.alternativeContactNumber.setValue(intermediaryDetail.alternativeContactNumber);
      this.streetNumber.setValue(intermediaryDetail.address.streetNumber);
      this.street1.setValue(intermediaryDetail.address.streetName);
      this.street2.setValue(intermediaryDetail.address.streetName2);
      this.suburb.setValue(intermediaryDetail.address.suburb);
      this.town.setValue(intermediaryDetail.address.town);
      this.province.setValue(intermediaryDetail.address.province);
      this.postalCode.setValue(intermediaryDetail.address.postalCode);
      this.brokerCode.setValue(intermediaryDetail?.brokerCode);
      // this.feesPercentage.setValue(intermediaryDetail.perc);
      this.intermediaryType.setValue(this.configData.intermediaryTypeList[this.configData.intermediaryTypeList.findIndex(value => value.name.toUpperCase() === intermediaryDetail.intermediaryType.name.toUpperCase())]);
      this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.name.toUpperCase() === 'HYBRID')]);
      this.upfrontCommission.setValue(intermediaryDetail.commission.upfront);
      this.asAndWhenCommission.setValue(intermediaryDetail.commission.asAndWhen);
      // this.staticService.loading.next(false);

      // Check inactive
      if (!this.intermediaryActive) {
        this.setFormInactive();
      } else {
        this.setFormActive();
      }

      this.loading = false;
    } else {
      this.subscription2 = this.intermediaryService
        .getIntermediary(parseInt(this.intermediaryId, 10))
        .subscribe({
          next: (intermediaryDetail: IntermediaryDetail) => {
            this.intermediaryStatus.setValue(intermediaryDetail.active ? this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'ACTIVE')] : this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'INACTIVE')]);
            this.intermediaryActive = intermediaryDetail.active;
            this.title.setValue(intermediaryDetail.title);
            this.fullName.setValue(intermediaryDetail.fullName);
            this.surname.setValue(intermediaryDetail.surname);
            this.idNumber.setValue(intermediaryDetail.idNumber);
            this.emailAddress.setValue(intermediaryDetail.emailAddress);
            this.password.setValue(intermediaryDetail.password);
            this.alternativeEmailAddress.setValue(intermediaryDetail.alternativeEmailAddress);
            this.workNumber.setValue(intermediaryDetail.workNumber);
            this.mobileNumber.setValue(intermediaryDetail.mobileNumber);
            this.alternativeContactNumber.setValue(intermediaryDetail.alternativeContactNumber);
            this.streetNumber.setValue(intermediaryDetail.address.streetNumber);
            this.street1.setValue(intermediaryDetail.address.streetName);
            this.street2.setValue(intermediaryDetail.address.streetName2);
            this.suburb.setValue(intermediaryDetail.address.suburb);
            this.town.setValue(intermediaryDetail.address.town);
            this.province.setValue(intermediaryDetail.address.province);
            this.postalCode.setValue(intermediaryDetail.address.postalCode);
            this.brokerCode.setValue(intermediaryDetail?.brokerCode);
            // this.feesPercentage.setValue(intermediaryDetail.perc); !!!!
            this.intermediaryType.setValue(this.configData.intermediaryTypeList[this.configData.intermediaryTypeList.findIndex(value => value.name.toUpperCase() === intermediaryDetail.intermediaryType.name.toUpperCase())]);
            this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.name.toUpperCase() === intermediaryDetail.commission.commissionStructureType.name.toUpperCase())]);
            this.upfrontCommission.setValue(intermediaryDetail.commission.upfront);
            this.asAndWhenCommission.setValue(intermediaryDetail.commission.asAndWhen);
            // this.staticService.loading.next(false);

            // Check inactive
            if (!this.intermediaryActive) {
              this.setFormInactive();
            } else {
              this.setFormActive();
            }

            this.loading = false;
          },
          error: (err: HttpErrorResponse) => {
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
            this.loading = false;
          }
        });
    }

  }

  get intermediaryStatus(): AbstractControl {
    return this.intermediaryForm.controls['intermediaryStatus'];
  }

  get title(): AbstractControl {
    return this.intermediaryForm.controls['title'];
  }

  public get fullName(): AbstractControl {
    return this.intermediaryForm.controls['fullName'];
  }

  get surname(): AbstractControl {
    return this.intermediaryForm.controls['surname'];
  }

  get idNumber(): AbstractControl {
    return this.intermediaryForm.controls['idNumber'];
  }

  get brokerCode(): AbstractControl {
    return this.intermediaryForm.controls['brokerCode'];
  }

  get intermediaryType(): AbstractControl {
    return this.intermediaryForm.controls['intermediaryType'];
  }

  get emailAddress(): AbstractControl {
    return this.intermediaryForm.controls['emailAddress'];
  }

  get alternativeEmailAddress(): AbstractControl {
    return this.intermediaryForm.controls['alternativeEmailAddress'];
  }

  get password(): AbstractControl {
    return this.intermediaryForm.controls['password'];
  }

  get workNumber(): AbstractControl {
    return this.intermediaryForm.controls['workNumber'];
  }

  get mobileNumber(): AbstractControl {
    return this.intermediaryForm.controls['mobileNumber'];
  }

  get alternativeContactNumber(): AbstractControl {
    return this.intermediaryForm.controls['alternativeContactNumber'];
  }

  get streetNumber(): AbstractControl {
    return this.intermediaryForm.controls['streetNumber'];
  }

  get street1(): AbstractControl {
    return this.intermediaryForm.controls['street1'];
  }

  get street2(): AbstractControl {
    return this.intermediaryForm.controls['street2'];
  }

  get suburb(): AbstractControl {
    return this.intermediaryForm.controls['suburb'];
  }

  get town(): AbstractControl {
    return this.intermediaryForm.controls['town'];
  }

  get province(): AbstractControl {
    return this.intermediaryForm.controls['province'];
  }

  get postalCode(): AbstractControl {
    return this.intermediaryForm.controls['postalCode'];
  }

  // get feesPercentage(): AbstractControl {
  //   return this.intermediaryForm.controls['feesPercentage'];
  // }

  get commissionType(): AbstractControl {
    return this.intermediaryForm.controls['commissionType'];
  }

  get upfrontCommission(): AbstractControl {
    return this.intermediaryForm.controls['upfrontCommission'];
  }

  get asAndWhenCommission(): AbstractControl {
    return this.intermediaryForm.controls['asAndWhenCommission'];
  }

  get intermediaryHome(): AbstractControl {
    return this.intermediaryForm.controls['intermediaryHome'];
  }

  createForm(): void {
    this.intermediaryForm = this.fb.group({
      intermediaryStatus: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]],
      title: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      fullName: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      surname: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      idNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.minLength(13), Validators.maxLength(13)]],
      brokerCode: [''],
      intermediaryHome: [''],
      intermediaryType: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(240)]],
      emailAddress: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.email, Validators.maxLength(240)]],
      password: [''],
      alternativeEmailAddress: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.email, Validators.maxLength(240)]],
      workNumber: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(10), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.min(100000000), Validators.max(999999999999)]],
      mobileNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(10), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.min(100000000), Validators.max(999999999999)]],
      alternativeContactNumber: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.minLength(10), Validators.maxLength(12), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.min(100000000), Validators.max(999999999999)]],
      streetNumber: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.min(0), Validators.max(999999), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(6)]],
      street1: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      street2: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      suburb: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      town: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      province: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(100)]],
      postalCode: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.max(9999999999), Validators.minLength(4), Validators.maxLength(10), Validators.pattern(/^[a-zA-Z0-9" "]*$/)]],
      // feesPercentage: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(3)]],
      commissionType: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.maxLength(240)]],
      upfrontCommission: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(3)]],
      asAndWhenCommission: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(3)]]
    });

    this.intermediaryForm.valueChanges
      .subscribe((val) => {

        // Commissions

        if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || (parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) > 100) {
          this.upfrontCommission.setErrors([Validators.max]);
          this.asAndWhenCommission.setErrors([Validators.max]);
          this.commissionError = 'Commissions are over 100%';
        } else {
          if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || !((parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) < 100)) {
            this.upfrontCommission.setErrors(null);
            this.asAndWhenCommission.setErrors(null);
            this.commissionError = null;
          }
        }

        if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || (parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) < 100) {
          this.upfrontCommission.setErrors([Validators.min]);
          this.asAndWhenCommission.setErrors([Validators.min]);
          this.commissionError = 'Commissions are less than 100%';
        } else {
          if ((((parseInt(this.upfrontCommission?.value, 10)) < 0 || (parseInt(this.asAndWhenCommission?.value, 10)) < 0)) || !((parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10)) > 100)) {
            this.upfrontCommission.setErrors(null);
            this.asAndWhenCommission.setErrors(null);
            this.commissionError = null;
          }
        }

        // if (((parseInt(this.upfrontCommission?.value, 10) + parseInt(this.asAndWhenCommission?.value, 10))) < 100) {
        //   this.upfrontCommission.setErrors([Validators.min]);
        //   this.asAndWhenCommission.setErrors([Validators.min]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.upfrontCommission.setErrors(null);
        //   this.asAndWhenCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.upfrontCommission?.value, 10) > 100) {
        //   this.upfrontCommission.setErrors([Validators.max]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.upfrontCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.asAndWhenCommission?.value, 10) > 100) {
        //   this.asAndWhenCommission.setErrors([Validators.max]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.asAndWhenCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.upfrontCommission?.value, 10) < 0) {
        //   this.upfrontCommission.setErrors([Validators.min]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.upfrontCommission.setErrors(null);
        // }
        //
        // if (parseInt(this.asAndWhenCommission?.value, 10) < 0) {
        //   this.asAndWhenCommission.setErrors([Validators.min]);
        //   console.log('invalid commissions', this.upfrontCommission, this.asAndWhenCommission);
        // } else {
        //   console.log('valid commissions', this.upfrontCommission, this.asAndWhenCommission);
        //   this.asAndWhenCommission.setErrors(null);
        // }

      });

  }

  saveIntermediary(): void {
    // this.staticService.loading.next(true);
    if (!this.intermediaryId) {
      this.intermediaryStatus.setValue(this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'ACTIVE')]);
    }

    this.address = {
      postalCode: this.postalCode?.value?.toString(),
      province: this.province?.value,
      streetName: this.street1?.value,
      streetName2: this.street2?.value,
      streetNumber: this.streetNumber?.value,
      suburb: this.suburb?.value,
      town: this.town?.value
    };

    this.commission = {
      asAndWhen: this.asAndWhenCommission?.value,
      commissionStructureType: this.commissionType?.value,
      upfront: this.upfrontCommission?.value
    };

    // this.fees = {
    //   feeStructureType: this.configData.feeStructureTypeList[0],
    //   percentage: this.feesPercentage?.value
    // };

    this.intermediaryDetails = {
      address: this.address,
      commission: this.commission,
      alternativeContactNumber: this.alternativeContactNumber?.value,
      emailAddress: this.emailAddress?.value,
      password: this.password?.value,
      id: parseInt(this.fspId, 10),
      workNumber: this.workNumber?.value,
      alternativeEmailAddress: this.alternativeEmailAddress?.value,
      brokerCode: this.brokerCode?.value,
      intermediaryHome: this.intermediaryHome?.value,
      commsPreference: this.configData.commsPreferenceList[this.configData.commsPreferenceList.findIndex(value => value.name.toUpperCase() === 'DEFAULT')],
      fspId: parseInt(this.fspId, 10),
      fullName: this.fullName?.value,
      idNumber: this.idNumber?.value,
      intermediaryAccreditationTestState: {
        id: 1,
        name: 'None'
      },
      intermediaryType: this.intermediaryType?.value,
      mobileNumber: this.mobileNumber?.value,
      surname: this.surname?.value,
      title: this.title?.value,
      active: this.intermediaryStatus?.value.toUpperCase() === 'ACTIVE'
    };

    if (!this.intermediaryId) {
      this.subscription = this.intermediaryService
        .saveIntermediary(this.intermediaryDetails)
        .subscribe({
          next: (resp: IntermediaryDetail) => {
            // this.staticService.loading.next(false);
            console.log(resp);
            this.snackBarService.showSnackBar('Intermediary Successfully saved', 'Success');
            this.dataService.setScrollToDocuments(true);
            this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view/${resp.intermediaryId}`]);
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err) => {
            console.error(err);
            // this.staticService.loading.next(false);
            if (err.status === 409) {
              this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            this.subscription?.unsubscribe();
            // this.staticService.loading.next(false);
          }
        });
    } else {
      this.subscription3 = this.intermediaryService
        .updateIntermediary(this.intermediaryDetails, parseInt(this.intermediaryId, 10))
        .subscribe({
          next: (resp: IntermediaryDetail) => {
            console.log(resp);
            this.snackBarService.showSnackBar('Intermediary Successfully edited', 'Success');
            // Check inactive
            if (this.intermediaryStatus.value?.toUpperCase() === 'INACTIVE') {
              this.setFormInactive();
            } else {
              this.setFormActive();
            }

            this.setExistingIntermediaryData(resp);
            // this.staticService.loading.next(true);
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err) => {
            console.error(err);
            // this.staticService.loading.next(false);
            if (err.status === 409) {
              this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            this.subscription3?.unsubscribe();
            // this.staticService.loading.next(false);
          }
        });
    }

  }

  confirmIntermediary() {

    // this.staticService.loading.next(true);

    const dialogData: DialogData = {
      buttonText: {
        confirm: 'Save',
        cancel: 'Cancel'
      },
      description: 'Saving this Intermediary will add them to the system.',
      failureMesssage: '',
      successMessage: '',
      title: `Are you sure you want to save?`
    };

    this.subscription4 = this.dialogService
      .openDialog(dialogData)
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (result) {
            this.saveIntermediary();
          } else {
            // this.staticService.loading.next(false);
          }
        },
        error: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Error occurred during confirmation dialog');
        }
      })
  }

  async ngOnInit() {
    this.configData = await firstValueFrom(this.staticService
      .configDetail);

    // In dev environment, fill with mock data
    if (!environment.production && !this.route.snapshot.paramMap.get('copy') && !this.intermediaryId) {
      this.intermediaryStatus.setValue(this.statuses[this.statuses.findIndex(value => value.toUpperCase() === 'ACTIVE')]);
      this.title.setValue("MR");
      this.fullName.setValue("Name");
      this.surname.setValue("Surname")
      this.idNumber.setValue("0000000000000");
      // this.brokerCode.setValue("ABCDEF90000");
      this.intermediaryHome.setValue(window.location.href);
      this.intermediaryType.setValue(this.configData.intermediaryTypeList[this.configData.intermediaryTypeList.findIndex(value => value.name.toUpperCase() === 'BROKER')]);
      this.emailAddress.setValue("test@email.com");
      this.alternativeEmailAddress.setValue("test@email.com");
      this.workNumber.setValue("0000000000");
      this.mobileNumber.setValue("0000000000");
      this.alternativeContactNumber.setValue("0000000000");
      this.streetNumber.setValue("1");
      this.street1.setValue("Main St");
      this.street2.setValue("0039");
      this.suburb.setValue("Suburb");
      this.town.setValue("Town");
      this.province.setValue("Province");
      this.postalCode.setValue("0039");
      // this.feesPercentage.setValue(25);
      this.commissionType.setValue(this.configData.commissionStructureTypeList[this.configData.commissionStructureTypeList.findIndex(value => value.name.toUpperCase() === 'BASIC')]);
      this.upfrontCommission.setValue(50);
      this.asAndWhenCommission.setValue(50);
    }


  }

  selectCommissionType($event: MatSelectChange) {
    // Disabling depending on commission structure type
    console.log($event?.value?.name);
    if ($event.value?.name?.toUpperCase() === 'AS AND WHEN') {
      this.asAndWhenCommission.disable();
      this.asAndWhenCommission.updateValueAndValidity();
      this.upfrontCommission.disable();
      this.upfrontCommission.updateValueAndValidity();
      this.upfrontCommission.setValue(0);
      this.asAndWhenCommission.setValue(100);
      this.upfrontCommissionHintMsg = 'This input is disabled as As and When commission is selected';
      this.asAndWhenHintMsg = 'This input is disabled as the value is defaulted to 100%';
    }

    if ($event.value?.name?.toUpperCase() === 'UPFRONT') {
      this.upfrontCommission.disable();
      this.upfrontCommission.updateValueAndValidity();
      this.asAndWhenCommission.disable();
      this.asAndWhenCommission.updateValueAndValidity();
      this.upfrontCommission.setValue(100);
      this.asAndWhenCommission.setValue(0);
      this.upfrontCommissionHintMsg = 'This input is disabled as the value is defaulted to 100%';
      this.asAndWhenHintMsg = 'This input is disabled as upfront commission is selected';
    }

    if ($event.value?.name?.toUpperCase() === 'HYBRID') {
      this.asAndWhenCommission.enable();
      this.upfrontCommission.enable();
      this.asAndWhenCommission.updateValueAndValidity();
      this.upfrontCommission.updateValueAndValidity();
      this.upfrontCommission.setValue(50);
      this.asAndWhenCommission.setValue(50);
      this.upfrontCommissionHintMsg = 'Upfront + as and when commissions should equate to 100';
      this.asAndWhenHintMsg = 'Upfront + as and when commissions should equate to 100';
    }
  }

  setFormInactive(): void {
    this.intermediaryForm.disable();
    this.intermediaryStatus.enable();
    this.intermediaryStatus.updateValueAndValidity();
    this.intermediaryInactive = true;
  }

  setFormActive(): void {
    this.intermediaryForm.enable();
    this.intermediaryStatus.enable();
    this.intermediaryStatus.updateValueAndValidity();
    this.intermediaryInactive = false;
  }

  checkNumbers($event: KeyboardEvent) {
    const numbersArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    console.log($event);
    if (numbersArr.indexOf($event.key) === -1) {
      return $event.preventDefault();
    }
  }

  checkNumbersPaste($event: ClipboardEvent) {
    const regexNumbers = new RegExp(/^[0-9]{1,45}$/);
    console.log($event);
    if (!regexNumbers.test(($event.clipboardData?.getData('text') as string))) {
      this.snackBarService.showSnackBar('You are attempting to paste a non digit-only value into a digit-only field');
      return $event.preventDefault();
    }
  }
}
