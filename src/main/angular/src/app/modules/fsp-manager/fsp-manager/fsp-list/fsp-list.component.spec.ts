import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FspListComponent } from './fsp-list.component';

describe('FspListComponent', () => {
  let component: FspListComponent;
  let fixture: ComponentFixture<FspListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FspListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FspListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
