import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntermediaryComponent } from './intermediary.component';

const routes: Routes = [
    {
      path: '',
      component: IntermediaryComponent
    },
    {
      path: 'detail',
      loadChildren: () => import('./intermediary-specific-detail/intermediary-specific-detail.module').then(m => m.IntermediarySpecificDetailModule)
    },
    {
      path: 'policy-list',
      loadChildren: () => import('./intermediary-policy-list/intermediary-policy-list.module').then(m => m.IntermediaryPolicyListModule)
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntermediaryRoutingModule { }
