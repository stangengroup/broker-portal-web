import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediaryDetailComponent } from './intermediary-detail.component';

describe('IntermediaryDetailComponent', () => {
  let component: IntermediaryDetailComponent;
  let fixture: ComponentFixture<IntermediaryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntermediaryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediaryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
