import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {PolicyDescriptor} from "../../../../../../../interfaces/policy/PolicyDescriptor";
import {PolicyService} from "../../../../../../../services/policy/policy.service";
import {HttpErrorResponse} from "@angular/common/http";
import {SnackbarService} from "../../../../../../../services/snackbar/snackbar.service";
import {StaticService} from "../../../../../../../services/static/static.service";
import {ConfigService} from "../../../../../../../services/config/config.service";

@Component({
  selector: 'app-intermediary-policy-list',
  templateUrl: './intermediary-policy-list.component.html',
  styleUrls: ['./intermediary-policy-list.component.scss']
})
export class IntermediaryPolicyListComponent implements OnInit, OnDestroy {

  private paramSubscription?: Subscription;
  private paramSubscription2?: Subscription;
  private fspId!: string;
  private intermediaryId!: string;

  displayedColumns: string[] = ['policyId', 'policyNumber', 'dateLinked'];
  dataSource!: MatTableDataSource<PolicyDescriptor>;
  policies!: PolicyDescriptor[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public limit: number = 5;
  public page: number = 0;

  public totalElements!: number;

  public loading: boolean = true;
  private subscription!: Subscription;
  private subscription2!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private policyService: PolicyService,
    private snackBarService: SnackbarService,
    private staticService: StaticService,
    private configService: ConfigService
  ) {
    this.getSavedLimit();
    // this.staticService.loading.next(true);

    this.paramSubscription = this.route
      .parent
      ?.parent
      ?.parent
      ?.parent
      ?.params
      .subscribe((params: any) => {
        console.log(`fsp id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.fspId = params['id'];
        }
      });

    this.paramSubscription2 = this.route
      .params
      .subscribe((params: any) => {
        console.log(`intermediary id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.intermediaryId = params['id'];
        }
      });
  }

  getSavedLimit(): void {
    const limit = this.configService.getUserPreferenceFromStorage('pagination-limit');
    if (limit == null) {
      return;
    }
    if (parseInt(limit, 10) > 0) {
      this.limit = parseInt(limit, 10);
    }
  }

  public async asyncUpdateTable(updatePage?: boolean): Promise<any> {
    this.loading = true;
    // this.staticService.loading.next(true);
    this.subscription = this.policyService
      .listIntermediaryPolicies(parseInt(this.intermediaryId, 10), this.limit, this.page)
      .subscribe({
        next: (policies: PolicyDescriptor[]) => {
          this.policies = policies;
          this.totalElements = policies[0].totalElements;
          this.dataSource = new MatTableDataSource(this.policies);
          // Paging
          this.dataSource.sort = this.sort;
          if (!updatePage) {
            this.paginator.pageIndex = 0;
          }
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });
  }

  private updateTable(updatePage: boolean): void {
    this.loading = true;
    // this.staticService.loading.next(true);
    this.subscription2 = this.policyService
      .listIntermediaryPolicies(parseInt(this.intermediaryId, 10), this.limit, this.page)
      .subscribe({
        next: (policies: PolicyDescriptor[]) => {
          this.policies = policies;
          this.dataSource = new MatTableDataSource(this.policies);
          // Paging
          this.dataSource.sort = this.sort;
          if (!updatePage) {
            this.paginator.pageIndex = 0;
          }
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });
  }

  ngOnInit(): void {
    this.updateTable(false);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.paramSubscription?.unsubscribe();
    this.paramSubscription2?.unsubscribe();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navigateToIntermediary(policyDescriptor: PolicyDescriptor): void {
    this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view/${policyDescriptor.id}`]);
  }

  addNewIntermediary(): void {
    this.router.navigate([`home/fsp-manager/fsp/${this.fspId}/intermediary/intermediary-view}`]);
  }

  async setPageSizeOptions(setPageSizeOptionsInput: PageEvent): Promise<PageEvent> {
    this.page = setPageSizeOptionsInput.pageIndex;
    this.limit = setPageSizeOptionsInput.pageSize;
    this.paginator.pageIndex = this.page;
    this.paginator.pageSize = this.limit;
    this.configService.setUserPreferenceFromStorage('pagination-limit', this.limit);
    await this.asyncUpdateTable(true);
    return setPageSizeOptionsInput;
  }

}
