import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntermediaryPolicyListRoutingModule } from './intermediary-policy-list-routing.module';
import { IntermediaryPolicyListComponent } from './intermediary-policy-list.component';
import {SharedModule} from "../../../../../../../shared/shared.module";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTooltipModule} from "@angular/material/tooltip";
import {IntermediaryService} from "../../../../../../../services/intermediary/intermediary.service";
import {IntermediaryApi} from "../../../../../../../api/intermediary.api.class";
import {PolicyService} from "../../../../../../../services/policy/policy.service";
import {PolicyApiClass} from "../../../../../../../api/policy.api.class";
import {ConfigService} from "../../../../../../../services/config/config.service";


@NgModule({
  declarations: [
    IntermediaryPolicyListComponent
  ],
  imports: [
    CommonModule,
    IntermediaryPolicyListRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule
  ],
  providers: [
    PolicyService,
    PolicyApiClass,
    ConfigService
  ]
})
export class IntermediaryPolicyListModule { }
