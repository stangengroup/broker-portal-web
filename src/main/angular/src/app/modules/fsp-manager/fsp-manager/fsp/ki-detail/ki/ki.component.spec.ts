import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KiComponent } from './ki.component';

describe('KiComponent', () => {
  let component: KiComponent;
  let fixture: ComponentFixture<KiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
