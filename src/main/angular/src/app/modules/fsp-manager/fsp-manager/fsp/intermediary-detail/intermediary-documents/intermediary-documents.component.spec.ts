import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntermediaryDocumentsComponent } from './intermediary-documents.component';

describe('IntermediaryDocumentsComponent', () => {
  let component: IntermediaryDocumentsComponent;
  let fixture: ComponentFixture<IntermediaryDocumentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntermediaryDocumentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntermediaryDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
