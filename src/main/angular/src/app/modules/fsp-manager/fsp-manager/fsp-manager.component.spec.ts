import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FspManagerComponent } from './fsp-manager.component';

describe('FspManagerComponent', () => {
  let component: FspManagerComponent;
  let fixture: ComponentFixture<FspManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FspManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FspManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
