import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntermediaryListRoutingModule } from './intermediary-list-routing.module';
import { IntermediaryListComponent } from './intermediary-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import {IntermediaryService} from "../../../../../../services/intermediary/intermediary.service";
import {IntermediaryApi} from "../../../../../../api/intermediary.api.class";
import {ConfigService} from "../../../../../../services/config/config.service";


@NgModule({
  declarations: [
    IntermediaryListComponent
  ],
  imports: [
    CommonModule,
    IntermediaryListRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule
  ],
  providers: [
    IntermediaryService,
    IntermediaryApi,
    ConfigService
  ]
})
export class IntermediaryListModule { }
