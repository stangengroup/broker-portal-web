import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KiDetailComponent } from './ki-detail.component';

const routes: Routes = [
  {
    path: '',
    component: KiDetailComponent,
    children: [
      {
        path: 'ki-list',
        loadChildren: () => import('./ki-list/ki-list.module').then(m => m.KiListModule)
      },
      {
        path: 'ki',
        loadChildren: () => import('./ki/ki.module').then(m => m.KiModule)
      },
      {
        path: 'ki/:id',
        loadChildren: () => import('./ki/ki.module').then(m => m.KiModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KiDetailRoutingModule { }
