import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntermediaryDetailRoutingModule } from './intermediary-detail-routing.module';
import { IntermediaryDetailComponent } from './intermediary-detail.component';


@NgModule({
    declarations: [
        IntermediaryDetailComponent
    ],
    exports: [
        IntermediaryDetailComponent
    ],
    imports: [
        CommonModule,
        IntermediaryDetailRoutingModule
    ]
})
export class IntermediaryDetailModule { }
