import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KiListComponent } from './ki-list.component';

describe('KiListComponent', () => {
  let component: KiListComponent;
  let fixture: ComponentFixture<KiListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KiListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
