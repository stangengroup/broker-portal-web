import { Injectable } from '@angular/core';
import {ConfigDetail} from "../../interfaces/config/ConfigDetail";
import {BehaviorSubject} from "rxjs";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StaticService {

  public configDetail!: BehaviorSubject<ConfigDetail>;
  // public loading: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  public setBehaviourSubject(configDetail: ConfigDetail): void {
    if (!this.configDetail) {
      this.configDetail = new BehaviorSubject<ConfigDetail>(configDetail);
    } else {
      return this.configDetail.next(configDetail);
    }
  }

  // public setLoading(loading: boolean): void {
  //   this.loading.next(loading);
  // }

  private isLoading$$ = new BehaviorSubject<boolean>(false);
  isLoading$ = this.isLoading$$.asObservable();

  setLoading(isLoading: boolean) {
    this.isLoading$$.next(isLoading);
  }

}
