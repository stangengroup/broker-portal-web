import { Injectable } from '@angular/core';
import {IntermediaryDetail} from "../../interfaces/intermediary/IntermediaryDetail";
import {IntermediaryApi} from "../../api/intermediary.api.class";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class IntermediaryService {

  constructor(
    private intermediaryApi: IntermediaryApi
  ) { }

  deleteIntermediary(id: number): Observable<any> {
    return this.intermediaryApi.deleteIntermediary(id);
  }

  getIntermediary(id: number): Observable<any> {
    return this.intermediaryApi.getIntermediary(id);
  }

  listIntermediary(fspId: number, limit: number, page: number): Observable<any> {
    return this.intermediaryApi.listIntermediary(fspId, limit, page);
  }

  listCount(fspId: number): Observable<any> {
    return this.intermediaryApi.listCount(fspId);
  }

  saveIntermediary(intermediaryDetail: IntermediaryDetail): Observable<any> {
    return this.intermediaryApi.saveIntermediary(intermediaryDetail);
  }

  updateIntermediary(intermediaryDetail: IntermediaryDetail, id: number): Observable<any> {
    return this.intermediaryApi.updateIntermediary(intermediaryDetail, id);
  }

  updateStatusIntermediary(fspId: number, statusId: number, id: number): Observable<any> {
    return this.intermediaryApi.updateStatusIntermediary(fspId, statusId, id);
  }

}
