import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FspApi } from 'src/app/api/fsp.api.class';
import { FspDetails } from 'src/app/interfaces/fsp/FspDetails';

@Injectable({
  providedIn: 'root'
})
export class FspService {

  constructor(
    private fspApi: FspApi
  ) {}

  deleteFsp(id: number): Observable<any> {
    return this.fspApi.deleteFsp(id);
  }

  getFsp(id: number): Observable<any> {
    return this.fspApi.getFsp(id);
  }

  listFsp(limit: number, page: number): Observable<any> {
    return this.fspApi.listFsp(limit, page);
  }

  listCount(): Observable<any> {
    return this.fspApi.listCount();
  }

  saveFsp(fspDetails: FspDetails): Observable<any> {
    return this.fspApi.saveFsp(fspDetails);
  }

  updateFsp(fspDetails: FspDetails, id: number): Observable<any> {
    return this.fspApi.updateFsp(fspDetails, id);
  }

  updateStatusFsp(statusId: number, id: number): Observable<any> {
    return this.fspApi.updateStatusFsp(statusId, id);
  }
}
