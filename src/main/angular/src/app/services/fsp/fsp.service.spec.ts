import { TestBed } from '@angular/core/testing';

import { FspService } from './fsp.service';

describe('FspService', () => {
  let service: FspService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FspService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
