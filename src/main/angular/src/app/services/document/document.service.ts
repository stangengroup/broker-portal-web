import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DocumentApi } from 'src/app/api/document.api.class';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(
    private documentApi: DocumentApi
  ) {}

  deleteDocument(id: number): Observable<any> {
    return this.documentApi.deleteDocument(id);
  }

  getDocument(id: number): Observable<any> {
    return this.documentApi.getDocument(id);
  }

  listByFsp(id: number): Observable<any> {
    return this.documentApi.listByFsp(id);
  }

  listByKeyIndividual(id: number): Observable<any> {
    return this.documentApi.listByKeyIndividual(id);
  }

  listByIntermediary(id: number): Observable<any> {
    return this.documentApi.listByIntermediary(id);
  }

  saveByFsp(fspId: number, documentTypeId: number, document: FormData): Observable<any> {
    return this.documentApi.saveByFsp(fspId, documentTypeId, document);
  }

  saveByKeyIndividual(keyIndividualId: number, documentTypeId: number, document: FormData): Observable<any> {
    return this.documentApi.saveByKeyIndividual(keyIndividualId, documentTypeId, document);
  }

  saveByIntermediary(intermediaryId: number, documentTypeId: number, document: FormData): Observable<any> {
    return this.documentApi.saveByIntermediary(intermediaryId, documentTypeId, document);
  }
}
