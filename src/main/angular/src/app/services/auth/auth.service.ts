import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { AuthApiClass } from 'src/app/api/auth.api.class';
import { Session } from 'src/app/interfaces/session/session';
import { SessionState } from 'src/app/state/store/session.state';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {

  constructor(
    private jwtHelperService: JwtHelperService,
    private router: Router,
    private authApi: AuthApiClass
  ) {}

  authSubscription!: Subscription;

  @Select(SessionState.session) session!: Observable<Session>;

  public isAuthenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authSubscription = this.session
        .subscribe({
          next: (res: Session) => {
            console.log('Session returned from store', res.token);
            return resolve(true);
            // if (!this.jwtHelperService.isTokenExpired(res.token)) {
            //   return resolve(true)
            // }
            // return reject(this.router.navigate(['login']));
          },
          error: (err) => {
            console.error('Error caused during canLoad', err);
            return reject(this.router.navigate(['login']));
          }
        });
    });
  }

  b2cLogin(): Promise<any> {
    console.log('B2C Login Service reached ...');
    return this.authApi.adB2cLogin();
  }

  createSession(jwtToken: string): Observable<any> {
    return this.authApi.createSession(jwtToken);
  }

  ngOnDestroy(): void {
    this.authSubscription?.unsubscribe();
  }

}
