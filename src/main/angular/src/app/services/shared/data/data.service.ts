import { Injectable } from '@angular/core';
import {KeyIndividualDetail} from "../../../interfaces/key-individual/KeyIndividualDetail";
import {BehaviorSubject} from "rxjs";
import {FspDetails} from "../../../interfaces/fsp/FspDetails";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public keyIndividualToBroker!: BehaviorSubject<KeyIndividualDetail>;
  public activeFsp!: BehaviorSubject<FspDetails>;
  public scrollToDocuments!: BehaviorSubject<boolean>;

  setCopyIntermediaryData(kiDetail: KeyIndividualDetail) {
    if (this.keyIndividualToBroker == null) {
      this.keyIndividualToBroker = new BehaviorSubject<KeyIndividualDetail>(kiDetail);
    } else {
      this.keyIndividualToBroker.next(kiDetail);
    }
  }

  setScrollToDocuments(scroll: boolean) {
    if (this.scrollToDocuments == null) {
      this.scrollToDocuments = new BehaviorSubject<boolean>(scroll);
    } else {
      this.scrollToDocuments.next(scroll);
    }
  }

  setActiveFsp(activeFsp: FspDetails): void {
    if (this.activeFsp == null) {
      this.activeFsp = new BehaviorSubject<FspDetails>(activeFsp);
      console.log('Set active FSP new', this.activeFsp?.getValue());
    } else {
      this.activeFsp.next(activeFsp);
      console.log('Set active FSP next', this.activeFsp?.getValue());
    }
  }

  constructor() { }
}
