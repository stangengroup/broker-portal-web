import {Injectable, OnDestroy} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/generic-components/dialog/confirm-dialog/confirm-dialog.component';
import { DialogData } from 'src/app/interfaces/generic/DialogData';
import { SnackbarService } from '../../snackbar/snackbar.service';
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DialogService implements OnDestroy {

  private subscription!: Subscription;

  constructor(
    private matDialog: MatDialog,
    private matDialogRef: MatDialogRef<any>,
    private snackBarService: SnackbarService
  ) { }

  openDialog(dialogData: DialogData): MatDialogRef<any> {
    const matDialogRef: MatDialogRef<any> = this.matDialog
      .open(ConfirmDialogComponent, {
        data: {
          buttonText: dialogData.buttonText,
          description: dialogData.description,
          title: dialogData.title
        },
        width: '400px',
        height: '260px'
      });

      // this.subscription = matDialogRef
      // .afterClosed()
      // .subscribe({
      //   next: () => {
      //     this.snackBarService.showSnackBar(dialogData.successMessage);
      //   },
      //   error: () => {
      //     this.snackBarService.showSnackBar(dialogData.failureMesssage);
      //   }
      // });

      return matDialogRef;
  }

  ngOnDestroy() {
    // this.subscription?.unsubscribe();
  }

}
