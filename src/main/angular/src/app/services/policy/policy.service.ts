import { Injectable } from '@angular/core';
import {PolicyApiClass} from "../../api/policy.api.class";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PolicyService {

  constructor(
    private policyApi: PolicyApiClass
  ) { }

  getIntermediaryPolicy(id: number): Observable<any> {
    return this.policyApi.getIntermediaryPolicy(id);
  }

  listIntermediaryPolicies(intermediaryId: number, limit: number, page: number): Observable<any> {
    return this.policyApi.listIntermediaryPolicies(intermediaryId, limit, page);
  }

}
