import { Injectable } from '@angular/core';
import {KeyIndividualDetail} from "../../interfaces/key-individual/KeyIndividualDetail";
import {KiApi} from "../../api/ki.api.class";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class KiService {

  constructor(
    private kiApi: KiApi
  ) { }

  deleteKeyIndividual(id: number): Observable<any> {
    return this.kiApi.deleteKeyIndividual(id);
  }

  getKeyIndividual(id: number): Observable<any> {
    return this.kiApi.getKeyIndividual(id);
  }

  listKeyIndividual(fspId: number, limit: number, page: number): Observable<any> {
    return this.kiApi.listKeyIndividual(fspId, limit, page);
  }

  listCount(fspId: number): Observable<any> {
    return this.kiApi.listCount(fspId);
  }

  saveKeyIndividual(kiDetail: KeyIndividualDetail): Observable<any> {
    return this.kiApi.saveKeyIndividual(kiDetail);
  }

  updateKeyIndividual(kiDetail: KeyIndividualDetail, id: number): Observable<any> {
    return this.kiApi.updateKeyIndividual(kiDetail, id);
  }

  updateStatusKeyIndividual(fspId: number, statusId: number, id: number): Observable<any> {
    return this.kiApi.updateStatusKeyIndividual(fspId, statusId, id);
  }
}
