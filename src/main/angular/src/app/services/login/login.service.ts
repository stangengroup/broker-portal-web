import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginApi } from 'src/app/api/login.api.class';
import { Login } from 'src/app/interfaces/login/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private loginApi: LoginApi
  ) { }

  login(login: Login): Observable<any> {
    return this.loginApi.login(login);
  }

}
