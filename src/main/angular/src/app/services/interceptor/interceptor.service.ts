import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from "@angular/common/http";
import {map, tap} from "rxjs";
import {Observable} from "rxjs";
import {StaticService} from "../static/static.service";
import {finalize} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  private totalRequests = 0;

  constructor(
    private staticService: StaticService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    this.totalRequests++;
    this.staticService.setLoading(true);

    return next.handle(request).pipe(
      finalize(() => {
        this.totalRequests--;
        if (this.totalRequests === 0) {
          this.staticService.setLoading(false);
        }
      })
    );
  }
}
