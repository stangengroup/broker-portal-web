import { HttpErrorResponse } from '@angular/common/http';
import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { MsalBroadcastService, MsalGuardConfiguration, MsalService, MSAL_GUARD_CONFIG } from '@azure/msal-angular';
import { EventType, InteractionStatus, RedirectRequest } from '@azure/msal-browser';
import { Select, Store } from '@ngxs/store';
import { filter, Observable, Subject, takeUntil } from 'rxjs';
import { ConfigDetail } from './interfaces/config/ConfigDetail';
import { Session } from './interfaces/session/session';
import { AuthService } from './services/auth/auth.service';
import { ConfigService } from './services/config/config.service';
import { SetSession } from './state/actions/session.action';
import { SessionState } from './state/store/session.state';
import {StaticService} from "./services/static/static.service";
import {SnackbarService} from "./services/snackbar/snackbar.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'kp-broker-portal';
  session!: Session;
  loginDisplay = false;
  isIframe = false;
  shouldLogin!: boolean;
  private readonly _destroying$ = new Subject<void>();

  @Select(SessionState.session) $session!: Observable<Session>;

  public loading: boolean = true;

  private subscription!: Subscription;
  private subscription2!: Subscription;
  private subscription3!: Subscription;
  private subscription4!: Subscription;
  private subscription5!: Subscription;
  private subscription6!: Subscription;
  private subscription7!: Subscription;
  private loadingSubscription!: Subscription;

  constructor(
    private router: Router,
    private store: Store,
    private route: ActivatedRoute,
    // @Inject(MSAL_GUARD_CONFIG) private msalGuardConfig: MsalGuardConfiguration,
    private authService: MsalService,
    private sessionService: AuthService,
    private broadcastService: MsalBroadcastService,
    private configService: ConfigService,
    public staticService: StaticService,
    private snackbarService: SnackbarService,
    private cdr: ChangeDetectorRef
  ) {

    // navigator.permissions.query({name:'notifications'}).then(function(result) {
    //   if (result.state == 'granted') {
    //     snackbarService.showSnackBar('Popup permission is granted');
    //   } else if (result.state == 'prompt') {
    //     snackbarService.showSnackBar('Popup permission is not granted', 'Please grant');
    //   }
    //   // Don't do anything if the permission was denied.
    // });

    // Router
    // router.events
    // .pipe(filter(event => event instanceof NavigationStart))
    // // @ts-ignore
    // .subscribe((event: NavigationStart) => {
      // Dispatch current route to store for use in other components.
      // this.store.dispatch(new SetRoute(event.url));
      // If LoginComponent active, don't do session retrieval call.

      this.subscription = this.$session.subscribe({
        next: (res: Session) => console.log(res),
        error: (err) => console.error(err)
      });

    // this.subscription2 = this.router.events
    //   .subscribe((events) => {
    //     if (events instanceof NavigationEnd) {
          console.log('Login if account length 0');
          if (this.authService.instance.getAllAccounts().length === 0) {
            this.shouldLogin = true;
          }
        // }
      // });

      // if (!router.navigated) {
      //   if (event.url !== '/login') {
      //     this.store.dispatch(new SetSession());
      //   }
      // }
    // });
  }

  // login() {
  //   if (this.msalGuardConfig.authRequest){
  //     this.authService.loginRedirect({...this.msalGuardConfig.authRequest} as RedirectRequest);
  //   } else {
  //     this.authService.loginRedirect();
  //   }
  // }

  // setLoginDisplay() {
  //   this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  // }

  // ngOnInit(): void {
  //   console.log('ZZZ');

  //   this.isIframe = window !== window.parent && !window.opener;

  //   // Account selection logic is app dependent. Adjust as needed for different use cases.
  //   // Set active acccount on page load
  //   // const accounts = this.authService.instance.getAllAccounts();
  //   // if (accounts.length > 0) {
  //   //   this.authService.instance.setActiveAccount(accounts[0]);
  //   // }

  //   // this.authService.instance.addEventCallback((event: any) => {
  //   //   // set active account after redirect
  //   //   if (event.eventType === EventType.LOGIN_SUCCESS && event.payload.account) {
  //   //     const account = event.payload.account;
  //   //     this.authService.instance.setActiveAccount(account);
  //   //   }
  //   // });

  //   console.log('get active account', this.authService.instance.getActiveAccount());

  //   // handle auth redired/do all initial setup for msal
  //   // this.authService.instance.handleRedirectPromise().then(authResult=>{
  //   //   // Check if user signed in
  //   //   console.log('AUTH RESULT', authResult);
  //   //   const account = this.authService.instance.getActiveAccount();
  //   //   if(!account){
  //   //     // redirect anonymous user to login page
  //   //     this.authService.instance.loginRedirect();
  //   //   }
  //   // }).catch(err =>{
  //   //   // TODO: Handle errors
  //   //   console.log(err);
  //   // });

  //   // this.broadcastService.inProgress$
  //   // .pipe(
  //   //   filter((status: InteractionStatus) => status === InteractionStatus.None),
  //   //   takeUntil(this._destroying$)
  //   // )
  //   // .subscribe((res) => {
  //   //   console.log('Broadcast service return...', res);
  //   //   this.setLoginDisplay();
  //   // });

  //   // if (this.authService?.instance?.getActiveAccount() == null) {
  //   //   this.login();
  //   // }

  // }

  ngOnInit() {
    this.isIframe = window !== window.parent && !window.opener;

    // this.subscription3 = this.broadcastService.inProgress$
    // .pipe(
    //   filter((status: InteractionStatus) => status === InteractionStatus.None),
    //   takeUntil(this._destroying$)
    // )
    // .subscribe(() => {
    //   this.setLoginDisplay();
    // });

    console.log(this.authService.instance.getActiveAccount());

    if (!this.shouldLogin) {
      if (this.authService.instance.getAllAccounts().length === 0) {
        this.login();
      } else {
        this.router.navigate(['/home']);
      }

      this.snackbarService.showSnackBar('Retrieving config data', 'Please wait');

      this.subscription4 = this.configService.getAllConfigData()
        .subscribe({
          next: (data: ConfigDetail) => {
            console.log(data);
            this.loading = false;
            this.snackbarService.showSnackBar('Config data retrieved');
            this.staticService.setBehaviourSubject(data);
          },
          error: (err) => this.snackbarService.showSnackBar('Could not retrieve config data', 'Please refresh')
        });
    }

    // this.subscription5 = this.staticService
    //   .loading
    //   .subscribe({
    //     next: (bool: boolean) => {
    //       this.loading = bool;
    //     },
    //     error: (err: HttpErrorResponse) => {
    //       return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
    //     }
    //   })

    // Promise.all(this.staticService.loadingPromiseArr)
    //   .then((value: boolean[]) => this.loading = true)
    //   .catch(reason => console.error(reason));

  }

  ngAfterViewInit() {
    this.loadingSubscription = this.staticService
      .isLoading$
      .subscribe({
        next: () => this.cdr.detectChanges(),
        error: () => this.cdr.detectChanges()
      });
  }

  receiveLogin(login: boolean) {
    if (login) {
      this.login();
    }
  }

  login() {
    this.subscription6 = this.authService.loginPopup()
      .subscribe({
        next: (result) => {
          console.log(result);
          this.authService.instance.setActiveAccount(result.account);
          this.setLoginDisplay();
          if (result && this.authService.instance.getAllAccounts().length > 0) {
            this.subscription7 = this.sessionService.createSession(result.accessToken)
              .subscribe({
                next: () => {
                  console.log('return from session creation');
                  this.router.navigate(['home']);
                },
                error: (err) => console.error(err)
              })
          }
        },
        error: (error) => console.log(error)
      });
  }

  setLoginDisplay() {
    this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  }

  ngOnDestroy(): void {
    this._destroying$.next(undefined);
    this._destroying$.complete();
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
    this.subscription5?.unsubscribe();
    this.subscription6?.unsubscribe();
    this.subscription7?.unsubscribe();
    this.loadingSubscription?.unsubscribe();
  }
}
