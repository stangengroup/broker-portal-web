import {Route} from './Route';

export interface RouteChild {
  route: Route;
  children: Route[];
}
