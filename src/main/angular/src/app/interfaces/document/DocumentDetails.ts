import {EntityDetails} from "../entity/EntityDetails";
import {NamedEntityDetails} from "../entity/NamedEntityDetails";

export interface DocumentDetails extends EntityDetails {
  name: string;
  documentType: NamedEntityDetails;
  url: string;
}
