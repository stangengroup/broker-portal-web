import {EntityDetails} from "./EntityDetails";

export interface NamedEntityDetails extends EntityDetails {
  name: string;
}
