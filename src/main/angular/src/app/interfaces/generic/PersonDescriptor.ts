import {EntityDetails} from "../entity/EntityDetails";

export interface PersonDescriptor extends EntityDetails {
  contactNumber: string;
  created: string;
  emailAddress: string;
  name: string;
}
