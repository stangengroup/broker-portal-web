import { NamedEntityDetails } from "../entity/NamedEntityDetails";

export interface Commission {
  asAndWhen: number;
  upfront: number;
  commissionStructureType: NamedEntityDetails;
}
