import { EntityDetails } from "../entity/EntityDetails";
import { Address } from "./Address";

export interface PersonDetails extends EntityDetails {
  alternativeContactNumber: string;
  alternativeEmailAddress: string;
  emailAddress: string;
  fullName: string;
  idNumber: string;
  mobileNumber: string;
  title: string;
  surname: string;
  workNumber: string;
  address: Address;
  password?: string;
}
