import { NamedEntityDetails } from "../entity/NamedEntityDetails";

export interface Fees {
  percentage: number;
  feeStructureType: NamedEntityDetails;
}
