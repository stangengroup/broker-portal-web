export interface Address {
  streetNumber: string;
  streetName: string;
  streetName2: string;
  suburb: string;
  town: string;
  province: string;
  postalCode: string;
}
