import { PersonDetails } from "../generic/PersonDetails";

export interface KeyIndividualDetail extends PersonDetails {
  fspId: number;
  active: boolean;
  keyIndividualId?: number;
}
