export interface FspDescriptor {
  created: string;
  fspRegNumber: string;
  fspStatus: string;
  name: string;
  id: number;
}
