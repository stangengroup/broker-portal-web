import { EntityDetails } from "../entity/EntityDetails";
import { Address } from "../generic/Address";
import { BankingDetails } from "../generic/BankingDetails";
import { Commission } from "../generic/Commission";
import { Fees } from "../generic/Fees";
import {NamedEntityDetails} from "../entity/NamedEntityDetails";

export interface FspDetails extends EntityDetails {
  altContactNumber: string;
  altEmailAddress: string;
  companyRegNumber: string;
  mobileNumber: string;
  emailAddress: string;
  domainName: string;
  fspRegNumber: string;
  taxNumber: string;
  tradingAs: string;
  vatNumber: string;
  workNumber: string;
  address: Address;
  bankingDetails: BankingDetails;
  commission: Commission;
  fees: Fees;
  fspStatus: NamedEntityDetails;
}
