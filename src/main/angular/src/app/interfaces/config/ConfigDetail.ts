import { NamedEntityDetails } from "../entity/NamedEntityDetails";

export interface ConfigDetail {
  commissionStructureTypeList: NamedEntityDetails[];
  commsPreferenceList: NamedEntityDetails[];
  documentTypeList: NamedEntityDetails[];
  feeStructureTypeList: NamedEntityDetails[];
  fspStatusList: NamedEntityDetails[];
  intermediaryTypeList: NamedEntityDetails[];
  policyTypeList: NamedEntityDetails[];
  qualificationTypeList: NamedEntityDetails[];
}
