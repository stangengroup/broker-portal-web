import {Commission} from "../generic/Commission";
import {NamedEntityDetails} from "../entity/NamedEntityDetails";

export interface PolicyDetails {
  number: string;
  premium: number;
  commission: Commission;
  commsPreference: NamedEntityDetails;
  policyType: NamedEntityDetails;
  id: number;
}
