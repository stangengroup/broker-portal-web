export interface PolicyDescriptor {
  created: string;
  policyNumber: string;
  id: number;
  totalElements: number;
}
