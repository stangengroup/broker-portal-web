import { NamedEntityDetails } from "../entity/NamedEntityDetails";
import { Commission } from "../generic/Commission";
import { PersonDetails } from "../generic/PersonDetails";

export interface IntermediaryDetail extends PersonDetails {
  brokerCode: string;
  fspId: number;
  commission: Commission;
  commsPreference: NamedEntityDetails;
  intermediaryAccreditationTestState: NamedEntityDetails;
  intermediaryType: NamedEntityDetails;
  active: boolean;
  intermediaryId?: number;
  intermediaryHome?: string;
}
