import {PersonDescriptor} from "../generic/PersonDescriptor";

export interface IntermediaryDescriptor extends PersonDescriptor {
  brokerCode: string;
}
