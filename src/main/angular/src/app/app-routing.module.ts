import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular';
import { AppComponent } from './app.component';
import { AuthGuard } from './guards/auth.guard';
import { MsalContainerGuard } from './guards/msal-container/msal-container.guard';
import { LoginComponent } from './login/login.component';

const routes: Routes = [

  // {
  //   path: 'login',
  //   loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  // },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    canLoad: [MsalContainerGuard]
  },
  {
    path: '',
    component: AppComponent
  },
  {
    path: '**',
    component: AppComponent
  },
  // { path: 'shared', loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule) },
];

const isIframe = window !== window.parent && !window.opener;
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: !isIframe ? 'enabled' : 'disabled' // Don't perform initial navigation in iframes
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
