import { Injectable } from "@angular/core";
import { Action, Select, Selector, State, StateContext } from "@ngxs/store";
import { Session } from "src/app/interfaces/session/session";
import { SetSession } from "../actions/session.action";

export class SessionModel {
  session!: Session
}

@State<string[]>({
  name: 'session',
  defaults: []
})
@Injectable()
export class SessionState {

  @Selector()
  static session(state: Session): Session {
    return state;
  }

  constructor() {}

  @Action(SetSession)
  session(patchState: StateContext<SessionModel>, {session}: SetSession): void {
    const state = patchState.getState();
    patchState.setState({
      ...state,
      session
    });
  }

}
