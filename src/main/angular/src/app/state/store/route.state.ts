import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {SetRoute} from '../actions/route.action';

export class RouteModel {
  route!: string;
}
@State<RouteModel>({
  name: 'route',
  defaults: {
    // @ts-ignore
    route: {}
  }
})
@Injectable()
export class RouteState {
  @Selector()
  static route(state: RouteModel): string {
    return state.route;
  }

  constructor() {}

  @Action(SetRoute)
  route(patchState: StateContext<RouteModel>, {route}: SetRoute): void {
    const state = patchState.getState();
    patchState.setState({
      ...state,
      route
    });
  }

}
