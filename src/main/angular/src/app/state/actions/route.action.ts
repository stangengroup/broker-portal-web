export class SetRoute {
  static readonly type = '[Navigation] Set Route';
  constructor(public route: string) {
  }
}
