import { Session } from "src/app/interfaces/session/session";

export class SetSession {
  static readonly type = '[Token] Add Session';
  constructor(public session: Session) {}
}
