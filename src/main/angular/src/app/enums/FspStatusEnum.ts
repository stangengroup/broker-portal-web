export enum FspStatusEnum {
  APPROVED = 'Approved',
  INACTIVE = 'Inactive',
  PENDING = 'Pending',
}
