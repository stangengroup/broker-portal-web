import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class PolicyApiClass extends HttpClient {

  baseUrl = environment.baseUrl + '/policy';

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  getIntermediaryPolicy(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/${id}`);
  }

  listIntermediaryPolicies(intermediaryId: number, limit: number, page: number): Observable<any> {
    return super.get(`${this.baseUrl}/intermediary/${intermediaryId}/${limit}/${page}`);
  }

  // saveIntermediary(intermediaryDetail: IntermediaryDetail): Observable<any> {
  //   return super.post(`${this.baseUrl}`, intermediaryDetail);
  // }

}
