import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { FspDetails } from "../interfaces/fsp/FspDetails";
import { KeyIndividualDetail } from "../interfaces/key-individual/KeyIndividualDetail";

@Injectable()
export class KiApi extends HttpClient {

  baseUrl = environment.baseUrl + '/key-individual';

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  deleteKeyIndividual(id: number): Observable<any> {
    return super.delete(`${this.baseUrl}/${id}`);
  }

  getKeyIndividual(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/${id}`);
  }

  listKeyIndividual(fspId: number, limit: number, page: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/${fspId}/${limit}/${page}`);
  }

  listCount(fspId: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/count/${fspId}`);
  }

  saveKeyIndividual(KiDetail: KeyIndividualDetail): Observable<any> {
    return super.post(`${this.baseUrl}`, KiDetail);
  }

  updateKeyIndividual(KiDetail: KeyIndividualDetail, id: number): Observable<any> {
    return super.put(`${this.baseUrl}/${id}`, KiDetail);
  }

  updateStatusKeyIndividual(fspId: number, statusId: number, id: number): Observable<any> {
    return super.put(`${this.baseUrl}/${id}/status/${statusId}`, null);
  }

}
