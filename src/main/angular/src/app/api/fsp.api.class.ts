import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { FspDetails } from "../interfaces/fsp/FspDetails";

@Injectable()
export class FspApi extends HttpClient {

  baseUrl = environment.baseUrl + '/fsp';

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  deleteFsp(id: number): Observable<any> {
    return super.delete(`${this.baseUrl}/${id}`);
  }

  getFsp(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/${id}`);
  }

  listFsp(limit: number, page: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/${limit}/${page}`);
  }

  listCount(): Observable<any> {
    return super.get(`${this.baseUrl}/list/count`);
  }

  saveFsp(fspDetails: FspDetails): Observable<any> {
    return super.post(`${this.baseUrl}`, fspDetails);
  }

  updateFsp(fspDetails: FspDetails, id: number): Observable<any> {
    return super.put(`${this.baseUrl}/${id}`, fspDetails);
  }

  updateStatusFsp(statusId: number, id: number): Observable<any> {
    return super.put(`${this.baseUrl}/${id}/status/${statusId}`, null);
  }

}
