import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class ConfigApi extends HttpClient {

  baseUrl = environment.baseUrl + '/config';

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  getAllConfigData(): Observable<any> {
    return super.get(`${this.baseUrl}/all`);
  }

}
