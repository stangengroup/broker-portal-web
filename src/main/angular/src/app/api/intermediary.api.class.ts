import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { IntermediaryDetail } from "../interfaces/intermediary/IntermediaryDetail";

@Injectable()
export class IntermediaryApi extends HttpClient {

  baseUrl = environment.baseUrl + '/intermediary';

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  deleteIntermediary(id: number): Observable<any> {
    return super.delete(`${this.baseUrl}/${id}`);
  }

  getIntermediary(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/${id}`);
  }

  listIntermediary(fspId: number, limit: number, page: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/${fspId}/${limit}/${page}`);
  }

  listCount(fspId: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/count/${fspId}`);
  }

  saveIntermediary(intermediaryDetail: IntermediaryDetail): Observable<any> {
    return super.post(`${this.baseUrl}`, intermediaryDetail);
  }

  updateIntermediary(intermediaryDetail: IntermediaryDetail, id: number): Observable<any> {
    return super.put(`${this.baseUrl}/${id}`, intermediaryDetail);
  }

  updateStatusIntermediary(fspId: number, statusId: number, id: number): Observable<any> {
    return super.put(`${this.baseUrl}/${id}/status/${statusId}`, null);
  }

}
