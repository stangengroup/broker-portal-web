import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class DocumentApi extends HttpClient {

  baseUrl = environment.baseUrl + '/document';
  baseApiUrl = environment.baseApiUrl;

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  deleteDocument(id: number): Observable<any> {
    return super.delete(`${this.baseUrl}/${id}`);
  }

  getDocument(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/${id}`);
  }

  listByFsp(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/fsp/${id}`);
  }

  listByKeyIndividual(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/key-individual/${id}`);
  }

  listByIntermediary(id: number): Observable<any> {
    return super.get(`${this.baseUrl}/list/intermediary/${id}`);
  }

  saveByFsp(fspId: number, documentTypeId: number, document: FormData): Observable<any> {
    return super.post(`${this.baseUrl}/fsp/${fspId}/${documentTypeId}`, document);
  }

  saveByKeyIndividual(keyIndividualId: number, documentTypeId: number, document: FormData): Observable<any> {
    return super.post(`${this.baseUrl}/key-individual/${keyIndividualId}/${documentTypeId}`, document);
  }

  saveByIntermediary(intermediaryId: number, documentTypeId: number, document: FormData): Observable<any> {
    return super.post(`${this.baseUrl}/intermediary/${intermediaryId}/${documentTypeId}`, document);
  }

}
