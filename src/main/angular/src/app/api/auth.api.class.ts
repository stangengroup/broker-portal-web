import { HttpClient, HttpHandler, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from '../../environments/environment';

export interface B2cAuth {
  tenant: string;
  policy: string;
  clientId: string;
  response_type: 'code' | 'token';
  redirect_uri: string;
  scope: string[];
  response_mode: 'query' | 'form_post' | 'fragment';
  state: string;
  code_challenge: string;
  code_challenge_method: 'S256' | 'plain';
  prompt?: 'login';
  login_hint?: string;
  domain_hint?: string;
  custom_parameters?: string;
}

@Injectable()
export class AuthApiClass extends HttpClient {

  baseUrl = environment.baseUrl + '/auth';

  constructor(private httpHandler: HttpHandler) {
    super(httpHandler);
  }

  async adB2cLogin(): Promise<any> {

    console.log('B2C Api reached ...');

    const codeVerifier: string = makeid(64);
    const challengeBytes: string = await digestMessage(codeVerifier);

    const b2cAuth: B2cAuth = {
      tenant: 'brokerportaltest',
      policy: 'B2C_1_signupsignin1',
      clientId: '840256e9-4165-4ee8-84f9-0967da0f90ef',
      response_type: 'code',
      redirect_uri: 'https://brokeradmin.kingpricelife.co.za',
      scope: ['openid', 'offline_access', 'profile'],
      response_mode: 'query',
      state: Math.floor(Math.random() * 1000).toString(),
      code_challenge: encodeURIComponent(challengeBytes),
      code_challenge_method: 'S256',
      prompt: 'login'
    };

    console.log(b2cAuth);

    const httpParams = new HttpParams()
      .set('client_id', b2cAuth.clientId)
      .set('response_type', b2cAuth.response_type)
      .set('redirect_uri', encodeURIComponent(b2cAuth.redirect_uri))
      .set('scope', b2cAuth.scope.toString().replaceAll(',', '%20'))
      .set('response_mode', b2cAuth.response_mode)
      .set('state', b2cAuth.state)
      .set('response_type', b2cAuth.response_type)
      .set('code_challenge', b2cAuth.code_challenge)
      .set('code_challenge_method', b2cAuth.code_challenge_method);

    console.log(httpParams);

    return super.get(`https://${b2cAuth.tenant}.b2clogin.com/${b2cAuth.tenant}.onmicrosoft.com/${b2cAuth.policy}/oauth2/v2.0/authorize`, {
      params: httpParams
    });

    // https://brokerportaltest.b2clogin.com/brokerportaltest.onmicrosoft.com/B2C_1_signupsignin1/oauth2/v2.0/authorize
  }

  createSession(jwtToken: string): Observable<any> {
    return super.get(`${this.baseUrl}/session`, {
      headers: {
        'Authorization': `${jwtToken}`
      }
    });
  }

}

async function digestMessage(message: string): Promise<string> {
  const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
  return hashHex;
}

function makeid(length: number) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() *
      charactersLength));
  }
  return result;
}

