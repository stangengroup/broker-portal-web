import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpParams } from '@angular/common/http';
import { Observable, Observer, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login } from '../interfaces/login/login';

export interface LoginApiResponse {
  result: string;
  message: string;
  data: string;
}

@Injectable()
export class LoginApi extends HttpClient {

  baseUrl = environment.baseUrl + '/oauth/token';

  constructor(
    private httpHandler: HttpHandler
  ) {
    super(httpHandler);
  }

  login(login: Login): Observable<any> {
    // return super.post(this.baseUrl, login, {
    //   params: new HttpParams()
    //     .set('grant_type', 'password')
    // });

    // const mockObservable = of({
    //   result: 'SUCCESS',
    //   message: '"User" has successfully logged in',
    //   data: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJ1c2VySWQiOjF9.5T1Ak2JoBnsLBOytxgsV39SXqR7VthL9AYfGlOcZz7A'
    // });

    const mockObserver = {
      next: (x: LoginApiResponse) => console.log('Observer got a next value: ' + x),
      error: (err: Error) => console.error('Observer got an error: ' + err),
      complete: () => console.log('Observer got a complete notification'),
    };

    return new Observable(this.sequenceSubscriber);
  }

  sequenceSubscriber(observer: Observer<LoginApiResponse>) {
    // synchronously deliver 1, 2, and 3, then complete

    const random = Math.floor(Math.random() * (100 - 1 + 1) + 1);

    setTimeout(() => {
      if (random >= 50) {
        observer.next({
          result: 'SUCCESS',
          message: '"User" has successfully logged in',
          data: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJ0dGwiOjE1OTE1MjM5MDIsInVzZXJJZCI6MX0.cEo5mrnHBEf-R1weI5YfbnuuuODuOfWVk4t7PgU8TPc'
        });
      } else {
        observer.next({
          result: 'FAILURE',
          message: '"User" has entered incorrect credentials',
          data: ''
        });
      }

      observer.complete();
    }, 2500);

    // unsubscribe function doesn't need to do anything in this
    // because values are delivered synchronously
    return {unsubscribe() {}};
  }

}
