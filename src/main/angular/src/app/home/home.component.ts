import { MediaMatcher } from '@angular/cdk/layout';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { MsalBroadcastService, MsalService } from '@azure/msal-angular';
import { EventMessage, EventType, InteractionStatus } from '@azure/msal-browser';
import { Select } from '@ngxs/store';
import { filter, Observable, Subscription } from 'rxjs';
import { NavMenuItem } from '../interfaces/Route/NavMenuItem';
import { Route } from '../interfaces/Route/Route';
import { RouteState } from '../state/store/route.state';

export const staticRouteConfig = {
  fspManagement: {
    name: 'FSP Manager',
    route: 'fsp-manager',
    code: 'FSP_MANAGER',
    active: false
  }
};

type ProfileType = {
  givenName?: string,
  surname?: string,
  userPrincipalName?: string,
  id?: string
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  profile!: ProfileType;

  svgImages = {
    // fspManager: `<g id="shape-admin" data-name="shape-admin"><path class="a" d="M85.574,107.659V97.269a1.269,1.269,0,0,0-2.538,0v10.38a3.807,3.807,0,0,0,0,7.148v5.314a1.269,1.269,0,1,0,2.538,0v-5.3a3.807,3.807,0,0,0,0-7.148Z" transform="translate(-74.153 -88.386)"/><path class="a" d="M45.438,19.5a4.222,4.222,0,0,0-4.831-2.176V8.883A8.883,8.883,0,0,0,31.725,0H8.883A8.883,8.883,0,0,0,0,8.883V31.725a8.883,8.883,0,0,0,8.883,8.883h9.711a4.069,4.069,0,0,0,2.793,3.615l14.7,4.821a13.84,13.84,0,0,0,10.632-.809,11.844,11.844,0,0,0,5.215-15.9Zm-42.9,12.22V8.883A6.345,6.345,0,0,1,8.883,2.538H31.725a6.345,6.345,0,0,1,6.345,6.345v9.444a4.217,4.217,0,0,0-4.81,1.031,4.222,4.222,0,0,0-1.535-.55V8.883a1.269,1.269,0,0,0-2.538,0V19.211h0a4.216,4.216,0,0,0-.726.467L26.5,15.813a4.187,4.187,0,0,0-2.449-2.1c-.117-.038-.238-.071-.354-.1a3.807,3.807,0,0,0-2.128-1.954V8.883a1.269,1.269,0,1,0-2.538,0v2.766a3.807,3.807,0,0,0-.416,6.961,4.237,4.237,0,0,0,.36,1.012l.056.111V31.725a1.269,1.269,0,0,0,2.538,0V24.745l5.73,11.32-3.853.018a5.938,5.938,0,0,0-2.638.634,4.035,4.035,0,0,0-1.526,1.348H8.883a6.345,6.345,0,0,1-6.345-6.342Zm43.04,14.258a11.313,11.313,0,0,1-8.7.659l-14.707-4.82a1.545,1.545,0,0,1-.22-2.845,3.41,3.41,0,0,1,1.511-.364l6.569-.033a.847.847,0,0,0,.714-.4.944.944,0,0,0,.054-.105.847.847,0,0,0-.021-.714L21.231,18.48a1.692,1.692,0,0,1,3.019-1.528l7.259,14.342a.846.846,0,0,0,1.507-.765l-3.433-6.791A1.692,1.692,0,0,1,32.6,22.207L36.035,29a.846.846,0,0,0,1.507-.764l-2.669-5.281a1.692,1.692,0,0,1,3.019-1.528l.177.35,2.5,4.934a.788.788,0,0,0,.04.071.845.845,0,0,0,1.469-.835l-1.469-2.9-.441-.872a1.692,1.692,0,1,1,3.019-1.528L49.68,33.476a9.317,9.317,0,0,1-4.1,12.507Z" transform="translate(0)"/></g>`,
    fspManager: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4" /></svg>`
  };

  menuItems: Route[] = [];
  navMenuItems: NavMenuItem[] = [];
  menuItemsHovered: Route[] = [];
  activeNavMenuItem!: NavMenuItem | null;
  navMenuItemHovered: boolean = false;
  route!: string;
  @Select(RouteState.route) $route!: Observable<string>;
  private routeObservable!: Subscription;

  private subscription1!: Subscription;
  private subscription2!: Subscription;

  routeConfig: {allowedRoute: string}[] = [{ allowedRoute: 'FSP_MANAGER' }];
  loginDisplay = false;

  constructor(
    private authService: MsalService,
    private msalBroadcastService: MsalBroadcastService,
    private http: HttpClient,
    private router: Router
  ) {

  }

  routeHome(): void {
    if (this.navMenuItems.length === 1) {
      this.router.navigate(['/home/fsp-manager'])
    } else {
      this.router.navigate(['/home'])
    }
  }

  routerLink(): string[] {
    if (this.navMenuItems.length === 1) {
      return ['/home/fsp-manager'];
    } else {
      return ['/home'];
    }
  }

  async constructingMenuItems() {
    await this.constructMenuItems();
    if (this.navMenuItems.length === 1) {
      this.router.navigate(['/home/fsp-manager'])
    }
  }

  ngOnDestroy(): void {
    this.subscription1?.unsubscribe();
    this.subscription2?.unsubscribe();
  }

  ngOnInit(): void {

    console.log('ngoninit reached...');

    this.subscription1 = this.msalBroadcastService.msalSubject$
      .pipe(
        filter((msg: EventMessage) => msg.eventType === EventType.LOGIN_SUCCESS),
      )
      .subscribe((result: EventMessage) => {
        console.log(result);
      });

      this.subscription2 = this.msalBroadcastService.inProgress$
      .pipe(
        filter((status: InteractionStatus) => status === InteractionStatus.None)
      )
      .subscribe(() => {
        this.setLoginDisplay();
      });

      this.constructingMenuItems();

      // this.getProfile();
  }

  constructMenuItems(): void {
    this.navMenuItems = [];

    for (const elt of this.routeConfig) {
      console.log(elt);
      switch (elt.allowedRoute) {
        case 'FSP_MANAGER': {
          console.log('FSPMANAGER');
          // this.menuItems.push(staticRouteConfig.transactionManagement);
          if (this.navMenuItems.length === 0 ) {
            this.navMenuItems.push({
              name: 'FSP Manager',
              code: 'FSP_MANAGER',
              headerText: 'FSP Manager',
              image: this.svgImages.fspManager,
              route: 'fsp-manager',
              active: false
            });
          }
          break;
        }
      }
    }
    this.navMenuItems.sort((a, b) => {
      const nameA = a.code.toUpperCase();
      const nameB = b.code.toUpperCase();
      if (nameA > nameB) {
        return 1;
      }
      if (nameA < nameB) {
        return -1;
      }
      // names must be equal
      return 0;
    });
  }

  enterHoveredMode(navMenuItem: NavMenuItem): void {
    this.navMenuItemHovered = true;
    this.activeNavMenuItem = navMenuItem;
  }

  exitHoveredMode(): void {
    this.navMenuItemHovered = false;
    this.activeNavMenuItem = null;
    this.menuItemsHovered = [];
  }

  setLoginDisplay() {
    this.loginDisplay = this.authService.instance.getAllAccounts().length > 0;
  }

  // logout() { // Add log out function here
  //   this.authService.logoutRedirect({
  //     postLogoutRedirectUri: 'http://localhost:4200'
  //   });
  // }

}
