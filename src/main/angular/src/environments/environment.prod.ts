export const environment = {
  production: true,
  baseUrl: 'https://brokeradmin.kingpricelife.co.za/broker-admin-portal/channel/v1',
  baseApiUrl: 'http://kp-services.kingpricelife.co.za:8006/v1'
};
