package za.co.kingprice.brokerportalorch.brokerportalorch.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentBytes {

     private byte[] image;
     private String contentType;
     private String originalFileName;
     private String name;

}
