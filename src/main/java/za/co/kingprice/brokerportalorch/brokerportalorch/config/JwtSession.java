package za.co.kingprice.brokerportalorch.brokerportalorch.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import za.co.kingprice.brokerportalorch.brokerportalorch.models.Token;

@Component
@Scope("session")
@Data
public class JwtSession {

    private Token token;

}
