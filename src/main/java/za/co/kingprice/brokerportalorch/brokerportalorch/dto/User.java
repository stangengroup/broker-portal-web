package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers.Named;

@Data
@AllArgsConstructor
public class User extends Named {

    public User(String name) {

        super(name);

    }

}
