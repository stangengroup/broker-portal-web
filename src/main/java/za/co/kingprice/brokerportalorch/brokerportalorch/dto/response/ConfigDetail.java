package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.NamedEntityDetails;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfigDetail {

    List<NamedEntityDetails> commissionStructureTypeList;
    List<NamedEntityDetails> commsPreferenceList;
    List<NamedEntityDetails> documentTypeList;
    List<NamedEntityDetails> feeStructureTypeList;
    List<NamedEntityDetails> fspStatusList;
    List<NamedEntityDetails> intermediaryTypeList;
    List<NamedEntityDetails> policyTypeList;
    List<NamedEntityDetails> qualificationTypeList;

}
