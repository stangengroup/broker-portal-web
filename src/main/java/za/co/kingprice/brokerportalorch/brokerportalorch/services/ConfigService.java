package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.ConfigDetail;

public interface ConfigService {

    ResponseEntity<ConfigDetail> findAllConfigData();

}
