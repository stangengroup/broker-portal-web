package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;

import java.security.NoSuchAlgorithmException;

public interface AuthService {


    ResponseEntity<?> login() throws NoSuchAlgorithmException;
}
