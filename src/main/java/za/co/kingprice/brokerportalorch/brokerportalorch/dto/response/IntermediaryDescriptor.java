package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response;

import lombok.Getter;
import lombok.Setter;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers.PersonDescriptor;

@Getter
@Setter
public class IntermediaryDescriptor extends PersonDescriptor {

    private String brokerCode;

}
