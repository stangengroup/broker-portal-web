package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.KeyIndividualDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;

public interface KeyIndividualService {

    void delete(long id);

    ResponseEntity<?> get(long id);

    ResponseEntity<?> list(long fspId, int limit, int page);

    KeyIndividualDetails save(KeyIndividualDetails request);

    KeyIndividualDetails update(KeyIndividualDetails request, long id);

    ResponseEntity<?> count(long fspId);
}
