package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String streetNumber;
    private String streetName;
    private String streetName2;
    private String suburb;
    private String town;
    private String province;
    private String postalCode;

}
