package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.DocumentTypeYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.IntermediaryTypeYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.NamedEntityDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class IntermediaryTypeServiceImpl implements IntermediaryTypeService {

    private static final Logger logger = LogManager.getLogger(IntermediaryTypeServiceImpl.class);
    private final IntermediaryTypeYamlProperties intermediaryTypeYamlProperties;

    private final RestTemplate restTemplate;

    public IntermediaryTypeServiceImpl(
            IntermediaryTypeYamlProperties intermediaryTypeYamlProperties,
            RestTemplateBuilder restTemplateBuilder
    ) {
        this.intermediaryTypeYamlProperties = intermediaryTypeYamlProperties;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public ResponseEntity<List<NamedEntityDetails>> list() {
        ParameterizedTypeReference<List<NamedEntityDetails>> responseType = new ParameterizedTypeReference<List<NamedEntityDetails>>() {};
        ResponseEntity<List<NamedEntityDetails>> responseEntity;
        try {
            responseEntity = restTemplate.exchange(
                    intermediaryTypeYamlProperties.getList(),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve list of", "intermediary type"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }

}
