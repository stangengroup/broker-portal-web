package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.DocumentDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.request.MultipartFileUnique;

import java.io.IOException;
import java.util.List;

public interface DocumentService {

    void delete(long id);

    ResponseEntity<?> get(long id);

    ResponseEntity<List<DocumentDetails>> listByFsp(long fspId);
    ResponseEntity<List<DocumentDetails>> listByKeyIndividual(long keyIndividualId);
    ResponseEntity<List<DocumentDetails>> listByIntermediary(long intermediaryId);

    void saveByFsp(long id, long fspId, MultipartFileUnique multipartFileUnique) throws IOException; // to do -- how will the file's data come in?
    void saveByKeyIndividual(long individualId, long keyIndividualId, MultipartFileUnique multipartFileUnique) throws IOException; // to do -- how will the file's data come in?
    void saveByIntermediary(long id, long intermediaryId, MultipartFileUnique multipartFileUnique) throws IOException; // to do -- how will the file's data come in?

}
