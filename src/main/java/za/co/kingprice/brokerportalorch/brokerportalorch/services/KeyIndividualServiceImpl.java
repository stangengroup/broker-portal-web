package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.UtilityService;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.KeyIndividualYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.IntermediaryDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.KeyIndividualDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.KeyIndividualDescriptor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.ListCount;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class KeyIndividualServiceImpl implements KeyIndividualService {

    private static final Logger logger = LogManager.getLogger(KeyIndividualServiceImpl.class);
    private final KeyIndividualYamlProperties keyIndividualYamlProperties;
    private final UtilityService utilityService;

    private final RestTemplate restTemplate;

    public KeyIndividualServiceImpl(
            KeyIndividualYamlProperties keyIndividualYamlProperties,
            UtilityService utilityService, RestTemplateBuilder restTemplateBuilder
    ) {
        this.keyIndividualYamlProperties = keyIndividualYamlProperties;
        this.utilityService = utilityService;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public void delete(long id) {
        try {
             restTemplate.exchange(
                    String.format(keyIndividualYamlProperties.getDelete(), id),
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "delete", "key individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }

    @Override
    public ResponseEntity<?> get(long id) {
        ResponseEntity<KeyIndividualDetails> getIntermediaryResponseEntity;
        try {
            getIntermediaryResponseEntity = restTemplate.exchange(
                    String.format(keyIndividualYamlProperties.getGet(), id),
                    HttpMethod.GET,
                    null,
                    KeyIndividualDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "key individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return getIntermediaryResponseEntity;
    }

    @Override
    public ResponseEntity<?> list(long fspId, int limit, int page) {
        ParameterizedTypeReference<List<KeyIndividualDescriptor>> responseType = new ParameterizedTypeReference<List<KeyIndividualDescriptor>>() {};
        ResponseEntity<List<KeyIndividualDescriptor>> responseEntity;
        try {
            logger.info("LIST URL | " + String.format(keyIndividualYamlProperties.getList(), fspId, limit, page));
            responseEntity = restTemplate.exchange(
                    String.format(keyIndividualYamlProperties.getList(), fspId, limit, page),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve list of", "key individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }

    @Override
    public KeyIndividualDetails save(KeyIndividualDetails request) {
        ResponseEntity<KeyIndividualDetails> postResponseResponseEntity;
        try {
            postResponseResponseEntity = restTemplate.exchange(
                    keyIndividualYamlProperties.getPost(),
                    HttpMethod.POST,
                    new HttpEntity<>(request, utilityService.getHttpHeaders()),
                    KeyIndividualDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "save", "key individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return postResponseResponseEntity.getBody();
    }

    @Override
    public KeyIndividualDetails update(KeyIndividualDetails request, long id) {
        ResponseEntity<KeyIndividualDetails> postResponseResponseEntity;
        try {
            postResponseResponseEntity = restTemplate.exchange(
                    String.format(keyIndividualYamlProperties.getPut(), id),
                    HttpMethod.PUT,
                    new HttpEntity<>(request, utilityService.getHttpHeaders()),
                    KeyIndividualDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "update", "key individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return postResponseResponseEntity.getBody();
    }

    @Override
    public ResponseEntity<?> count(long fspId) {
        ResponseEntity<ListCount> listCountResponseEntity;
        try {
            listCountResponseEntity = restTemplate.exchange(
                    String.format(keyIndividualYamlProperties.getListCount(), fspId),
                    HttpMethod.GET,
                    null,
                    ListCount.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "key individual list count"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return listCountResponseEntity;
    }

}
