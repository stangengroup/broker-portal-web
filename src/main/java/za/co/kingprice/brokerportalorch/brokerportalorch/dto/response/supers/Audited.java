package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers;

import lombok.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.AuditEvent;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Audited {

    private AuditEvent created;
    private AuditEvent updated;
    private long id;

}
