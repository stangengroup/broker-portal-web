package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.PersonDetails;

@Getter
@NoArgsConstructor
@Setter
public class KeyIndividualDetails extends PersonDetails {

    private Long fspId;
    private boolean active;
    private long keyIndividualId;

}
