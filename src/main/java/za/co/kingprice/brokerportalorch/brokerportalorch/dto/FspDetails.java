package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FspDetails extends EntityDetails {

    private String altContactNumber;
    private String altEmailAddress;
    private String mobileNumber;
    private String companyRegNumber;
    private String emailAddress;
    private String domainName;
    private String fspRegNumber;
    private String taxNumber;
    private String tradingAs;
    private String vatNumber;
    private String workNumber;

    private Address address;
    private BankingDetails bankingDetails;
    private Commission commission;
    private Fees fees;

    private NamedEntityDetails fspStatus;

}
