package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.AuthYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.models.B2cAuth;

import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    private final AuthYamlProperties yamlProperties;
    private final RestTemplate restTemplate;
    private static final Logger logger = LogManager.getLogger(AuthServiceImpl.class);

    public AuthServiceImpl(AuthYamlProperties yamlProperties, RestTemplateBuilder restTemplateBuilder) {
        this.yamlProperties = yamlProperties;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public ResponseEntity<?> login() throws NoSuchAlgorithmException {

        logger.info("Reached AuthServiceImpl");

        SecureRandom secureRandom = SecureRandom.getInstance("NativePRNG");
        byte[] bytes = new byte[64];
        secureRandom.nextBytes(bytes);

        byte[] challengeBytesSha256hex = DigestUtils.sha256Hex(secureRandom.toString()).getBytes();
        String codeChallenge = Base64.getEncoder().encodeToString(challengeBytesSha256hex);

        B2cAuth b2cAuth = new B2cAuth(
                yamlProperties.getTenant(),
                yamlProperties.getPolicy(),
                yamlProperties.getClientId(),
                "code",
                yamlProperties.getRedirectUri(),
                yamlProperties.getScope(),
                "query",
                UUID.randomUUID().toString(),
                codeChallenge,
                "S256",
                "login"
        );

        String url = String.format(
                yamlProperties.getUrl(),
                yamlProperties.getTenant(),
                yamlProperties.getTenant(),
                yamlProperties.getPolicy()
        );

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("client_id", b2cAuth.getClientId())
                .queryParam("response_type", b2cAuth.getResponseType())
                .queryParam("redirect_uri", b2cAuth.getRedirectUri())
                .queryParam("scope", b2cAuth.getScope())
                .queryParam("response_mode", b2cAuth.getResponseMode())
                .queryParam("state", b2cAuth.getState())
                .queryParam("prompt", b2cAuth.getPrompt())
                .queryParam("code_challenge", b2cAuth.getCodeChallenge())
                .queryParam("code_challenge_method", b2cAuth.getCodeChallengeMethod());

        HttpHeaders headers = new HttpHeaders();

        logger.info("Calling responseEntity");

        ResponseEntity responseEntity = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                String.class
        );

        headers.setLocation(URI.create(String.valueOf(responseEntity.getHeaders().get("Location"))));

        logger.info("Calling responseEntity | ");

        return responseEntity;

    }
}
