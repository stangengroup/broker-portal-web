package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.DocumentDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.NamedEntityDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.request.MultipartFileUnique;
import za.co.kingprice.brokerportalorch.brokerportalorch.enums.DocumentType;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.DocumentService;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.DocumentServiceImpl;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.DocumentTypeService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/document")
public class DocumentController {

    private final DocumentService documentService;
    private final DocumentTypeService documentTypeService;
    private static final Logger logger = LogManager.getLogger(DocumentController.class);

    public DocumentController(DocumentService documentService, DocumentTypeService documentTypeService) {
        this.documentService = documentService;
        this.documentTypeService = documentTypeService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        documentService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable long id) {
        return documentService.get(id);

    }

    @GetMapping("/list/fsp/{id}")
    public ResponseEntity<?> listByFsp(@PathVariable long id) {
        List<DocumentDetails> documentDetails = documentService.listByFsp(id).getBody();

        ArrayList<String> fspDocumentTypes = new ArrayList<String>(Arrays.asList(DocumentType.CIPC_REG.value(), DocumentType.DELEGATION_OF_AUTHORITY.value(), DocumentType.FSP_REGISTRATION.value(), DocumentType.PROOF_OF_ADDRESS.value(), DocumentType.PROOF_OF_BANKING.value(), DocumentType.SARS_LETTER_OF_GOOD_STANDING.value(), DocumentType.VAT_REGISTRATION_CERTIFICATE.value(), DocumentType.BBBEE_VERIFICATION_CERTIFICATE.value(), DocumentType.PROOF_OF_PROFESSIONAL_INDEMNITY_COVER.value()));

        List<NamedEntityDetails> documentTypeList = Objects.requireNonNull(documentTypeService
                .list()
                .getBody())
                .stream()
                .filter(namedEntityDetails -> fspDocumentTypes.contains(namedEntityDetails.getName()))
                .collect(Collectors.toList());

        documentTypeList
                .forEach(namedEntityDetails -> {
                    if (namedEntityDetails != null) {
                        if (documentDetails != null && documentDetails.size() > 0) {
                            boolean isPresent = documentDetails.stream().anyMatch(documentDetails1 -> {
                                logger.info(String.format("Is %s a match for %s | result is %s", documentDetails1.getDocumentType(), namedEntityDetails, documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName())));
                                return documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName());
                            });
                            if (!isPresent) {
                                logger.info("No document of type | " + namedEntityDetails.getName() + " is present for fsp with id | " + id);
                                documentDetails.add(new DocumentDetails(namedEntityDetails));
                            }
//                                documentDetails
//                                        .forEach(documentDetails1 -> {
//                                            if (documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName())) {
//
//                                            }
//                                        });
                        } else {
                            if (documentDetails != null) {
                                documentDetails.add(new DocumentDetails(namedEntityDetails));
                            }
                        }
                    }
                });

        return ResponseEntity.ok(documentDetails);

    }

    @GetMapping("/list/key-individual/{id}")
    public ResponseEntity<?> listByKeyIndividual(@PathVariable long id) {
        List<DocumentDetails> documentDetails = documentService.listByKeyIndividual(id).getBody();

        ArrayList<String> keyIndividualDocumentTypes = new ArrayList<String>(Arrays.asList(DocumentType.APPLICATION_FORM.value(), DocumentType.PROOF_OF_ADDRESS.value(), DocumentType.PROOF_OF_IDENTITY.value(), DocumentType.KI_QUALIFICATION.value()));

        List<NamedEntityDetails> documentTypeList = Objects.requireNonNull(documentTypeService
                .list()
                .getBody())
                .stream()
                .filter(namedEntityDetails -> keyIndividualDocumentTypes.contains(namedEntityDetails.getName()))
                .collect(Collectors.toList());

        documentTypeList
                .forEach(namedEntityDetails -> {
                    if (namedEntityDetails != null) {
                        if (documentDetails != null && documentDetails.size() > 0) {
                            boolean isPresent = documentDetails.stream().anyMatch(documentDetails1 -> {
                                logger.info(String.format("Is %s a match for %s | result is %s", documentDetails1.getDocumentType(), namedEntityDetails, documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName())));
                                return documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName());
                            });
                            if (!isPresent) {
                                logger.info("No document of type | " + namedEntityDetails.getName() + " is present for KI with id | " + id);
                                documentDetails.add(new DocumentDetails(namedEntityDetails));
                            }
//                                documentDetails
//                                        .forEach(documentDetails1 -> {
//                                            if (documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName())) {
//
//                                            }
//                                        });
                        } else {
                            if (documentDetails != null) {
                                documentDetails.add(new DocumentDetails(namedEntityDetails));
                            }
                        }
                    }
                });

        return ResponseEntity.ok(documentDetails);

    }

    @GetMapping("/list/intermediary/{id}")
    public ResponseEntity<?> listByIntermediary(@PathVariable long id) {
        List<DocumentDetails> documentDetails = documentService.listByIntermediary(id).getBody();

        ArrayList<String> intermediaryDocumentTypes = new ArrayList<String>(Arrays.asList(DocumentType.APPLICATION_FORM.value(), DocumentType.PROOF_OF_ADDRESS.value(), DocumentType.PROOF_OF_IDENTITY.value(), DocumentType.PROOF_OF_REGISTRATION_OF_REPRESENTATIVE_FSP.value(), DocumentType.INTERMEDIARY_QUALIFICATION.value()));

        List<NamedEntityDetails> documentTypeList = Objects.requireNonNull(documentTypeService
                .list()
                .getBody())
                .stream()
                .filter(namedEntityDetails -> intermediaryDocumentTypes.contains(namedEntityDetails.getName()))
                .collect(Collectors.toList());

        logger.info("Document details at start | " + documentDetails.toString());

        documentTypeList
                .forEach(namedEntityDetails -> {
                    if (namedEntityDetails != null) {
                        if (documentDetails != null && documentDetails.size() > 0) {
                            boolean isPresent = documentDetails.stream().anyMatch(documentDetails1 -> {
                                logger.info(String.format("Is %s a match for %s | result is %s", documentDetails1.getDocumentType(), namedEntityDetails, documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName())));
                                return documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName());
                            });
                            if (!isPresent) {
                                logger.info("No document of type | " + namedEntityDetails.getName() + " is present for Intermediary with id | " + id);
                                documentDetails.add(new DocumentDetails(namedEntityDetails));
                                logger.info("Document details thus far | !present " + documentDetails.toString());
                            }
//                                documentDetails
//                                        .forEach(documentDetails1 -> {
//                                            if (documentDetails1.getDocumentType().getName().equalsIgnoreCase(namedEntityDetails.getName())) {
//
//                                            }
//                                        });
                        } else {
                            logger.info("Document details is null or = 0");
                            if (documentDetails != null) {
                                documentDetails.add(new DocumentDetails(namedEntityDetails));
                                logger.info("Since it is null, add Document detail " + namedEntityDetails.getName());
                            }
                        }
                    }
                });

        return ResponseEntity.ok(documentDetails);
    }

    @PostMapping(value = "/fsp/{fspId}/{documentTypeId}", consumes = {"multipart/form-data"})
    public ResponseEntity<?> saveByFsp(@PathVariable long fspId, @PathVariable long documentTypeId, @ModelAttribute MultipartFileUnique multipartFileUnique) throws IOException {

        documentService.saveByFsp(fspId, documentTypeId, multipartFileUnique);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/key-individual/{keyIndividualId}/{documentTypeId}", consumes = {"multipart/form-data"})
    public ResponseEntity<?> saveByKeyIndividual(@PathVariable long keyIndividualId, @PathVariable long documentTypeId, @ModelAttribute MultipartFileUnique multipartFileUnique) throws IOException {
        documentService.saveByKeyIndividual(keyIndividualId, documentTypeId, multipartFileUnique);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/intermediary/{intermediaryId}/{documentTypeId}", consumes = {"multipart/form-data"})
    public ResponseEntity<?> saveByIntermediary(@PathVariable long intermediaryId, @PathVariable long documentTypeId, @ModelAttribute MultipartFileUnique multipartFileUnique) throws IOException {
        documentService.saveByIntermediary(intermediaryId, documentTypeId, multipartFileUnique);
        return ResponseEntity.ok().build();
    }

}
