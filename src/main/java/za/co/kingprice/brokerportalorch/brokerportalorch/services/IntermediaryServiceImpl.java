package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.UtilityService;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.IntermediaryYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.IntermediaryDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.IntermediaryDescriptor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.ListCount;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class IntermediaryServiceImpl implements IntermediaryService {

    private static final Logger logger = LogManager.getLogger(IntermediaryServiceImpl.class);
    private final IntermediaryYamlProperties intermediaryYamlProperties;
    private final UtilityService utilityService;

    private final RestTemplate restTemplate;

    public IntermediaryServiceImpl(
            IntermediaryYamlProperties intermediaryYamlProperties,
            UtilityService utilityService, RestTemplateBuilder restTemplateBuilder
    ) {
        this.intermediaryYamlProperties = intermediaryYamlProperties;
        this.utilityService = utilityService;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public void delete(long id) {
        try {
             restTemplate.exchange(
                    String.format(intermediaryYamlProperties.getDelete(), id),
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "delete", "intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }

    @Override
    public ResponseEntity<?> get(long id) {
        ResponseEntity<IntermediaryDetails> getIntermediaryResponseEntity;
        try {
            getIntermediaryResponseEntity = restTemplate.exchange(
                    String.format(intermediaryYamlProperties.getGet(), id),
                    HttpMethod.GET,
                    null,
                    IntermediaryDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return getIntermediaryResponseEntity;
    }

    @Override
    public ResponseEntity<?> list(long fspId, int limit, int page) {
        ParameterizedTypeReference<List<IntermediaryDescriptor>> responseType = new ParameterizedTypeReference<List<IntermediaryDescriptor>>() {};
        ResponseEntity<List<IntermediaryDescriptor>> responseEntity;
        try {
            logger.info("LIST URL | " + String.format(intermediaryYamlProperties.getList(), fspId, limit, page));
            responseEntity = restTemplate.exchange(
                    String.format(intermediaryYamlProperties.getList(), fspId, limit, page),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve list of", "intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }

    @Override
    public IntermediaryDetails save(IntermediaryDetails request) {
        ResponseEntity<IntermediaryDetails> postResponseResponseEntity;
        try {
            postResponseResponseEntity = restTemplate.exchange(
                    intermediaryYamlProperties.getPost(),
                    HttpMethod.POST,
                    new HttpEntity<>(request, utilityService.getHttpHeaders()),
                    IntermediaryDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "save", "intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return postResponseResponseEntity.getBody();
    }

    @Override
    public IntermediaryDetails update(IntermediaryDetails request, long id) {
        ResponseEntity<IntermediaryDetails> postResponseResponseEntity;
        try {
            postResponseResponseEntity = restTemplate.exchange(
                    String.format(intermediaryYamlProperties.getPut(), id),
                    HttpMethod.PUT,
                    new HttpEntity<>(request, utilityService.getHttpHeaders()),
                    IntermediaryDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "update", "intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return postResponseResponseEntity.getBody();
    }

    @Override
    public ResponseEntity<?> count(long fspId) {
        ResponseEntity<ListCount> listCountResponseEntity;
        try {
            listCountResponseEntity = restTemplate.exchange(
                    String.format(intermediaryYamlProperties.getListCount(), fspId),
                    HttpMethod.GET,
                    null,
                    ListCount.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "intermediary list count"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return listCountResponseEntity;
    }

}
