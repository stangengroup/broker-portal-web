package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

@Getter
@NoArgsConstructor
@Setter
public class PolicyDetails extends EntityDetails {

    private String number;
    private Float premium;

    private Commission commission;
    private NamedEntityDetails commsPreference;
    private NamedEntityDetails policyType;

}
