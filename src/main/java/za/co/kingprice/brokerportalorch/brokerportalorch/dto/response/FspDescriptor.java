package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

import java.util.Date;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FspDescriptor extends EntityDetails {

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date created;
    public String fspRegNumber;
    public String fspStatus;
    public String name;

}
