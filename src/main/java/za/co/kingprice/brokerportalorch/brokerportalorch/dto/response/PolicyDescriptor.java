package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.Policy;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PolicyDescriptor extends EntityDetails {

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date created;
    public String policyNumber;

    public PolicyDescriptor(Policy policy) {

        super(policy.getId());

        this.created = policy.getCreated().getTimestamp();
        this.policyNumber = policy.getNumber();

    }

}
