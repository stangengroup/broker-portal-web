package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.UtilityService;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.FspYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.FspDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class FspServiceImpl implements FspService {

    private static final Logger logger = LogManager.getLogger(FspServiceImpl.class);
    private final FspYamlProperties fspYamlProperties;
    private final UtilityService utilityService;

    private final RestTemplate restTemplate;

    public FspServiceImpl(
            FspYamlProperties fspYamlProperties,
            UtilityService utilityService, RestTemplateBuilder restTemplateBuilder
    ) {
        this.fspYamlProperties = fspYamlProperties;
        this.utilityService = utilityService;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public void delete(long id) {
        try {
             restTemplate.exchange(
                    String.format(fspYamlProperties.getDelete(), id),
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "delete", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }

    @Override
    public ResponseEntity<?> get(long id) {
        ResponseEntity<FspDetails> getFspResponseEntity;
        try {
            getFspResponseEntity = restTemplate.exchange(
                    String.format(fspYamlProperties.getGet(), id),
                    HttpMethod.GET,
                    null,
                    FspDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return getFspResponseEntity;
    }

    @Override
    public ResponseEntity<?> list(int limit, int page) {
        ParameterizedTypeReference<List<FspDescriptor>> responseType = new ParameterizedTypeReference<List<FspDescriptor>>() {};
        ResponseEntity<List<FspDescriptor>> responseEntity;
        try {
            logger.info("LIST URL | " + String.format(fspYamlProperties.getList(), limit, page));
            responseEntity = restTemplate.exchange(
                    String.format(fspYamlProperties.getList(), limit, page),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve list of", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }

    @Override
    public FspDetails save(FspDetails request) {
        ResponseEntity<FspDetails> postResponseResponseEntity;
        try {
            postResponseResponseEntity = restTemplate.exchange(
                    String.format(fspYamlProperties.getPost()),
                    HttpMethod.POST,
                    new HttpEntity<>(request, utilityService.getHttpHeaders()),
                    FspDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info("MESSAGE | " + e.getMessage());
            logger.info("Response body | " + e.getResponseBodyAsString());
            logger.info("Status Text | " + e.getStatusText());
            logger.info("Status Text | " + e.getLocalizedMessage());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "save", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return postResponseResponseEntity.getBody();
    }

    @Override
    public FspDetails update(FspDetails request, long id) {
        ResponseEntity<FspDetails> postResponseResponseEntity;
        try {
            postResponseResponseEntity = restTemplate.exchange(
                    String.format(fspYamlProperties.getPut(), id),
                    HttpMethod.PUT,
                    new HttpEntity<>(request, utilityService.getHttpHeaders()),
                    FspDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "update", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return postResponseResponseEntity.getBody();
    }

    @Override
    public void updateStatus(long id, long statusId) {
        try {
            restTemplate.exchange(
                    String.format(fspYamlProperties.getUpdateStatus(), id, statusId),
                    HttpMethod.PUT,
                    null,
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "update status of", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }

    @Override
    public ResponseEntity<?> count() {
        ResponseEntity<ListCount> listCountResponseEntity;
        try {
            listCountResponseEntity = restTemplate.exchange(
                    fspYamlProperties.getListCount(),
                    HttpMethod.GET,
                    null,
                    ListCount.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "FSP list count"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return listCountResponseEntity;
    }

}
