package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListCount {

    private long count;

}
