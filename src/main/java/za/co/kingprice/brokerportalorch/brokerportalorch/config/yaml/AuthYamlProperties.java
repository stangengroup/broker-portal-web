package za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "application.properties.configuration.auth")
public class AuthYamlProperties {

    private String url;
    private String clientId;
    private String tenant;
    private String policy;
    private String redirectUri;
    private String scope;

}
