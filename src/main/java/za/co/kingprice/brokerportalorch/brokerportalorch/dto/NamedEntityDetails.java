package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class NamedEntityDetails extends EntityDetails {

    private String name;

}
