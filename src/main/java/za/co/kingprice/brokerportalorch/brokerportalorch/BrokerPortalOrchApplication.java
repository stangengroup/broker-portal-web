package za.co.kingprice.brokerportalorch.brokerportalorch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class BrokerPortalOrchApplication {

    public static void main(String[] args) {
        SpringApplication.run(BrokerPortalOrchApplication.class, args);
    }

}
