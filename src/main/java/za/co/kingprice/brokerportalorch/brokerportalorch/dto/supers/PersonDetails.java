package za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.Address;

@Getter
@NoArgsConstructor
@Setter
public abstract class PersonDetails extends EntityDetails {

    private String alternativeContactNumber;
    private String alternativeEmailAddress;
    private String emailAddress;
    private String fullName;
    private String idNumber;
    private String mobileNumber;
    private String title;
    private String password;
    private String surname;
    private String workNumber;

    private Address address;

}
