package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.NamedEntityDetails;

import java.util.List;

public interface DocumentTypeService {

    ResponseEntity<List<NamedEntityDetails>> list();

}
