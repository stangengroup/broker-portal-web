package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.FspStatusService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/fspStatus")
public class FspStatusController {

    private final FspStatusService fspStatusService;

    public FspStatusController(FspStatusService fspStatusService) {
        this.fspStatusService = fspStatusService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return fspStatusService.list();
    }

}
