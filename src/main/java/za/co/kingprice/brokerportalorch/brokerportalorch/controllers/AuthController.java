package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.JwtSession;
import za.co.kingprice.brokerportalorch.brokerportalorch.models.Token;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.AuthService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/auth")
public class AuthController {

    private final AuthService authService;
    private static final Logger logger = LogManager.getLogger(AuthController.class);

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/session")
    private void createSession(HttpServletRequest httpServletRequest, HttpSession session) {
        logger.info("Creating session... | " + httpServletRequest.getHeader("Authorization"));
        String jwtToken = httpServletRequest.getHeader("Authorization");
        Token token = new Token();
        token.setAccess_token(jwtToken);
        JwtSession jwtSession = new JwtSession();
        jwtSession.setToken(token);
        session.setAttribute("token", jwtSession);
    }

    @GetMapping("/login")
    private ResponseEntity<?> login() throws NoSuchAlgorithmException {
        logger.info("calling login");
        return authService.login();
    }

}
