package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.PolicyService;

@RequestMapping("broker-admin-portal/channel/v1/policy")
@RestController
@CrossOrigin
public class PolicyController {

    private final PolicyService policyService;

    public PolicyController(PolicyService policyService) {
        this.policyService = policyService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable(name = "id") long id) {
        return policyService.get(id);
    }

    @GetMapping("/intermediary/{intermediaryId}/{limit}/{page}")
    public ResponseEntity<?> listByIntermediary(@PathVariable long intermediaryId, @PathVariable int limit, @PathVariable int page) {
        return policyService.listByIntermediary(intermediaryId, limit, page);
    }

//    @PostMapping("/{id}")
//    public ResponseEntity<?> saveByIntermediary(@PathVariable(name = "id") long id) {
//
//    }

}
