package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.FspDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.IntermediaryDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.IntermediaryService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/intermediary")
public class InterrmediaryController {

    private final IntermediaryService intermediaryService;

    public InterrmediaryController(IntermediaryService intermediaryService) {
        this.intermediaryService = intermediaryService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        intermediaryService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable long id) {
        return intermediaryService.get(id);

    }

    @GetMapping("/list/{fspId}/{limit}/{page}")
    public ResponseEntity<?> list(@PathVariable long fspId, @PathVariable int limit, @PathVariable int page) {
        return intermediaryService.list(fspId, limit, page);

    }

    @GetMapping("/list/count/{fspId}")
    public ResponseEntity<?> count(@PathVariable long fspId) {
        return intermediaryService.count(fspId);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody IntermediaryDetails request) {
        IntermediaryDetails intermediaryDetails = intermediaryService.save(request);
        return ResponseEntity.ok(intermediaryDetails);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody IntermediaryDetails request, @PathVariable long id) {
        IntermediaryDetails intermediaryDetails = intermediaryService.update(request, id);
        return ResponseEntity.ok(intermediaryDetails);
    }

}
