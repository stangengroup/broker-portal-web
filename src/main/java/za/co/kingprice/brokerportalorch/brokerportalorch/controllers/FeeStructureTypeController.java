package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.FeeStructureTypeService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/feeStructureType")
public class FeeStructureTypeController {

    private final FeeStructureTypeService feeStructureTypeService;

    public FeeStructureTypeController(FeeStructureTypeService feeStructureTypeService) {
        this.feeStructureTypeService = feeStructureTypeService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return feeStructureTypeService.list();
    }

}
