package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.QualificationTypeService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/qualificationType")
public class QualificationTypeController {

    private final QualificationTypeService qualificationTypeService;

    public QualificationTypeController(QualificationTypeService qualificationTypeService) {
        this.qualificationTypeService = qualificationTypeService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return qualificationTypeService.list();
    }

}
