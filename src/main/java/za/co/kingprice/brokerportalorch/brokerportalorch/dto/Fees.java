package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fees {

    private Float percentage;

    private NamedEntityDetails feeStructureType;

}
