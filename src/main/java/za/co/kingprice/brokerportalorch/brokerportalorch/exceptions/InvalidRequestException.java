package za.co.kingprice.brokerportalorch.brokerportalorch.exceptions;

public class InvalidRequestException extends Exception {

    public InvalidRequestException(String field) {

        super("'" + field + "' is missing or invalid.");

    }

    public InvalidRequestException(NullPointerException e) {

        super(e);

    }

}
