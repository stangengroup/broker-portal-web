package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.ConfigDetail;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.ConfigService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/config")
public class ConfigController {

    private final ConfigService configService;

    public ConfigController(ConfigService configService) {
        this.configService = configService;
    }

//    @PreAuthorize("@sessionSecurityEvaluator.hasSessionToAuthority(#httpSession)")
    @GetMapping("/all")
    public ResponseEntity<ConfigDetail> getAllConfigData() {
        return configService.findAllConfigData();
    }


}
