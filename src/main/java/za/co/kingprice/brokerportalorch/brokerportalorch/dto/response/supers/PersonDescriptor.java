package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

import java.util.Date;

@Getter
@Setter
public abstract class PersonDescriptor extends EntityDetails {

    public String contactNumber;
    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date created;
    public String emailAddress;
    public String name;

}
