package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.PolicyTypeService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/policyType")
public class PolicyTypeController {

    private final PolicyTypeService policyTypeService;

    public PolicyTypeController(PolicyTypeService policyTypeService) {
        this.policyTypeService = policyTypeService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return policyTypeService.list();
    }

}
