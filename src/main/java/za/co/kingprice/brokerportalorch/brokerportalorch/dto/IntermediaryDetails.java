package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.PersonDetails;

@Getter
@NoArgsConstructor
@Setter
public class IntermediaryDetails extends PersonDetails {

    private String brokerCode;
    private String intermediaryHome;
    private Long fspId;
    private long intermediaryId;

    private Commission commission;
    private NamedEntityDetails commsPreference;
    private NamedEntityDetails intermediaryAccreditationTestState;
    private NamedEntityDetails intermediaryType;

    private boolean active;

}
