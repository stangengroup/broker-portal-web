package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.KeyIndividualDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.KeyIndividualService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/key-individual")
public class KeyIndividualController {

    private final KeyIndividualService keyIndividualService;

    public KeyIndividualController(KeyIndividualService keyIndividualService) {
        this.keyIndividualService = keyIndividualService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        keyIndividualService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable long id) {
        return keyIndividualService.get(id);

    }

    @GetMapping("/list/{fspId}/{limit}/{page}")
    public ResponseEntity<?> list(@PathVariable long fspId, @PathVariable int limit, @PathVariable int page) {
        return keyIndividualService.list(fspId, limit, page);

    }

    @GetMapping("/list/count/{fspId}")
    public ResponseEntity<?> count(@PathVariable long fspId) {
        return keyIndividualService.count(fspId);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody KeyIndividualDetails request) {
        KeyIndividualDetails keyIndividualDetails = keyIndividualService.save(request);
        return ResponseEntity.ok(keyIndividualDetails);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody KeyIndividualDetails request, @PathVariable long id) {
        KeyIndividualDetails keyIndividualDetails = keyIndividualService.update(request, id);
        return ResponseEntity.ok(keyIndividualDetails);
    }

}
