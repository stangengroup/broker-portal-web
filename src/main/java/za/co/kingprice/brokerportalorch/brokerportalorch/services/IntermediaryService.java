package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.IntermediaryDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;

public interface IntermediaryService {

    void delete(long id);

    ResponseEntity<?> get(long id);

    ResponseEntity<?> list(long fspId, int limit, int page);

    IntermediaryDetails save(IntermediaryDetails request);

    IntermediaryDetails update(IntermediaryDetails request, long id);

    ResponseEntity<?> count(long fspId);

}
