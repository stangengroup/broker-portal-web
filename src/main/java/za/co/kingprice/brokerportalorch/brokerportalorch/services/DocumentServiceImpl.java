package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.UtilityService;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.DocumentYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.DocumentDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.request.DocumentBytes;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.request.MultipartFileUnique;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentYamlProperties documentYamlProperties;
    private static final Logger logger = LogManager.getLogger(DocumentServiceImpl.class);
    private final UtilityService utilityService;
    private final RestTemplate restTemplate;

    public DocumentServiceImpl(DocumentYamlProperties documentYamlProperties, UtilityService utilityService, RestTemplateBuilder restTemplateBuilder) {
        this.documentYamlProperties = documentYamlProperties;
        this.utilityService = utilityService;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public void delete(long id) {
        try {
            restTemplate.exchange(
                    String.format(documentYamlProperties.getDelete(), id),
                    HttpMethod.DELETE,
                    null,
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "delete document", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }

    @Override
    public ResponseEntity<?> get(long id) {
        ResponseEntity<DocumentDetails> getFspResponseEntity;
        try {
            getFspResponseEntity = restTemplate.exchange(
                    String.format(documentYamlProperties.getGet(), id),
                    HttpMethod.GET,
                    null,
                    DocumentDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return getFspResponseEntity;
    }

    @Override
    public ResponseEntity<List<DocumentDetails>> listByFsp(long fspId) {
        ParameterizedTypeReference<List<DocumentDetails>> responseType = new ParameterizedTypeReference<List<DocumentDetails>>() {};
        ResponseEntity<List<DocumentDetails>> responseEntity;
        try {
            responseEntity = restTemplate.exchange(
                    String.format(documentYamlProperties.getListByFsp(), fspId),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve document list of", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }

        return responseEntity;
    }

    @Override
    public ResponseEntity<List<DocumentDetails>> listByKeyIndividual(long keyIndividualId) {
        ParameterizedTypeReference<List<DocumentDetails>> responseType = new ParameterizedTypeReference<List<DocumentDetails>>() {};
        ResponseEntity<List<DocumentDetails>> responseEntity;
        try {
            responseEntity = restTemplate.exchange(
                    String.format(documentYamlProperties.getListByKeyIndividual(), keyIndividualId),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve document list of", "Key Individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<List<DocumentDetails>> listByIntermediary(long intermediaryId) {
        ParameterizedTypeReference<List<DocumentDetails>> responseType = new ParameterizedTypeReference<List<DocumentDetails>>() {};
        ResponseEntity<List<DocumentDetails>> responseEntity;
        try {
            responseEntity = restTemplate.exchange(
                    String.format(documentYamlProperties.getListByIntermediary(), intermediaryId),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve document list of", "Intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }

    @Override
    public void saveByFsp(long fspId, long documentTypeId, MultipartFileUnique multipartFileUnique) throws IOException {
        try {

            byte[] imageBytes = multipartFileUnique.getImage().getBytes();

            DocumentBytes documentBytes = new DocumentBytes(imageBytes, multipartFileUnique.getImage().getContentType(), multipartFileUnique.getImage().getOriginalFilename(), multipartFileUnique.getImage().getName());

            restTemplate.exchange(
                    String.format(documentYamlProperties.getPostByFsp(), fspId, documentTypeId),
                    HttpMethod.POST,
                    new HttpEntity<>(documentBytes, utilityService.getHttpHeaders()),
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "save document", "FSP"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
//        } catch (IOException e) {
//            throw new IOException(e);
//        }
        }
    }

    @Override
    public void saveByKeyIndividual(long keyIndividualId, long documentTypeId, MultipartFileUnique multipartFileUnique) throws IOException {
        try {

            byte[] imageBytes = multipartFileUnique.getImage().getBytes();

            DocumentBytes documentBytes = new DocumentBytes(imageBytes, multipartFileUnique.getImage().getContentType(), multipartFileUnique.getImage().getOriginalFilename(), multipartFileUnique.getImage().getName());

            restTemplate.exchange(
                    String.format(documentYamlProperties.getPostByKeyIndividual(), keyIndividualId, documentTypeId),
                    HttpMethod.POST,
                    new HttpEntity<>(documentBytes, utilityService.getHttpHeaders()),
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "save document", "Key Individual"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }

    @Override
    public void saveByIntermediary(long intermediaryId, long documentTypeId, MultipartFileUnique multipartFileUnique) throws IOException {
        try {

            byte[] imageBytes = multipartFileUnique.getImage().getBytes();

            DocumentBytes documentBytes = new DocumentBytes(imageBytes, multipartFileUnique.getImage().getContentType(), multipartFileUnique.getImage().getOriginalFilename(), multipartFileUnique.getImage().getName());

            restTemplate.exchange(
                    String.format(documentYamlProperties.getPostByIntermediary(), intermediaryId, documentTypeId),
                    HttpMethod.POST,
                    new HttpEntity<>(documentBytes, utilityService.getHttpHeaders()),
                    Void.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "save document", "Intermediary"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
    }
}
