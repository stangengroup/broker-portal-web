package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.FspDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;

public interface FspService {

    void delete(long id);

    ResponseEntity<?> get(long id);

    ResponseEntity<?> list(int limit, int page);

    FspDetails save(FspDetails request);

    FspDetails update(FspDetails request, long id);

    void updateStatus(long id, long statusId);

    ResponseEntity<?> count();

}
