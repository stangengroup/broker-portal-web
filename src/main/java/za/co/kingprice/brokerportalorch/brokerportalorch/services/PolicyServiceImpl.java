package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml.PolicyYamlProperties;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.FspDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.PolicyDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.FspDescriptor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PolicyDescriptor;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class PolicyServiceImpl implements PolicyService {

    private final PolicyYamlProperties policyYamlProperties;
    private final RestTemplate restTemplate;
    private static final Logger logger = LogManager.getLogger(PolicyServiceImpl.class);

    public PolicyServiceImpl(
            PolicyYamlProperties policyYamlProperties,
            RestTemplateBuilder restTemplateBuilder
    ) {
        this.policyYamlProperties = policyYamlProperties;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public ResponseEntity<?> get(long id) {
        ResponseEntity<PolicyDetails> policyDetailsResponseEntity;
        try {
            policyDetailsResponseEntity = restTemplate.exchange(
                    String.format(policyYamlProperties.getGet(), id),
                    HttpMethod.GET,
                    null,
                    PolicyDetails.class
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve", "Intermediary Policy"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return policyDetailsResponseEntity;
    }

    @Override
    public ResponseEntity<?> listByIntermediary(long intermediaryId, int limit, int page) {
        ParameterizedTypeReference<List<PolicyDescriptor>> responseType = new ParameterizedTypeReference<List<PolicyDescriptor>>() {};
        ResponseEntity<List<PolicyDescriptor>> responseEntity;
        try {
            logger.info("LIST URL | " + String.format(policyYamlProperties.getListByIntermediary(), intermediaryId, limit, page));
            responseEntity = restTemplate.exchange(
                    String.format(policyYamlProperties.getListByIntermediary(), intermediaryId, limit, page),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve list of", "Intermediary policies"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }
        return responseEntity;
    }
}
