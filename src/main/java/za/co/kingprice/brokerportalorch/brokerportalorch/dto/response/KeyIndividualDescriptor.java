package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response;

import lombok.NoArgsConstructor;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers.PersonDescriptor;

@NoArgsConstructor
public class KeyIndividualDescriptor extends PersonDescriptor {

}
