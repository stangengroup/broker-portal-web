package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankingDetails {

    private String accountHolderName;
    private String accountNumber;
    private String accountType;
    private String bankName;
    private String branchCode;

}
