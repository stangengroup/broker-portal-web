package za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Named {

    @NonNull
    private String name;
    private long id;

    public Named(String name) {

        this.setName(name);

    }

}
