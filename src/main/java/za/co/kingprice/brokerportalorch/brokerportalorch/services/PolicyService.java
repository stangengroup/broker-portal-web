package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.springframework.http.ResponseEntity;

public interface PolicyService {

    ResponseEntity<?> get(long id);

    ResponseEntity<?> listByIntermediary(long intermediaryId, int limit, int page);

}
