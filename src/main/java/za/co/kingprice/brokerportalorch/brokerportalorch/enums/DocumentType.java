package za.co.kingprice.brokerportalorch.brokerportalorch.enums;

import java.util.ArrayList;
import java.util.List;

public enum DocumentType {

    CIPC_REG("CIPC_REG"),
    DELEGATION_OF_AUTHORITY("DELEGATION_OF_AUTHORITY"),
    FSP_REGISTRATION("FSP_REGISTRATION"),
    PROOF_OF_ADDRESS("PROOF_OF_ADDRESS"),
    PROOF_OF_BANKING("PROOF_OF_BANKING"),
    SARS_LETTER_OF_GOOD_STANDING("SARS_LETTER_OF_GOOD_STANDING"),
    VAT_REGISTRATION_CERTIFICATE("VAT_REGISTRATION_CERTIFICATE"),
    BBBEE_VERIFICATION_CERTIFICATE("BBBEE_VERIFICATION_CERTIFICATE"),
    PROOF_OF_PROFESSIONAL_INDEMNITY_COVER("PROOF_OF_PROFESSIONAL_INDEMNITY_COVER"),
    PROOF_OF_IDENTITY("PROOF_OF_IDENTITY"),
    PROOF_OF_REGISTRATION_OF_REPRESENTATIVE_FSP("PROOF_OF_REGISTRATION_OF_REPRESENTATIVE_FSP"),
    INTERMEDIARY_QUALIFICATION("INTERMEDIARY_QUALIFICATION"),
    APPLICATION_FORM("APPLICATION_FORM"),
    KI_QUALIFICATION("KI_QUALIFICATION");

    private String value;
    DocumentType(String value) {
        this.value = value;
    }
    public String value() {
        return value;
    }
    @Override
    public String toString() {
        return String.valueOf(value);
    }
    public static DocumentType fromValue(String v) {
        for (DocumentType b : DocumentType.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
    public static String[] getSearchValuesAsString() {
        List<String> transactionStates = new ArrayList<>();
        for (DocumentType b : DocumentType.values()) {
            transactionStates.add(b.value());
        }
        String[] returnList = new String[transactionStates.size()];
        return transactionStates.toArray(returnList);
    }
}
