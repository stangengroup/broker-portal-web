package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.FspDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.PostResponse;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.FspService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/fsp")
public class FspController {

    private final FspService fspService;

    public FspController(FspService fspService) {
        this.fspService = fspService;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        fspService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable long id) {
        return fspService.get(id);

    }

    @GetMapping("/list/{limit}/{page}")
    public ResponseEntity<?> list(@PathVariable int limit, @PathVariable int page) {
        return fspService.list(limit, page);

    }

    @GetMapping("/list/count")
    public ResponseEntity<?> count() {
        return fspService.count();
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody FspDetails request) {
        FspDetails fspDetails = fspService.save(request);
        return ResponseEntity.ok(fspDetails);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody FspDetails request, @PathVariable long id) {
        fspService.update(request, id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}/status/{statusId}")
    public ResponseEntity<?> updateStatus(@PathVariable long id, @PathVariable long statusId) {
        fspService.updateStatus(id, statusId);
        return ResponseEntity.ok().build();
    }

}
