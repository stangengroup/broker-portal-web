package za.co.kingprice.brokerportalorch.brokerportalorch.exceptions;

public class DuplicationException extends Exception {

    public DuplicationException(String value) {

        super("Entity with '" + value + "' already listed.");

    }

}
