package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers.EntityDetails;

@NoArgsConstructor
@Data
@AllArgsConstructor
@ToString
public class DocumentDetails extends EntityDetails {

    private String name;
    private NamedEntityDetails documentType;
    private String url;

    public DocumentDetails(NamedEntityDetails namedEntityDetails) {
        this.name = null;
        this.documentType = namedEntityDetails;
        this.url = null;
    }

}
