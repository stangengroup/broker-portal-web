package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.CommsPreferenceService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/commsPreference")
public class CommsPreferenceController {

    private final CommsPreferenceService commsPreferenceService;

    public CommsPreferenceController(CommsPreferenceService commsPreferenceService) {
        this.commsPreferenceService = commsPreferenceService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return commsPreferenceService.list();
    }

}
