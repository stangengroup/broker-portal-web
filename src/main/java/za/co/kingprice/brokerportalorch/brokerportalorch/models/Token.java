package za.co.kingprice.brokerportalorch.brokerportalorch.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Token {

    private String access_token;

}
