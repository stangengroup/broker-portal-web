package za.co.kingprice.brokerportalorch.brokerportalorch.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.NamedEntityDetails;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.ConfigDetail;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import java.util.List;

import static za.co.kingprice.brokerportalorch.brokerportalorch.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class ConfigServiceImpl implements ConfigService {

    private final CommissionStructureTypeService commissionStructureTypeService;
    private final CommsPreferenceService commsPreferenceService;
    private final DocumentTypeService documentTypeService;
    private final FeeStructureTypeService feeStructureTypeService;
    private final FspStatusService fspStatusService;
    private final IntermediaryTypeService intermediaryTypeService;
    private final PolicyTypeService policyTypeService;
    private final QualificationTypeService qualificationTypeService;

    private static final Logger logger = LogManager.getLogger(ConfigServiceImpl.class);

    public ConfigServiceImpl(CommissionStructureTypeService commissionStructureTypeService, CommsPreferenceService commsPreferenceService, DocumentTypeService documentTypeService, FeeStructureTypeService feeStructureTypeService, FspStatusService fspStatusService, IntermediaryTypeService intermediaryTypeService, PolicyTypeService policyTypeService, QualificationTypeService qualificationTypeService) {
        this.commissionStructureTypeService = commissionStructureTypeService;
        this.commsPreferenceService = commsPreferenceService;
        this.documentTypeService = documentTypeService;
        this.feeStructureTypeService = feeStructureTypeService;
        this.fspStatusService = fspStatusService;
        this.intermediaryTypeService = intermediaryTypeService;
        this.policyTypeService = policyTypeService;
        this.qualificationTypeService = qualificationTypeService;
    }

    @Override
    public ResponseEntity<ConfigDetail> findAllConfigData() {

        try {
            List<NamedEntityDetails> commissionStructureTypeList = commissionStructureTypeService.list().getBody();
            List<NamedEntityDetails> commsPreferenceList = commsPreferenceService.list().getBody();
            List<NamedEntityDetails> documentTypeList = documentTypeService.list().getBody();
            List<NamedEntityDetails> feeStructureTypeList = feeStructureTypeService.list().getBody();
            List<NamedEntityDetails> fspStatusList = fspStatusService.list().getBody();
            List<NamedEntityDetails> intermediaryTypeList = intermediaryTypeService.list().getBody();
            List<NamedEntityDetails> policyTypeList = policyTypeService.list().getBody();
            List<NamedEntityDetails> qualificationTypeList = qualificationTypeService.list().getBody();

            ConfigDetail configDetail = new ConfigDetail(commissionStructureTypeList, commsPreferenceList, documentTypeList, feeStructureTypeList, fspStatusList, intermediaryTypeList, policyTypeList, qualificationTypeList);
            return ResponseEntity.ok(configDetail);
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "get all", "config"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }


    }
}
