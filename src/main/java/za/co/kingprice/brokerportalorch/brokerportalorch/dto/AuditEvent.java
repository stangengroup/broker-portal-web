package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.Timestamp;
import java.util.Date;

@Getter
@NoArgsConstructor
public class AuditEvent {

    public enum Type {
        CREATE, // 0
        UPDATE, // 1
        DELETE  // 2
    }

    @NonNull
    private Timestamp timestamp;
    private int type;

    private User user;

    private long id;

    public AuditEvent(User user, Type type) {
        this.timestamp = new Timestamp(new Date().getTime());
        this.type = type.ordinal();
        this.user = user;
    }

}
