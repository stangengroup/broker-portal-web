package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.IntermediaryTypeService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/intermediaryType")
public class IntermediaryTypeController {

    private final IntermediaryTypeService intermediaryTypeService;

    public IntermediaryTypeController(IntermediaryTypeService intermediaryTypeService) {
        this.intermediaryTypeService = intermediaryTypeService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return intermediaryTypeService.list();
    }

}
