package za.co.kingprice.brokerportalorch.brokerportalorch.dto.supers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public abstract class EntityDetails {

    private Long id;

}
