package za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "api.broker-portal.document")
public class DocumentYamlProperties {

    private String get;
    private String listByFsp;
    private String listByKeyIndividual;
    private String listByIntermediary;
    private String postByFsp;
    private String postByKeyIndividual;
    private String postByIntermediary;
    private String delete;

}
