package za.co.kingprice.brokerportalorch.brokerportalorch.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.kingprice.brokerportalorch.brokerportalorch.services.CommissionStructureTypeService;

@RestController
@CrossOrigin
@RequestMapping("broker-admin-portal/channel/v1/commissionStructureType")
public class CommissionStructureTypeController {

    private final CommissionStructureTypeService commissionStructureTypeService;

    public CommissionStructureTypeController(CommissionStructureTypeService commissionStructureTypeService) {
        this.commissionStructureTypeService = commissionStructureTypeService;
    }

    @GetMapping("/list")
    public ResponseEntity<?> list() {
        return commissionStructureTypeService.list();
    }

}
