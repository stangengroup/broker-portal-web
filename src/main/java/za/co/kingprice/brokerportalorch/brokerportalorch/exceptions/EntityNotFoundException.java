package za.co.kingprice.brokerportalorch.brokerportalorch.exceptions;

import lombok.Getter;

@Getter
public class EntityNotFoundException extends Exception {

    private final String identifier;

    public EntityNotFoundException(String type, long id) {

        super(type + " with ID " + id + " not found.");

        this.identifier = "" + id;

    }

    public EntityNotFoundException(String type, String name) {

        super(type + " with '" + name + "' not found.");

        this.identifier = name;

    }

}
