package za.co.kingprice.brokerportalorch.brokerportalorch.config.yaml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "api.broker-portal.key-individual")
public class KeyIndividualYamlProperties {

    private String get;
    private String put;
    private String post;
    private String delete;
    private String list;
    private String listCount;

}
