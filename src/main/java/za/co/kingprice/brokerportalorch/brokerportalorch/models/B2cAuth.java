package za.co.kingprice.brokerportalorch.brokerportalorch.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class B2cAuth {

    private String tenant;
    private String policy;
    private String clientId;
    private String responseType;
    private String redirectUri;
    private String scope;
    private String responseMode;
    private String state;
    private String codeChallenge;
    private String codeChallengeMethod;
    private String prompt;

}
