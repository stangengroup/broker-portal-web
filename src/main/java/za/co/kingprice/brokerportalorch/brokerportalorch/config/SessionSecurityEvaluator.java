package za.co.kingprice.brokerportalorch.brokerportalorch.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.GenericException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component(value = "sessionSecurityEvaluator")
public class SessionSecurityEvaluator {

    private static final Logger logger = LogManager.getLogger(SessionSecurityEvaluator.class);

    public boolean hasSessionToAuthority(HttpSession httpSession) {
        logger.info("Checking if session is valid");
        boolean isValid;
        if (httpSession == null) {
            return false;
        }
        try {
            JwtSession jwtSession = (JwtSession) httpSession.getAttribute("token");
            if (jwtSession == null) {
                isValid = false;
            } else {
                isValid = true;
            }
        } catch (IllegalStateException ise) {
            // it's invalid
            isValid = false;
        }
        return isValid;
    }

//    public boolean hasSessionToAuthorityHttpSession (HttpSession httpSession, String permission) {
//        logger.info("Checking if user has " + permission + " permission");
//        try {
//            VerifyUserSession jwtSession = (VerifyUserSession) httpSession.getAttribute("tokenVerify");
//            JWT jwt = new JWT();
//            DecodedJWT decodedJWT = jwt.decodeJwt(jwtSession.getTokenVerify().getAccess_token().replace("Bearer ", ""));
//            return decodedJWT.getClaim("authorities")
//                    .asList(String.class)
//                    .stream()
//                    .anyMatch(authority -> authority.equalsIgnoreCase(permission));
//        } catch (Exception e) {
//            logger.error("Error occurred | " + Arrays.toString(e.getStackTrace()));
//            return false;
//        }
//    }

}
