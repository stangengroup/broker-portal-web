package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Commission {

    private Float asAndWhen;
    private Float upfront;

    private NamedEntityDetails commissionStructureType;

}
