package za.co.kingprice.brokerportalorch.brokerportalorch.dto;

import lombok.*;
import za.co.kingprice.brokerportalorch.brokerportalorch.dto.response.supers.Audited;
import za.co.kingprice.brokerportalorch.brokerportalorch.exceptions.InvalidRequestException;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Policy extends Audited {

    private String number;
    private float premium;

    @NonNull
    private Commission commissionStructure;
    private NamedEntityDetails commsPreference;
    @NonNull
    private IntermediaryDetails intermediary;
    @NonNull
    private NamedEntityDetails policyType;

    public Policy(PolicyDetails from, IntermediaryDetails intermediary) throws InvalidRequestException {

        this.update(from);

        this.setIntermediary(intermediary);

    }

    public Policy update(PolicyDetails from) throws InvalidRequestException {

        try {
            this.setNumber(from.getNumber());
            this.setPremium(from.getPremium());
        } catch (NullPointerException e) {
            throw new InvalidRequestException("'policyNumber' or 'premium' missing.");
        }

        return this;

    }

}
